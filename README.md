# 0. Setup lol
    git clone https://:@gitlab.cern.ch:8443/tkunigo/StatToolsWVlvqq.git
    cd StatToolsWVlvqq
    source ./init_setup.sh



# 1. Workspace production
    cd Run
    ./run_mkdir.py
Please copy input trees you want to use to 'test_run/WVlvqq_mc15_v28/data/itest01/'
You need to re-name trees in the root-files before launching ws production jobs, please use 'myRenameTree.C'

    root -b -q 'myRenameTree.C("HVTWW1000.root")' ... # please run on all the root-files
Now you can start ws production.

    ./run_ws_single.sh
Then you can launch jobs for HVTWW1000, BOOSTED HP and LP.
If you want to submit multiple condor jobs, please consider using 'run_ws_example.sh'



# 2. BOOSTED-RESOLVED combination
    cd CombinationTool
    source setup.sh
    ./doCombineFast.sh (BOOSTED HP+LP combinaiton) / ./doCombineBoosteed_Resolved.sh (BOOSTED HP+BOOSTED LP+RESOLVED combination)
Please make sure the coorelation setting in the 'lvqq_combine_fast.py'/'lvqq_combine_fast_boosted_resolved.py'.



# 3.1. Limit calculation (asymptotic)
    cd Run
    ./run_limit_example.sh HVTWW 1000
You can calculate upper limits asymptotically.
The asymptotic limits are needed to calculate toy limits.



# 3.2. Limit calculation (pseudo experiment, toy)
    cd Limit_Toys
    source setup.sh && source setpy.sh
    lsetup panda
    ./submit.sh
These codes limit the range of signal strength to be scanned based on the asymptotic results and then submit jobs to the grid.
**This kind of jobs use huge CPU resources.(In my case, ~10M CPU hours/weak) You should use group production role, if you don't have it, please request it.**
You don't need to calculate toy limits for all the mass points.
Please make sure the agreement between toy and asymptotic limits(meand and 1/2 sigma error bands) in the high-mass region.

    # download the result files from grid to 'output'
    ./merge.py
Then you can merge the results to a single file.



# 3.3 Limit morphing
**We don't use this for ATL-COM-PHYS-2017-324, because we confirmed that there is little difference between morphing result and linear assumption**
First you need to convert TTree -> TH1.
( llqq/vvqq group kindly provide us these codes. They use WorkspaceMaker, for which inputs are TH1, instead of ResonanceFinder. In order to use their nice codes, we need to convert TTree -> TH1.)

    cd Limit_Morphing
    cd yield_hist
    nohup ./hist_maker.py & # please modify the input tree directory in the script
It'll take ~10 hours thus I recommend running it via nohup.
Then you can produce morphing output using 

    cd ../
    cp yield_hist/*.root sig_acceptance/
    nohup ./scripts/morphingZV.sh & # it'll take ~10 hours
    ./run.sh
You'll find the output (TH1) in the output directory, you can convert them to TTrees by 'run.sh'. ( Then you can calculate asymptotic(/toy) limits from them )



# 4. Limit plots
    cd RunLimits
    ./run.sh
    ./run_HEPData.sh
You can make the limit plots and HEPData file.
You can use both asymptotic and toy limits.



# 5.1. Post-fit plots
    cd NuisanceCheckCombination # or NuisanceCheck
    ./runChecks.sh
    ./postfit_macro.sh



# 5.2. Pre-fit distributions
    cd NuisanceCheck
    ./runChecks.sh
    root -l results_HVTWW1000/FitCrossChecks.root
    # cd PlotsBeforeFit/MuIsEqualTo_0/${Region}/${Syst_name}
You can find TCanvas in the TDirectory, in which you can find pre-fit distributions.
**For this study, NuisanceCheckCombination is NOT recommended, as it sometimes shows strange distributions.**



# 5.3. Correlation matrix
    cd NuisanceCheck
    ./GetCorrMatrix.sh
In the results, please make sure that the correlation you applied in the combination package is properly applied. ( e.g. You should not see XS_Top_fileThree etc. )



# 5.4. Pulls plot
    cd NuisanceCheckCombination # or NuisanceCheck
    root -l results_HVTWW1000/FitCrossChecks.root
    # You can find the plot ini TCanvas 'PlotsAfterGlobalFit/conditionnal_MuIsEqualTo_0/can_NuisPara_GlobalFit_conditionnal_mu0'



# 5.5. Nuisance parameter ranking (delta(mu_i)/delta(mu_tot))
    cd NPranking
    lsetup root && ./submit.sh
    ./doBreakdown.sh
    root -b
    .x macros/drawPlot_pulls.C("1000","HVTWW_1000",1)
Then you can make a ranking plot.

    cd StatDir
    ./run.sh
    ./checkErrors.py
To check total syst., data stat. and MC stat. uncertainties, please use the scripts/macros in 'StatDir'.

    root -b -q 'getNumber.C("HVTWW_1000")' | sort -g -k 3,3
You can dump the numbers(delta(mu_i)/delta(mu_tot)) using the macro.



# 5.6. Nuisance parameter ranking (delta(mu_i)/mu_hat)
    cd NPranking_asimovmu
    lsetup root && ./submit.sh
    ./doBreakdown.sh
    root -b
    .x macros/drawPlot_pulls.C("1000","HVTWW_1000",1)

    cd StatDir
    ./run.sh
    ./checkErrors.py
To check total syst., data stat. and MC stat. uncertainties, please use the scripts/macros in 'StatDir'.

    root -b -q 'getNumber.C("HVTWW_1000")' | sort -g -k 10,10
You can dump the numbers(delta(mu_i)/mu_hat) using the macro.





# X. Post-fit plots
If you want to make, for example, D2 post-fit plots, please follow the instruction below.
First you need to generate a s+b fit or b-only fit.
You can do this inside RF by doing at least one calculation.
For example, do a simple  pvalue calculation.
The fit files will be in <Release>/<Analysis>/ws/diagnostics/sbfit_<Analysis>_mass.root
Then, you also need to generate your alternate WS using the other variable you want to plot.

    cd Run
    ./run_D2ws_example.sh 
You can do it in the script.
Then please run run the plotting macro.

    cd ResonanceFinder/python
    ./run_PostFit.sh
