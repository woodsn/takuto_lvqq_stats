// Author      : Stefan Gadatsch
// Email       : gadatsch@nikhef.nl
// Date        : 2013-04-24
// Description : Compute pulls and impact on the POI

#include <string>
#include <vector>

#include "TFile.h"
#include "TH1D.h"
#include "Math/MinimizerOptions.h"
#include "TStopwatch.h"

#include "RooWorkspace.h"
#include "RooStats/ModelConfig.h"
#include "RooDataSet.h"
#include "RooArgSet.h"
#include "RooRealVar.h"
#include "RooNLLVar.h"
#include "RooFitResult.h"

#include "macros/log.C"
#include "macros/parseString.C"
#include "macros/minimize.C"
#include "macros/findSigma.C"

using namespace std;
using namespace RooFit;
using namespace RooStats;

// ____________________________________________________________________________|__________
//void runPulls() {
//    // for compiling only
//}

// ____________________________________________________________________________|__________
// compute pulls of the nuisance parameters and store them in text files. norm and syst parameters will be split among different files
void unfoldConstraints(RooArgSet& initial, RooArgSet& final, RooArgSet& obs, RooArgSet& nuis, int& counter);
RooDataSet* makeAsimovData(double mu_val=0.0, bool fluctuateData=false, string* mu_str=NULL, RooWorkspace* wc = 0, ModelConfig* mc = 0);
void runPulls(const char* inFileName = "workspace.root",
	      const float testMuValue = 1.0,
	      const char* poiName = "SigXsecOverSM",
	      const char* wsName = "combined",
	      const char* modelConfigName = "ModelConfig",
	      const char* dataName = "asimovData",
	      const char* folder = "test",
	      const char* variable = NULL,
	      double precision = 0.005,
	      bool useMinos = 0,
	      string loglevel = "DEBUG")
{
  cout<<"start doing "<<endl;
  
    //TStopwatch timer_pulls;
    //timer_pulls.Start();

    // DEBUG OUTPUT
    // - ERROR
    // - WARNING
    // - INFO
    // - DEBUG

    LOG::FromString(loglevel);
    cout<<"start doing 1"<<endl;

    //LOG::ReportingLevel() = LOG::FromString(loglevel);

    cout<<"start doing 2"<<endl;

    // some settings
    ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
    ROOT::Math::MinimizerOptions::SetDefaultStrategy(2);
    if (loglevel == "DEBUG") {
        ROOT::Math::MinimizerOptions::SetDefaultPrintLevel(1);
    } else {
        ROOT::Math::MinimizerOptions::SetDefaultPrintLevel(-1);
        RooMsgService::instance().setGlobalKillBelow(RooFit::FATAL);
    }
    cout<<"start doing 3"<<endl;


    // loading the workspace etc.
    LOG(logINFO) << "Running over workspace: " << inFileName;
    cout<<"start doing 3.1"<<endl;
    system(("mkdir -vp root-files/" + string(folder) + "_pulls").c_str());
    vector<string> parsed = parseString(poiName, ",");

    TFile* file = new TFile(inFileName);

    RooWorkspace* ws = (RooWorkspace*)file->Get(wsName);
    if (!ws) {
        LOG(logERROR) << "Workspace: " << wsName << " doesn't exist!";
        exit(1);
    }

    ModelConfig* mc = (ModelConfig*)ws->obj(modelConfigName);
    if (!mc) {
        LOG(logERROR) << "ModelConfig: " << modelConfigName << " doesn't exist!";
        exit(1);
    }

    RooDataSet* data;
    if( strcmp(dataName, "asimovData") == 0 ){
      data = makeAsimovData(testMuValue, false, NULL, ws, mc);
    }else{
      data = (RooDataSet*)ws->data(dataName);
      if (!data) {
        LOG(logERROR) << "Dataset: " << dataName << " doesn't exist!";
        exit(1);
      }
    }
    cout<<"start doing 4"<<endl;

    vector<RooRealVar*> pois;
    for (int i = 0; i < parsed.size(); i++) {
        RooRealVar* poi = (RooRealVar*)ws->var(parsed[i].c_str());
        LOG(logINFO) << "Getting POI " << poi->GetName();
        if (!poi) {
            LOG(logERROR) << "POI: " << poi->GetName() << " doesn't exist!";
            exit(1);
        }
        poi->setVal(1);
        poi->setRange(-5.,5.);
        poi->setConstant(0);
        pois.push_back(poi);
    }
    cout<<"start doing 5"<<endl;

    RooArgSet* nuis = (RooArgSet*)mc->GetNuisanceParameters();
    if (!nuis) {
        LOG(logERROR) << "Nuisance parameter set doesn't exist!";
        exit(1);
    }
    TIterator* itr = nuis->createIterator();
    RooRealVar* var;

    RooArgSet* globs = (RooArgSet*)mc->GetGlobalObservables();
    if (!globs) {
        LOG(logERROR) << "GetGlobal observables don't exist!";
        exit(1);
    }
        cout<<"start doing 6"<<endl;

    // collect nuisance parameters
    vector<string> vec_nuis;

    while ((var = (RooRealVar*)itr->Next())) {
        string varName(var->GetName());

        // skipping MC stat, lvlv pacman efficiencies
        if (varName.find("gamma_stat") != string::npos && varName.find("alpha") == string::npos) {
            LOG(logDEBUG) << "Skipping " << varName;
            continue;
        }

        // all remaining nuisance parameters
        LOG(logDEBUG) << "pushed back " << varName;
        vec_nuis.push_back(string(var->GetName()));
    }

    itr->Reset();
    int nrNuis = vec_nuis.size();

        cout<<"start doing 7"<<endl;

    // create nll and do unconditional fit
    // ws->loadSnapshot("ucmles");
    for (int i = 0; i < pois.size(); i++) {
    	pois[i]->setConstant(0);
        pois[i]->setRange(-5., 5.);
    }

    //int numCPU = sysconf( _SC_NPROCESSORS_ONLN );
    int numCPU = 1;
    if (useMinos) numCPU = 1; // Minos doesn't like our NLL with multiple cores
    RooNLLVar* nll = (RooNLLVar*)mc->GetPdf()->createNLL(*data, Constrain(*nuis), GlobalObservables(*globs), Offset(1), NumCPU(numCPU, RooFit::Hybrid));

    RooFitResult* fitresult; minimize(nll);
    double nll_hat = nll->getVal();
    vector<double> pois_hat;
    for (int i = 0; i < pois.size(); i++) {
        pois_hat.push_back(pois[i]->getVal());
    }

    // set all nuisance parameters floating and save snapshot
    while ((var = (RooRealVar*)itr->Next())) {
        var->setConstant(0);
    }
    itr->Reset();
    for (int i = 0; i < pois.size(); i++) {
        pois[i]->setConstant(0);
    }
    ws->saveSnapshot("tmp_snapshot", *mc->GetPdf()->getParameters(data));

        cout<<"start doing 8"<<endl;

    LOG(logDEBUG) << "Made unconditional snapshot";

    for (int in = 0; in < nrNuis; in++) {
        ws->loadSnapshot("tmp_snapshot");

        RooRealVar* nuip = (RooRealVar*)nuis->find(vec_nuis[in].c_str());
        string nuipName(nuip->GetName());

	cout<<" doing nuip name "<< nuipName <<endl;
        if (variable != NULL && nuipName != string(variable)) continue;

        // find all unconstrained NFs etc.
        bool isNorm = 0;
        if (nuipName.find("XS_Top") != string::npos) isNorm = true;
        if (nuipName.find("XS_Wjets") != string::npos) isNorm = true;
        // if (nuipName.find("ATLAS_norm") != string::npos) isNorm = 1;
        // if (nuipName.find("atlas_nbkg_") != string::npos) isNorm = true;
        // if (nuipName.find("slope_") != string::npos) isNorm = true;
        // if (nuipName.find("p0_") != string::npos) isNorm = true;
        // if (nuipName.find("p1_") != string::npos) isNorm = true;
        // if (nuipName.find("p2_") != string::npos) isNorm = true;
        // if (nuipName.find("p3_") != string::npos) isNorm = true;
        // if (nuipName.find("ATLAS_Hbb_norm_") != string::npos) isNorm = true;
        // if (nuipName.find("ATLAS_PM_EFF_") != string::npos) isNorm = true;
        if (nuipName.find("scale_") != string::npos && nuipName.find("QCDscale_") == string::npos) isNorm = true;
        if (nuipName.find("XS_") != string::npos && nuipName.find("alpha") == string::npos) isNorm = true;

        // setting all parameters but the one NF to look at constant at the best fit value when computing the error for the NF
        if (isNorm) {
            LOG(logDEBUG) << nuipName << " is unconstrained.";
            while ((var = (RooRealVar*)itr->Next())) {
                string varName(var->GetName());
                if (varName.find(nuipName) == string::npos) var->setConstant(1);
                LOG(logDEBUG) << varName << " is constant -> " << var->isConstant();
            }
            itr->Reset();
        }

        double nuip_hat = nuip->getVal();
        nuip->setConstant(0);

        ws->saveSnapshot("tmp_snapshot2", *mc->GetPdf()->getParameters(data));

        LOG(logDEBUG) << "Computing error for var " << nuip->GetName() << " at " << nuip->getVal();

        double nuip_errup;
        double nuip_errdown;

        if (useMinos) {
            RooArgSet* minosSet = new RooArgSet(*nuip);
            minimize(nll);
            nuip_errup = nuip->getErrorHi();
            nuip_errdown = nuip->getErrorLo();
        } else {
            ws->loadSnapshot("tmp_snapshot2");
            nuip_errup = findSigma(nll, nll_hat, nuip, nuip_hat+fabs(nuip->getErrorHi()), nuip_hat, +1, precision);
            ws->loadSnapshot("tmp_snapshot2");
            nuip_errdown = findSigma(nll, nll_hat, nuip, nuip_hat-fabs(nuip->getErrorLo()), nuip_hat, -1, precision);
        }

        LOG(logINFO) << nuip->GetName() << " = " << nuip_hat << " +" << fabs(nuip_errup) << " /  -" << fabs(nuip_errdown);

        // fix theta at the MLE value +/- postfit uncertainty and minimize again to estimate the change in the POI
        ws->loadSnapshot("tmp_snapshot2");
        nuip->setVal(nuip_hat+fabs(nuip_errup));
        nuip->setConstant(1);
        minimize(nll);
        vector<double> pois_up;
        for (int i = 0; i < pois.size(); i++) {
            pois_up.push_back(pois[i]->getVal());
        }

        ws->loadSnapshot("tmp_snapshot2");
        nuip->setVal(nuip_hat-fabs(nuip_errdown));
        nuip->setConstant(1);
        minimize(nll);
        vector<double> pois_down;
        for (int i = 0; i < pois.size(); i++) {
            pois_down.push_back(pois[i]->getVal());
        }

        // fix theta at the MLE value +/- postfit uncertainty and minimize again to estimate the change in the POI
        ws->loadSnapshot("tmp_snapshot2");
        nuip->setVal(nuip_hat+1.0);
        nuip->setConstant(1);
        minimize(nll);
        vector<double> pois_nom_up;
        for (int i = 0; i < pois.size(); i++) {
            pois_nom_up.push_back(pois[i]->getVal());
        }

        ws->loadSnapshot("tmp_snapshot2");
        nuip->setVal(nuip_hat-1.0);
        nuip->setConstant(1);
        minimize(nll);
        vector<double> pois_nom_down;
        for (int i = 0; i < pois.size(); i++) {
            pois_nom_down.push_back(pois[i]->getVal());
        }

        for (int i = 0; i < pois.size(); i++) {
            LOG(logINFO) << "Variation of " << pois[i]->GetName() << " = " << pois_up[i] << " (" << pois_nom_up[i] << ") / " << pois_down[i] << " (" << pois_nom_down[i] << ")";
        }

        // store result in root file
        stringstream fileName;
        fileName << "root-files/" << folder << "_pulls/" << nuipName << ".root";
        TFile fout(fileName.str().c_str(), "recreate");

        TH1D* h_out = new TH1D(nuipName.c_str(), nuipName.c_str(), 3 + 5 * pois.size(), 0, 3 + 5 * pois.size());

        h_out->SetBinContent(1, nuip_hat);
        h_out->SetBinContent(2, fabs(nuip_errup));
        h_out->SetBinContent(3, fabs(nuip_errdown));

        h_out->GetXaxis()->SetBinLabel(1, "nuip_hat");
        h_out->GetXaxis()->SetBinLabel(2, "nuip_up");
        h_out->GetXaxis()->SetBinLabel(3, "nuip_down");

        int bin = 4;
        for (int i = 0; i < pois.size(); i++) {
            h_out->SetBinContent(bin, pois_hat[i]);
            h_out->SetBinContent(bin+1, pois_up[i]);
            h_out->SetBinContent(bin+2, pois_down[i]);
            h_out->SetBinContent(bin+3, pois_nom_up[i]);
            h_out->SetBinContent(bin+4, pois_nom_down[i]);

            h_out->GetXaxis()->SetBinLabel(bin, pois[i]->GetName());
            h_out->GetXaxis()->SetBinLabel(bin+1, "poi_up");
            h_out->GetXaxis()->SetBinLabel(bin+2, "poi_down");
            h_out->GetXaxis()->SetBinLabel(bin+3, "poi_nom_up");
            h_out->GetXaxis()->SetBinLabel(bin+4, "poi_nom_down");

            bin += 5;
        }

        fout.Write();
        fout.Close();
    }

    //timer_pulls.Stop();
    //timer_pulls.Print();
}

RooDataSet* makeAsimovData(double mu_val, bool fluctuateData, string* mu_str, RooWorkspace* ws, ModelConfig* mc){

  ////////////////////
  //make asimov data//
  ////////////////////
  RooAbsPdf* combPdf = mc->GetPdf();
  
  int _printLevel = 1;

  stringstream muStr;
  muStr << setprecision(5);
  muStr << "_" << mu_val;
  if (mu_str) *mu_str = muStr.str();
  cout<<muStr.str()<<endl;

  stringstream muStrProf;

  RooRealVar* mu = (RooRealVar*)mc->GetParametersOfInterest()->first();//ws->var("mu");
  mu->setVal(mu_val);

  RooArgSet mc_obs = *mc->GetObservables();
  RooArgSet mc_globs = *mc->GetGlobalObservables();
  RooArgSet mc_nuis = *mc->GetNuisanceParameters();

  //pair the nuisance parameter to the global observable
  RooArgSet mc_nuis_tmp = mc_nuis;
  RooArgList nui_list("ordered_nuis");
  RooArgList glob_list("ordered_globs");
  RooArgSet constraint_set_tmp(*combPdf->getAllConstraints(mc_obs, mc_nuis_tmp, false));
  RooArgSet constraint_set;
  int counter_tmp = 0;
  unfoldConstraints(constraint_set_tmp, constraint_set, mc_obs, mc_nuis_tmp, counter_tmp);

  TIterator* cIter = constraint_set.createIterator();
  RooAbsArg* arg;
  while ((arg = (RooAbsArg*)cIter->Next())){
    RooAbsPdf* pdf = (RooAbsPdf*)arg;
    if (!pdf) continue;
    TIterator* nIter = mc_nuis.createIterator();
    RooRealVar* thisNui = NULL;
    RooAbsArg* nui_arg;
    while ((nui_arg = (RooAbsArg*)nIter->Next())){
      if (pdf->dependsOn(*nui_arg)){
	thisNui = (RooRealVar*)nui_arg;
	break;
      }
    }
    delete nIter;
    

    //need this in case the observable isn't fundamental. 
    //in this case, see which variable is dependent on the nuisance parameter and use that.
    RooArgSet* components = pdf->getComponents();
    components->remove(*pdf);
    if (components->getSize()){
      TIterator* itr1 = components->createIterator();
      RooAbsArg* arg1;
      while ((arg1 = (RooAbsArg*)itr1->Next())){
	TIterator* itr2 = components->createIterator();
	RooAbsArg* arg2;
	while ((arg2 = (RooAbsArg*)itr2->Next())){
	  if (arg1 == arg2) continue;
	  if (arg2->dependsOn(*arg1)){
	    components->remove(*arg1);
	  }
	}
	delete itr2;
      }
      delete itr1;
    }
    if (components->getSize() > 1){
      cout << "ERROR::Couldn't isolate proper nuisance parameter" << endl;
      return NULL;
    }
    else if (components->getSize() == 1){
      thisNui = (RooRealVar*)components->first();
    }
    
    TIterator* gIter = mc_globs.createIterator();
    RooRealVar* thisGlob = NULL;
    RooAbsArg* glob_arg;
    while ((glob_arg = (RooAbsArg*)gIter->Next())){
      if (pdf->dependsOn(*glob_arg)){
	thisGlob = (RooRealVar*)glob_arg;
	break;
      }
    }
    delete gIter;

    if (!thisNui || !thisGlob){
      cout << "WARNING::Couldn't find nui or glob for constraint: " << pdf->GetName() << endl;
      continue;
    }

    if (_printLevel >= 1) cout << "Pairing nui: " << thisNui->GetName() << ", with glob: " << thisGlob->GetName() << ", from constraint: " << pdf->GetName() << endl;

    nui_list.add(*thisNui);
    glob_list.add(*thisGlob);

  }
  delete cIter;
  



  //save the snapshots of nominal parameters, but only if they're not already saved
  ws->saveSnapshot("tmpGlobs",*mc->GetGlobalObservables());
  ws->saveSnapshot("tmpNuis",*mc->GetNuisanceParameters());
  if (!ws->loadSnapshot("nominalGlobs")){
    cout << "nominalGlobs doesn't exist. Saving snapshot." << endl;
    ws->saveSnapshot("nominalGlobs",*mc->GetGlobalObservables());
  }
  else ws->loadSnapshot("tmpGlobs");
  if (!ws->loadSnapshot("nominalNuis")){
    cout << "nominalNuis doesn't exist. Saving snapshot." << endl;
    ws->saveSnapshot("nominalNuis",*mc->GetNuisanceParameters());
  }
  else ws->loadSnapshot("tmpNuis");
    
  mu->setConstant(0);
  mu->setVal(mu_val);



  //loop over the nui/glob list, grab the corresponding variable from the tmp ws, and set the glob to the value of the nui
  int nrNuis = nui_list.getSize();
  if (nrNuis != glob_list.getSize()){
    cout << "ERROR::nui_list.getSize() != glob_list.getSize()!" << endl;
    return NULL;
  }
  
  for (int i=0;i<nrNuis;i++){
    RooRealVar* nui = (RooRealVar*)nui_list.at(i);
    RooRealVar* glob = (RooRealVar*)glob_list.at(i);
    
    cout << "nui: " << nui << ", glob: " << glob << endl;
    cout << "Setting glob: " << glob->GetName() << ", which had previous val: " << glob->getVal() << ", to conditional val: " << nui->getVal() << endl;
    
    glob->setVal(nui->getVal());
  }
  
  ws->loadSnapshot("nominalGlobs");
  ws->loadSnapshot("nominalNuis");

  if (_printLevel >= 1) cout << "Making asimov mu=" <<mu_val<< endl;

  //make the asimov data (snipped from Kyle)
  mu->setVal(mu_val);

  int iFrame=0;

  const char* weightName="weightVar";
  RooArgSet obsAndWeight;
  obsAndWeight.add(*mc->GetObservables());

  RooRealVar* weightVar = NULL;
  if (!(weightVar = ws->var(weightName))){
    ws->import(*(new RooRealVar(weightName, weightName, 1,0,10000000)));
    weightVar = ws->var(weightName);
  }
  obsAndWeight.add(*ws->var(weightName));
  ws->defineSet("obsAndWeight",obsAndWeight);


  //////////////////////////////////////////////////////
  // MAKE ASIMOV DATA FOR OBSERVABLES
  //////////////////////////////////////////////////////

  RooSimultaneous* simPdf = dynamic_cast<RooSimultaneous*>(mc->GetPdf());

  RooDataSet* asimovData;
  if (!simPdf)
  {
    // Get pdf associated with state from simpdf
    RooAbsPdf* pdftmp = mc->GetPdf();//simPdf->getPdf(channelCat->getLabel()) ;
	
    // Generate observables defined by the pdf associated with this state
    RooArgSet* obstmp = pdftmp->getObservables(*mc->GetObservables()) ;

    if (_printLevel >= 1) obstmp->Print();
    
    if( fluctuateData ){
      cout<<"Creating extended datasample"<<endl;
      asimovData = pdftmp->generate(RooArgSet(obsAndWeight),Extended(kTRUE));
    }else{
      asimovData = new RooDataSet(("asimovData"+muStr.str()).c_str(),("asimovData"+muStr.str()).c_str(),RooArgSet(obsAndWeight),WeightVar(*weightVar));
      
      RooRealVar* thisObs = ((RooRealVar*)obstmp->first());
      double expectedEvents = pdftmp->expectedEvents(*obstmp);
      double thisNorm = 0;
      for(int jj=0; jj<thisObs->numBins(); ++jj){
	thisObs->setBin(jj);
	
	thisNorm=pdftmp->getVal(obstmp)*thisObs->getBinWidth(jj);
	if (thisNorm*expectedEvents <= 0)
	  {
	    cout << "WARNING::Detected bin with zero expected events (" << thisNorm*expectedEvents << ") ! Please check your inputs. Obs = " << thisObs->GetName() << ", bin = " << jj << endl;
	  }
	if (thisNorm*expectedEvents > 0 && thisNorm*expectedEvents < pow(10.0, 18)) {
	  asimovData->add(*mc->GetObservables(), thisNorm*expectedEvents); 
	}
      }
    }
    if (_printLevel >= 1){
      asimovData->Print();
      cout <<"sum entries "<<asimovData->sumEntries()<<endl;
    }
    if(asimovData->sumEntries()!=asimovData->sumEntries()){
      cout << "sum entries is nan"<<endl;
      exit(1);
    }

    ws->import(*asimovData);

    if (_printLevel >= 1){
      asimovData->Print();
      cout << endl;
    }
  }
  
  else{
    map<string, RooDataSet*> asimovDataMap;
    
    //try fix for sim pdf
    RooCategory* channelCat = (RooCategory*)&simPdf->indexCat();
    TIterator* iter = channelCat->typeIterator() ;
    RooCatType* tt = NULL;
    int nrIndices = 0;
    while((tt=(RooCatType*) iter->Next())) {
      nrIndices++;
    }
    
    for (int i=0;i<nrIndices;i++){
      channelCat->setIndex(i);
      iFrame++;
      // Get pdf associated with state from simpdf
      RooAbsPdf* pdftmp = simPdf->getPdf(channelCat->getLabel()) ;
	
      // Generate observables defined by the pdf associated with this state
      RooArgSet* obstmp = pdftmp->getObservables(*mc->GetObservables()) ;

      if (_printLevel >= 1){
	obstmp->Print();
	cout << "on type " << channelCat->getLabel() << " " << iFrame << endl;
      }

      RooDataSet* obsDataUnbinned = new RooDataSet(Form("combAsimovData%d",iFrame),Form("combAsimovData%d",iFrame),RooArgSet(obsAndWeight,*channelCat),WeightVar(*weightVar));
      RooRealVar* thisObs = ((RooRealVar*)obstmp->first());
      double expectedEvents = pdftmp->expectedEvents(*obstmp);
      double thisNorm = 0;

      if( fluctuateData ){
	cout<<"Creating extended datasample"<<endl;
	obsDataUnbinned = pdftmp->generate(RooArgSet(obsAndWeight),Extended(kTRUE));
	obsDataUnbinned = pdftmp->generate(RooArgSet(obsAndWeight),Extended(kTRUE));
      }
      else{
	for(int jj=0; jj<thisObs->numBins(); ++jj){
	  thisObs->setBin(jj);
	  
	  thisNorm=pdftmp->getVal(obstmp)*thisObs->getBinWidth(jj);
	  if (thisNorm*expectedEvents > 0 && thisNorm*expectedEvents < pow(10.0, 18)) obsDataUnbinned->add(*mc->GetObservables(), thisNorm*expectedEvents);
	}
      }
      if (_printLevel >= 1){
	obsDataUnbinned->Print();
	cout <<"sum entries "<<obsDataUnbinned->sumEntries()<<endl;
      }
      if(obsDataUnbinned->sumEntries()!=obsDataUnbinned->sumEntries()){
	cout << "sum entries is nan"<<endl;
	exit(1);
      }

      asimovDataMap[string(channelCat->getLabel())] = obsDataUnbinned;

      if (_printLevel >= 1){
	cout << "channel: " << channelCat->getLabel() << ", data: ";
	obsDataUnbinned->Print();
	cout << endl;
      }
    }
    
    asimovData = new RooDataSet(("asimovData"+muStr.str()).c_str(),
				("asimovData"+muStr.str()).c_str(),
				RooArgSet(obsAndWeight,*channelCat),
				Index(*channelCat),
				Import(asimovDataMap),
				WeightVar(*weightVar));
    ws->import(*asimovData);
  }

  //bring us back to nominal for exporting
  ws->loadSnapshot("nominalGlobs");

  return asimovData;
}  


void unfoldConstraints(RooArgSet& initial, RooArgSet& final, RooArgSet& obs, RooArgSet& nuis, int& counter)
{
  if (counter > 50)
  {
    cout << "ERROR::Couldn't unfold constraints!" << endl;
    cout << "Initial: " << endl;
    initial.Print("v");
    cout << endl;
    cout << "Final: " << endl;
    final.Print("v");
    exit(1);
  }
  TIterator* itr = initial.createIterator();
  RooAbsPdf* pdf;
  while ((pdf = (RooAbsPdf*)itr->Next()))
  {
    RooArgSet nuis_tmp = nuis;
    RooArgSet constraint_set(*pdf->getAllConstraints(obs, nuis_tmp, false));
    //if (constraint_set.getSize() > 1)
    //{
    string className(pdf->ClassName());
    if (className != "RooGaussian" && className != "RooLognormal" && className != "RooGamma" && className != "RooPoisson" && className != "RooBifurGauss")
    {
      counter++;
      unfoldConstraints(constraint_set, final, obs, nuis, counter);
    }
    else
    {
      final.add(*pdf);
    }
  }
  delete itr;
}
