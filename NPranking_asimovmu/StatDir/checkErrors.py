import glob 
import re
import os
import math

StatErrors   = glob.glob('StatError*')

for StatError in StatErrors:
    sample_name = re.split('[._]', StatError)[1]
    print "Sample: " + sample_name

    MCStatError = "./MCStatError_" + sample_name + ".log"
    if not os.path.exists( MCStatError ):
        print "MCStatError log not exists for " + sample_name

    A1=0
    B1=0
    C1=0
    D1=0
    StatFile = open(StatError, 'r')
    iEntry=0
    for line in StatFile:
        if "mu\t" in line:
            temp_str0=line.split("\t")[1]
            temp_str1=line.split("\t")[2]
            value0=float( temp_str0.split("= ")[1] )
            value1=float( temp_str1.split("+/-")[1] )
            if(iEntry==0):
                A1=value0
                B1=value1
            elif(iEntry==1):
                C1=value0
                D1=value1
            else:
                print "ERROR!!!"
                break
            iEntry = iEntry + 1
    StatFile.close()




    A2=0
    B2=0
    C2=0
    D2=0
    MCStatFile = open(MCStatError, 'r')
    iEntry=0
    for line in MCStatFile:
        if "mu\t" in line:
            temp_str0=line.split("\t")[1]
            temp_str1=line.split("\t")[2]
            value0=float( temp_str0.split("= ")[1] )
            value1=float( temp_str1.split("+/-")[1] )
            if(iEntry==0):
                A2=value0
                B2=value1
            elif(iEntry==1):
                C2=value0
                D2=value1
            else:
                print "ERROR!!!"
                break
            iEntry = iEntry + 1
    MCStatFile.close()




    if(B1!=B2):
        print "ERROR1"
        print str(A1) + " +/- " + str(B1) + "\t" + str(C1) + " +/- " + str(D1)
        print str(A2) + " +/- " + str(B2) + "\t" + str(C2) + " +/- " + str(D2)
        continue
    
    print "Syst.      = " + str( math.sqrt(B1**2-D1**2)/C1 )
    print "Data stat. = " + str( D1/C1 )
    print "MC stat.   = " + str( math.sqrt(D2**2-D1**2)/C1 )
    print ""
