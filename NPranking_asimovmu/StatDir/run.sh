#!/bin/bash -f

declare -A upper_limit
upper_limit["HVTWW500"]=0.2118365
upper_limit["HVTWW2000"]=0.0010301
upper_limit["HVTWZ500"]=0.2471960
upper_limit["HVTWZ2000"]=0.0011553
upper_limit["VBF-HVTWW500"]=0.1679622
upper_limit["VBF-HVTWW1200"]=0.0053978
upper_limit["VBF-HVTWZ500"]=0.1555750
upper_limit["VBF-HVTWZ1200"]=0.0055314

test_mu=${upper_limit["HVTWW500"]}
root -l -b -q "./macros/checkStatErrorAsimov.C(\"/afs/cern.ch/work/t/tkunigo/public/ws_june25/HVTWW/ws_HPLP_500.root\", $test_mu)"       | tee StatError_HVTWW500.log &
test_mu=${upper_limit["HVTWZ500"]}
root -l -b -q "./macros/checkStatErrorAsimov.C(\"/afs/cern.ch/work/t/tkunigo/public/ws_june25/HVTWZ/ws_HPLP_500.root\", $test_mu)"        | tee StatError_HVTWZ500.log &
test_mu=${upper_limit["VBF-HVTWW500"]}
root -l -b -q "./macros/checkStatErrorAsimov.C(\"/afs/cern.ch/work/t/tkunigo/public/ws_june25/VBF-HVTWW/ws_HPLP_500.root\", $test_mu)"    | tee StatError_VBF-HVTWW500.log &
test_mu=${upper_limit["VBF-HVTWZ500"]}
root -l -b -q "./macros/checkStatErrorAsimov.C(\"/afs/cern.ch/work/t/tkunigo/public/ws_june25/VBF-HVTWZ/ws_HPLP_500.root\", $test_mu)"    | tee StatError_VBF-HVTWZ500.log &


test_mu=${upper_limit["VBF-HVTWW1200"]}
root -l -b -q "./macros/checkStatErrorAsimov.C(\"/afs/cern.ch/work/t/tkunigo/public/ws_june25/VBF-HVTWW/ws_HPLP_1200.root\", $test_mu)"    | tee StatError_VBF-HVTWW1200.log &
test_mu=${upper_limit["VBF-HVTWZ1200"]}
root -l -b -q "./macros/checkStatErrorAsimov.C(\"/afs/cern.ch/work/t/tkunigo/public/ws_june25/VBF-HVTWZ/ws_HPLP_1200.root\", $test_mu)"    | tee StatError_VBF-HVTWZ1200.log &
test_mu=${upper_limit["HVTWW2000"]}
root -l -b -q "./macros/checkStatErrorAsimov.C(\"/afs/cern.ch/work/t/tkunigo/public/ws_june25/HVTWW/ws_HPLP_2000.root\", $test_mu)"    | tee StatError_HVTWW2000.log &
test_mu=${upper_limit["HVTWZ2000"]}
root -l -b -q "./macros/checkStatErrorAsimov.C(\"/afs/cern.ch/work/t/tkunigo/public/ws_june25/HVTWZ/ws_HPLP_2000.root\", $test_mu)"    | tee StatError_HVTWZ2000.log &






test_mu=${upper_limit["HVTWW500"]}
root -l -b -q "./macros/checkMCStatErrorAsimov.C(\"/afs/cern.ch/work/t/tkunigo/public/ws_june25/HVTWW/ws_HPLP_500.root\", $test_mu)"       | tee MCStatError_HVTWW500.log &
test_mu=${upper_limit["HVTWZ500"]}
root -l -b -q "./macros/checkMCStatErrorAsimov.C(\"/afs/cern.ch/work/t/tkunigo/public/ws_june25/HVTWZ/ws_HPLP_500.root\", $test_mu)"        | tee MCStatError_HVTWZ500.log &
test_mu=${upper_limit["VBF-HVTWW500"]}
root -l -b -q "./macros/checkMCStatErrorAsimov.C(\"/afs/cern.ch/work/t/tkunigo/public/ws_june25/VBF-HVTWW/ws_HPLP_500.root\", $test_mu)"    | tee MCStatError_VBF-HVTWW500.log &
test_mu=${upper_limit["VBF-HVTWZ500"]}
root -l -b -q "./macros/checkMCStatErrorAsimov.C(\"/afs/cern.ch/work/t/tkunigo/public/ws_june25/VBF-HVTWZ/ws_HPLP_500.root\", $test_mu)"    | tee MCStatError_VBF-HVTWZ500.log &


test_mu=${upper_limit["VBF-HVTWW1200"]}
root -l -b -q "./macros/checkMCStatErrorAsimov.C(\"/afs/cern.ch/work/t/tkunigo/public/ws_june25/VBF-HVTWW/ws_HPLP_1200.root\", $test_mu)"    | tee MCStatError_VBF-HVTWW1200.log &
test_mu=${upper_limit["VBF-HVTWZ1200"]}
root -l -b -q "./macros/checkMCStatErrorAsimov.C(\"/afs/cern.ch/work/t/tkunigo/public/ws_june25/VBF-HVTWZ/ws_HPLP_1200.root\", $test_mu)"    | tee MCStatError_VBF-HVTWZ1200.log &
test_mu=${upper_limit["HVTWW2000"]}
root -l -b -q "./macros/checkMCStatErrorAsimov.C(\"/afs/cern.ch/work/t/tkunigo/public/ws_june25/HVTWW/ws_HPLP_2000.root\", $test_mu)"    | tee MCStatError_HVTWW2000.log &
test_mu=${upper_limit["HVTWZ2000"]}
root -l -b -q "./macros/checkMCStatErrorAsimov.C(\"/afs/cern.ch/work/t/tkunigo/public/ws_june25/HVTWZ/ws_HPLP_2000.root\", $test_mu)"    | tee MCStatError_HVTWZ2000.log &
