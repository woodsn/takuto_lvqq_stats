#!/bin/bash

#$(O)"(BWork space
# combined HP/LP
wsdir="/afs/cern.ch/work/t/tkunigo/public/ws_june25"

#$(O)"(BSignal: HVTWW, HVTWZ, RSGWW, ggHWWNWA, VBFWWNWA
analyses=("HVTWW")
masses=("2000")

declare -A upper_limit
upper_limit["HVTWW500"]=0.2118365
upper_limit["HVTWW2000"]=0.0010301
upper_limit["HVTWZ500"]=0.2471960
upper_limit["HVTWZ2000"]=0.0011553
upper_limit["VBF-HVTWW500"]=0.1679622
upper_limit["VBF-HVTWW1200"]=0.0053978
upper_limit["VBF-HVTWZ500"]=0.1555750
upper_limit["VBF-HVTWZ1200"]=0.0055314

for analysis in "${analyses[@]}"
do
    for mass in "${masses[@]}"
    do
	
#$(O)"(BWork space name
	ws="ws_HPLP_"${mass}".root"
	
	
	input_file=${wsdir}/${analysis}/${ws}
	folder=${analysis}"_"${mass}
	wsName="combined"
	dataName="asimovData"
	poiName="mu"
	
	
	
	echo "Running with--------------------"
	echo "             file: "${input_file}
	echo "           folder: "${folder}
	echo "           wsName: "${wsName}
	echo "         dataName: "${dataName}
	echo "          poiName: "${poiName}
	
	sample_name=$analysis$mass
	test_mu=${upper_limit[$sample_name]}
	echo $test_mu

	python runBatchPulls.py ${input_file} --folder ${folder} --workspaceName ${wsName} --dataName ${dataName} --poiName ${poiName} --compileRunPulls --testmu ${test_mu}
	done
done