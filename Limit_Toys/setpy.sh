## in order to us numpy
## https://twiki.atlas-canada.ca/bin/view/AtlasCanada/LocalSetupSFT2#pyanalysis_numpy_scipy_etc

## find available dependent packages
showVersions sft --cmtConfig=x86_64-slc6-gcc49-opt | grep -i -e pyanalysis -e lapack -e blas

## setup 
lsetup "sft --cmtconfig=x86_64-slc6-gcc49-opt releases/pyanalysis/1.5_python2.7-d641e,releases/lapack/3.5.0-57d41,releases/blas/20110419-e1974"
