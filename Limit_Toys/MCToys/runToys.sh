#!/bin/bash

mass=$1
np=$2
ntoys=$3
idseed=$4

tar -xzvf ZVtoy.tar.gz
# make clean
# make

python scripts/runToys.py -i ${mass}.root -l asymptotic_${mass}.root -n ${np} -t ${ntoys} -r ${idseed} --doBand