## setup grid evn before submission
#
#export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
#source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet
#lsetup panda
#lsetup rucio --quiet
#voms-proxy-init -voms atlas

sigs=(VBFH)
masses=$(seq 2100 200 3000)

for sig in ${sigs[*]};
do
  for mass in ${masses[*]}; do
    echo "Submit ${sig} ${mass}"
    ./submit_toyLimit.sh ${sig} ${mass}
  done
done
