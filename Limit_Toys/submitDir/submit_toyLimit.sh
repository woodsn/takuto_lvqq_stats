#!/bin/bash

toyDir=./MCToys
WSDir=/data/maxi178/zp/tnobe/inputs/workspaces_final/Combined
LimitsDir=/data/maxi178/zp/tnobe/inputs/limits_final/Combined_limits
thisdir=$(pwd)

sig=$1
mass=$2
DATE=`date +%Y%m%d`
official=0
Ver="nToy50k_nJob1k_np12_NoMake"
cleanup=1

if [[ -n $3 && $3 -ge 1 ]];
then
  official=1
fi

rootVer="6.06/02"
cmtConfig="x86_64-slc6-gcc49-opt"

output="Freq_CLs*.root"

nJobs=1000
np=12
ntoys=50

# Gathering files
tmpdir=../tmp/${sig}_${mass}
mkdir -p ${tmpdir}
for name in $thisdir/bin $thisdir/Makefile $thisdir/run.sh $thisdir/scripts $thisdir/setpy.sh $thisdir/setup.sh $thisdir/util runToys.sh;
do
  /bin/cp -nr $name ${tmpdir}/
done
cp $WSDir/$sig/ws_HPLP_${mass}.root ${tmpdir}/${mass}.root
cp $LimitsDir/$sig/${mass}.root ${tmpdir}/asymptotic_${mass}.root
cd ${tmpdir}
tar -czf ${thisdir}/ZVtoy.tar.gz *
cd $thisdir
# End of Gathering files

if [ $official -eq 1 ];
then
  prun \
    --outDS=group.phys-higgs.ZVtoy.${sig}.${mass}.${DATE}.${Ver} \
    --cmtConfig=$cmtConfig --rootVer=$rootVer --nJobs ${nJobs} --official \
    --skipScout --unlimitNumOutputs --long --exec="sh runToys.sh ${mass} ${np} ${ntoys} %RNDM:1" \
    --extFile ZVtoy.tar.gz,runToys.sh --outputs=$output
else
  prun \
    --outDS=user.$USER.ZVtoy.${sig}.${mass}.${DATE}.${Ver} \
    --cmtConfig=$cmtConfig --rootVer=$rootVer --nJobs ${nJobs} \
    --skipScout --unlimitNumOutputs --long --exec="sh runToys.sh ${mass} ${np} ${ntoys} %RNDM:1" \
    --extFile ZVtoy.tar.gz,runToys.sh --outputs=$output
fi

mv ${thisdir}/ZVtoy.tar.gz $tmpdir

if [ $cleanup -eq 1 ];
then
  rm -fr $tmpdir
fi
