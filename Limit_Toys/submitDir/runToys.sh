#!/bin/bash

sig=$1
mass=$2
np=$3
ntoys=$4
idseed=$5

tar -xzvf ZVtoy${sig}${mass}.tar.gz
make clean
make

python scripts/runToys.py -i ${mass}.root -l asymptotic_${mass}.root -n ${np} -t ${ntoys} -r ${idseed} --doBand
