## setup grid evn before submission
#
#export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
#source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet
#lsetup panda
#lsetup rucio --quiet
#voms-proxy-init -voms atlas


sigs=(RSG Kp5RSG)
#masses=( $(seq 2100 200 3000) 3200 3400 3600 3800 ) # The total that we need
#masses=( 3200 3400 3600 3800 ) # Josh has done this part
masses=$(seq 2100 200 3000)

official=0
if [[ -n $1 && $1 -ge 1 ]];
then
  official=1
fi

for sig in ${sigs[*]};
do
  for mass in ${masses[*]}; do
    echo "Submit ${sig} ${mass}"
    ./submit_toyLimit.sh ${sig} ${mass} ${official}
  done
done
