import os
import sys
import argparse

def parseArguments ():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--infile", dest="infile", required=True, help="input file")
    parser.add_argument("-l", "--limitfile", dest="limitfile", default="", help="asymptotic limit file")
    parser.add_argument("-n", "--npoints", dest="npoints", required=True, type=int, help="input file")
    parser.add_argument("-p", "--poimin", dest="poimin", default=-99.0, type=float, help="minimum poi value to scan")
    parser.add_argument("-q", "--poimax", dest="poimax", default=-99.0, type=float, help="maximum poi value to scan")
    parser.add_argument("-t", "--nToys", dest="nToys", required=True, type=int, help="nToys per worker")
    parser.add_argument("-w", "--nworkers", dest="nworkers", default=1, type=int, help="total number of workers")
    parser.add_argument("-o", "--outputDir", dest="outputDir", default=".", help="output directory for results")
    parser.add_argument("-r", "--randomseed", dest="randomseed", default=0, help="randomseed for job ID")
    parser.add_argument("--reuseAltToys", action="store_true", help="flag for reusing same toys for alt hypo")
    parser.add_argument("--doBand", action="store_true", help="flag for scanning expected +- 1/2 error band")

    return parser

if __name__=="__main__":

    parser = parseArguments()
    args = parser.parse_args()

    if not args.infile or not os.path.exists(args.infile):
        print "Input file not configured correctly!"
        sys.exit(1)

    if (args.poimin == -99.0 or args.poimax == -99.0):
        if not args.limitfile or not os.path.exists(args.limitfile):
            print "Please setup poi min and max values properly"
            print "You can either provide the asymptotic results (recommended) or set up the values manually"
            sys.exit(1)
        import ROOT
        f = ROOT.TFile.Open (args.limitfile)
        h = f.Get("stats")
        if not h:
            print "\"limit\" stats not found in the limit file provided!"
            sys.exit(1)
        h.GetEntry(0)
        obs_lim = h.obs_upperlimit
        exp_lim = h.exp_upperlimit
        exp_2up = h.exp_upperlimit_plus2
        exp_1up = h.exp_upperlimit_plus1
        exp_1down = h.exp_upperlimit_minus1
        exp_2down = h.exp_upperlimit_minus2
        args.poimin = min([obs_lim, exp_lim, exp_1down]) * 0.8
        args.poimax = max([obs_lim, exp_lim, exp_1up]) * 1.2
        if args.doBand:
          args.poimin = min([obs_lim, exp_lim, exp_2down]) * 0.5
          args.poimax = max([obs_lim, exp_lim, exp_2up]) * 1.5

    arguments = "-i {0} -n {1} -p {2} -q {3} -t {4} -s Job{5} -o {6} -r {7}".format(
            args.infile, args.npoints, args.poimin, args.poimax, args.nToys, args.randomseed, args.outputDir, 0)

    mass=args.infile.split('/')[-1].split('.root')[0]
    cmd="./bin/runToys.exe "+arguments
    print cmd
    os.system(cmd)
