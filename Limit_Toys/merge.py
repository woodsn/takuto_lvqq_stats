#!/usr/bin/python -f

import os
from os import listdir
from os.path import isfile, join

dirs = [f for f in listdir("./") if not isfile(join("../", f))]

for dir_name in dirs:
    sig_name = dir_name.split(".")[3]
    mass     = dir_name.split(".")[4]
    command_line = "root -b -q 'AnalyzeResults.C(\""+sig_name+"\", " + mass +")'"
    print command_line
    os.system(command_line)
