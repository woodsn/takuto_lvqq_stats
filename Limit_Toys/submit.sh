#!/bin/bash -f

samples=("ggHWWNWA" "HVTWW" "HVTWZ" "RSGWW" "RSGWW-SmReW" "VBFWWNWA" "VBF-HVTWW" "VBF-HVTWZ")
masses_Scalar=(300 400 500 600 700 800 900 1000 1200 1400 1600 1800 2000 2200 2400 2600 2800 3000)
masses_HVT=(300 400 500 600 700 800 900 1000 1100 1200 1300 1400 1500 1600 1700 1800 1900 2000 2200 2400 2600 2800 3000 3500 4000 4500 5000)
masses_VBFHVT=(300 400 500 600 700 800 900 1000 1100 1200 1300 1400 1500 1600 1700 1800 1900 2000 2200 2400 2600 2800 3000 3500 4000)

for sample in "${samples[@]}"
do
    if echo $sample | grep 'NWA'; then
	masses=("${masses_Scalar[@]}")
    elif echo $sample | grep 'VBF'; then
     	masses=("${masses_VBFHVT[@]}")
    else
	masses=("${masses_HVT[@]}")
    fi

    for mass in "${masses[@]}"
    do
	./submitDir/submit_toyLimit.sh $sample $mass	
    done
done
