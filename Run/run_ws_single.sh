#!/bin/bash

HVT_signal=( "HVTWW" )
HVT_mass=( "1000" )
#purity=( "HP" "LP" )
purity=( "HP" )

for pr in "${purity[@]}"
do
    for mass in "${HVT_mass[@]}"
    do
	for signal in "${HVT_signal[@]}"
	do
	    printf "\nProducing workspace for signal: "$signal"; mass: "$mass" GeV; purity: "$pr
	    printf "\nLog stored in: log_ws_"$signal"_"$mass"_"$pr".txt\n" 
	    python ../ResonanceFinder/python/lvqq_Main.py $signal $mass $pr > log_ws_${signal}_${mass}_${pr}.txt 
	done
    done
done
