import condor, time, os, commands,sys
timestr = time.strftime("%m%d%Y_%H%M")

#signal = ["ggHWWNWA","HVTWW", "HVTWZ", "RSGWW","VBFWWNWA", "VBF_HVTWW", "VBF_HVTWZ"]
signal = ["HVTWW", "VBFWWNWA"]
#mass = "700"
#mass_higgs = ["700","600"]
mass_higgs = ["500","600","700","800","900","1000","1200","1400","1600", "1800", "2000", "2200", "2400", "2600", "2800", "3000"]
#mass_higgs = ["1600", "1800", "2000", "2200", "2400", "2600", "2800", "3000"]
#mass_HVT = [ "700", "8003"]
mass_HVT = ["500","600","700","800","900","1000","1100","1200","1300","1400","1500", "1600", "1700", "1800", "1900", "2000", "2200", "2400", "2600", "2800", "3000", "3500", "4000", "4500", "5000"]
#mass_HVT = ["300","400","500","600","700","800","900","1000","1100","1200","1300","1400","1500", "1600", "1700", "1800", "1900", "2000", "2200", "2400", "2600", "2800", "3000", "3500", "4000", "4500", "5000"]
mass_RSG = ["300","400","500","600","700","800","900","1000","1100","1200","1300","1400","1500"]
#purity = ["RES","RESVBF"]
purity = "boosted"
purity2="boostedVBF"
exe = 'run_ws_example.sh'
for s in signal:
	if s=="VBF-HVTWW" or s=="VBF-HVTWZ":
		for m in mass_HVT:

			directory = '/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/Run/test_run/VVlvqq_mc15_v2016/data/itest01/'
			files = commands.getoutput('ls /export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/Run/dummy.txt')
			files = files.split('\n')
			dirname = 'ws_'+s+m+purity2+timestr
			argtemplate = ' %s %s %s %s'%(s,m,purity2,files)
			for i, iFile in enumerate(files):
			    files[i] = iFile
			    dirname = 'ws_' +s+'_'+m+'_'+purity2+'_'+timestr
			    print iFile
			print argtemplate
			condor.run(exe,argtemplate,files, dirname, 1)
	if s=="HVTWW" or s=="HVTWZ":
		for m in mass_HVT:

			directory = '/export/share/gauss/woodsn/takuto_lvqq/StatToolsWVlvqq/Run/data/itest01/'
			files = commands.getoutput('ls /export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/Run/dummy.txt')
			files = files.split('\n')
			dirname = 'ws_'+s+m+purity+timestr
			argtemplate = ' %s %s %s'%(s,m,files)
			for i, iFile in enumerate(files):
			    files[i] = iFile
			    dirname = 'ws_' +s+'_'+m+'_'+'_'+timestr
			    print iFile
			print argtemplate
			condor.run(exe,argtemplate,files, dirname, 1)
	if s == "RSGWW":
		for m in mass_RSG:

			directory = '/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/Run/test_run/VVlvqq_mc15_v2016/data/itest01/'
			files = commands.getoutput('ls /export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/Run/dummy.txt')
			files = files.split('\n')
			dirname = 'ws_'+s+m+purity+timestr
			argtemplate = ' %s %s %s %s'%(s,m,purity,files)
			for i, iFile in enumerate(files):
			    files[i] = iFile
			    dirname = 'ws_' +s+'_'+m+'_'+purity+'_'+timestr
			    print iFile
			print argtemplate
			condor.run(exe,argtemplate,files, dirname, 1)
	if s == "ggHWWNWA":
		for m in mass_higgs:

			directory = '/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/Run/test_run/VVlvqq_mc15_v2016/data/itest01/'
			files = commands.getoutput('ls /export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/Run/dummy.txt')
			files = files.split('\n')
			dirname = 'ws_'+s+m+purity+timestr
			argtemplate = ' %s %s %s %s'%(s,m,purity,files)
			for i, iFile in enumerate(files):
			    files[i] = iFile
			    dirname = 'ws_' +s+'_'+m+'_'+purity+'_'+timestr
			    print iFile
			print argtemplate
			condor.run(exe,argtemplate,files, dirname, 1)
	if s =="VBFWWNWA":
		for m in mass_higgs:

			directory = '/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/Run/test_run/VVlvqq_mc15_v2016/data/itest01/'
			files = commands.getoutput('ls /export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/Run/dummy.txt')
			files = files.split('\n')
			dirname = 'ws_'+s+m+purity2+timestr
			argtemplate = ' %s %s %s %s'%(s,m,purity2,files)
			for i, iFile in enumerate(files):
			    files[i] = iFile
			    dirname = 'ws_' +s+'_'+m+'_'+purity2+'_'+timestr
			    print iFile
			print argtemplate
			condor.run(exe,argtemplate,files, dirname, 1)
