#!/bin/bash

MYPWD=`pwd`
source $MYPWD/../rcSetup.sh ""

signal=$1
mass=$2
ws="../ws_combined"

printf "\nProducing Limits for signal: "$signal"; mass: "$mass" GeV"
printf "\nLog stored in: log_limit_"$signal"_"$mass".txt\n" 
python getLimit.py $signal $mass $ws | tee log_limit_${signal}_${mass}.txt
