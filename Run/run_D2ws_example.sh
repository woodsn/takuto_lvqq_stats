#!/bin/bash

MYPWD=`pwd`
source $MYPWD/../rcSetup.sh ""

signal="HVTWW"
masses=( "1000" )
purity=( "comb" )

for pr in "${purity[@]}"
do
    for mass in "${masses[@]}"
    do
	printf "\nProducing workspace for signal: "$signal"; mass: "$mass" GeV; purity: "$pr
	python ../ResonanceFinder/python/lvqq_Main_comb.py $signal $mass $pr | tee log_Combws_${signal}_${mass}_${pr}.txt &
	python ../ResonanceFinder/python/lvqq_Main_D2.py $signal $mass $pr | tee log_D2ws_${signal}_${mass}_${pr}.txt &
    done
done
