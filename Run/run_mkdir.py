#!/usr/bin/python


#___________________________________________________________________________
def createDirectoryStructure(input_dir, releaseDir, analysis, inputListTag):
  import os
  import shutil
  import glob

  # create directories
  newdir = '{releaseDir}/{analysis}/data/{inputListTag}'.format(releaseDir=releaseDir, analysis=analysis, inputListTag=inputListTag)
  diagnosticsdir = '{releaseDir}/{analysis}/ws/diagnostics'.format(releaseDir=releaseDir, analysis=analysis ) 
  tmpdir = '{releaseDir}/{analysis}/tmp'.format(releaseDir=releaseDir, analysis=analysis)

  # first, we remove the previously existing directories
  dirs = [newdir, diagnosticsdir, tmpdir]
  for dir in dirs:
    if os.path.exists(dir):
      shutil.rmtree(dir)

  # we use makedirs, which takes care of creating intermediate directories
  os.makedirs(newdir)
  os.makedirs(diagnosticsdir)
  os.makedirs(tmpdir)
  
  # copy input trees from input location
  files = glob.glob(os.path.join(input_dir, '*.*'))
  ### commented out
   
   
#_________________________
if __name__ == '__main__':
 
  releaseDir   = './test_run'
  analysis     = 'WVlvqq_mc15_v28'
  inputListTag = 'itest01'
  
  print "Creating directory structure"
  createDirectoryStructure('/afs/cern.ch/work/r/rcarbone/public/forStats/inputs/', releaseDir, analysis, inputListTag)

