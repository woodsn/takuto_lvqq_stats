#!/bin/bash

#MYPWD=`pwd`
source /export/share/gauss/woodsn/takuto_lvqq/StatToolsWVlvqq/rcSetup.sh

signal=$1 #HVTWW RSGWW ggHWW VBFWW
mass=$2 # mass point e.g. 500
purity=( $3 ) # RES or RESVBF

for pr in "${purity[@]}"
do
	printf "\nProducing workspace for signal: "$signal"; mass: "$mass" GeV; purity: "$pr
	printf "\nLog stored in: log_ws_"$signal"_"$mass"_"$pr".txt\n" 
	python /export/share/gauss/woodsn/takuto_lvqq/StatToolsWVlvqq/ResonanceFinder/python/lvqq_Resolved.py $signal $mass $pr >& log_ws_${signal}_${mass}_${pr}.txt
done
