import math
import ROOT

#fill in:

p0Values=[0.5 ,0.5 ,0.387409 ,0.00271037 ,0.0634845 ,0.282291 ,0.5 ,0.5 ,0.5 ,0.5 ,0.5 ,0.5 ,0.5 ,0.5 ,0.5 ,0.5 ,0.5 ,0.5 ,0.5 ,0.290822 ,0.073811 ,0.328043 ,0.5 ,0.5,0.5]
q0Values=[0,0,1,5,3,1,0,0,0,0,0,0,0,0,0,0,0,0,0,2,4,3,0,0,0]



#p_val = (1-TMath::Erf(sqrt(q_0/2)))/2
#2*(Erf^-1((1-2*p)))^2
minP0 = min(p0Values)
maxSig = ROOT.Math.gaussian_quantile(1-minP0, 1)
print 'minimum local p0 = ' + str(minP0)
print 'corresponds to local significance of ' + str(maxSig) + 'sigma'


#print 'p0Values = ',p0Values
#print 'q0Values = ',q0Values

#calculate the number of upcrossings at a reference point
c0 = 0.1
below = True
nUpCross = 0
maxq0 = -99.
for q in q0Values:
  if q > maxq0:
    maxq0 = q
  if below:
    if q > c0:
      nUpCross += 1
      below = False
  else:
    if q < c0:
      below = True

print 'number of up-crossings at reference q=' + str(c0) + ', is ' + str(nUpCross)
corr = float(nUpCross) * math.exp(-((maxq0 - c0)/2.))
globSig = math.fabs(ROOT.Math.gaussian_quantile(1-(minP0+corr), 1))

print 'correction for LEE is  ',corr
print 'trials factor is (minP0+corr/minP0) ' + str((minP0+corr)/minP0)
print 'global p value is ' + str(minP0 + corr)
print 'corresponding to a global significance of ' + str(globSig) + ' sigma'
