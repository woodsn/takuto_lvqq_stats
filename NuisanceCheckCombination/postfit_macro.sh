#!/bin/bash
#does this change
mkdir -p plots
cd plots
mkdir -p HVTWW HVTWZ VBF-HVTWW VBF-HVTWZ
cd ..

#--------HVTWW------------------------------------------------

#----------HVTWW 500 GeV Resolved--------------
rootfile="\"./results_HVTWW500/FitCrossChecks.root\""
signal="\"HVT Model A Z\264 500 GeV\""
xsec="1.93" #HVTWW 500 GeV

root -l -b <<EOF
.L drawPostFit.C
drawPostFit("SRWW_Resolved_fileThree",$rootfile,$signal,$xsec)
drawPostFit("Sideband_Resolved_fileThree",$rootfile,$signal,$xsec)
drawPostFit("Btag_Resolved_fileThree",$rootfile,$signal,$xsec)
.q
EOF

----------HVTWW 2000 GeV Boosted -------------
xsec="0.003612549743" #HVTWW 2TeV
rootfile="\"./results_HVTWW2000/FitCrossChecks.root\""
signal="\"HVT Model A Z\264 2000 GeV\""
root -l -b <<EOF
.L drawPostFit.C
drawPostFit("SRWW_fileOne",$rootfile,$signal,$xsec)
drawPostFit("Sideband_fileOne",$rootfile,$signal,$xsec)
drawPostFit("Btag_fileOne",$rootfile,$signal,$xsec)
drawPostFit("SRWW_LP_fileTwo",$rootfile,$signal,$xsec)
drawPostFit("Sideband_LP_fileTwo",$rootfile,$signal,$xsec)
drawPostFit("Btag_LP_fileTwo",$rootfile,$signal,$xsec)
.q
EOF

mv *eps plots/HVTWW
mv *pdf plots/HVTWW
mv *png plots/HVTWW
mv *root plots/HVTWW
mv *file*.C plots/HVTWW



#---------------------------------------------------------------------------------------------

#-----------------HVTWZ--------------------------
#----------HVTWZ 500 GeV Resolved--------------
xsec="2.11" #HVTWZ 500 GeV
rootfile="\"./results_HVTWZ500/FitCrossChecks.root\""
signal="\"HVT Model A W\264 500 GeV\""

root -l -b <<EOF
.L drawPostFit.C
drawPostFit("SRWZ_Resolved_fileThree",$rootfile,$signal,$xsec)
drawPostFit("Sideband_Resolved_fileThree",$rootfile,$signal,$xsec)
drawPostFit("Btag_Resolved_fileThree",$rootfile,$signal,$xsec)
.q
EOF

#----------HVTWZ 2000 GeV Boosted -------------
xsec="0.00406" #HVTWZ 2TeV
rootfile="\"./results_HVTWZ2000/FitCrossChecks.root\""
signal="\"HVT Model A W\264 2000 GeV\""
root -l -b <<EOF
.L drawPostFit.C
drawPostFit("SRWZ_fileOne",$rootfile,$signal,$xsec)
drawPostFit("Sideband_fileOne",$rootfile,$signal,$xsec)
drawPostFit("Btag_fileOne",$rootfile,$signal,$xsec)
drawPostFit("SRWZ_LP_fileTwo",$rootfile,$signal,$xsec)
drawPostFit("Sideband_LP_fileTwo",$rootfile,$signal,$xsec)
drawPostFit("Btag_LP_fileTwo",$rootfile,$signal,$xsec)
.q
EOF


mv *eps plots/HVTWZ
mv *pdf plots/HVTWZ
mv *png plots/HVTWZ
mv *file*.C plots/HVTWZ

#---------------------------------------------------------------------------------------------

#-------VBF HVTWW-------------------------------
xsec="0.005033" #VBF HVTWW 500 GeV

rootfile="\"./results_VBF-HVTWW500/FitCrossChecks.root\""
signal="\"HVT VBF Model Z\264 500 GeV\""

root -l -b <<EOF

.L drawPostFit.C

drawPostFit("SRWW_ResolvedVBF_fileThree",$rootfile,$signal,$xsec)
drawPostFit("Sideband_ResolvedVBF_fileThree",$rootfile,$signal,$xsec)
drawPostFit("Btag_ResolvedVBF_fileThree",$rootfile,$signal,$xsec)

.q

EOF

#----------VBF HVTWW 1200 GeV Boosted -------------
xsec="0.000125" #VBFHVTWW 1.2TeV
rootfile="\"./results_VBF-HVTWW1200/FitCrossChecks.root\""
signal="\"HVT VBF Model Z\264 1200 GeV\""
root -l -b <<EOF
.L drawPostFit.C
drawPostFit("SRWW_fileOne",$rootfile,$signal,$xsec)
drawPostFit("Sideband_fileOne",$rootfile,$signal,$xsec)
drawPostFit("Btag_fileOne",$rootfile,$signal,$xsec)
drawPostFit("SRWW_LP_fileTwo",$rootfile,$signal,$xsec)
drawPostFit("Sideband_LP_fileTwo",$rootfile,$signal,$xsec)
drawPostFit("Btag_LP_fileTwo",$rootfile,$signal,$xsec)
.q
EOF

mv *eps plots/VBF-HVTWW
mv *pdf plots/VBF-HVTWW
mv *png plots/VBF-HVTWW
mv *file*.C plots/VBF-HVTWW

#---------------------------------------------------------------------------------------------


#-------VBF HVTWZ-------------------------------

#--------Resolved 500GeV---------------
xsec="0.0041372" #VBF HVTWZ 500 GeV

rootfile="\"./results_VBF-HVTWZ500/FitCrossChecks.root\""
signal="\"HVT VBF Model W\264 500 GeV\""

root -l -b <<EOF

.L drawPostFit.C

drawPostFit("SRWZ_ResolvedVBF_fileThree",$rootfile,$signal,$xsec)
drawPostFit("Sideband_ResolvedVBF_fileThree",$rootfile,$signal,$xsec)
drawPostFit("Btag_ResolvedVBF_fileThree",$rootfile,$signal,$xsec)

.q

EOF

#----------VBF HVTWZ 1200 GeV Boosted -------------
xsec="0.000102459" #VBFHVTWZ 1.2TeV
rootfile="\"./results_VBF-HVTWZ1200/FitCrossChecks.root\""
signal="\"HVT VBF Model W\264 1200 GeV\""
root -l -b <<EOF
.L drawPostFit.C
drawPostFit("SRWZ_fileOne",$rootfile,$signal,$xsec)
drawPostFit("Sideband_fileOne",$rootfile,$signal,$xsec)
drawPostFit("Btag_fileOne",$rootfile,$signal,$xsec)
drawPostFit("SRWZ_LP_fileTwo",$rootfile,$signal,$xsec)
drawPostFit("Sideband_LP_fileTwo",$rootfile,$signal,$xsec)
drawPostFit("Btag_LP_fileTwo",$rootfile,$signal,$xsec)
.q
EOF

mv *pdf plots/VBF-HVTWZ
mv *png plots/VBF-HVTWZ
mv *file*.C plots/VBF-HVTWZ
mv *eps plots/VBF-HVTWZ
