class SignalUncertainty:
    def __init__(self):
        self.Name = ""
        self.errPDF = []
        self.errISRFSRHP = []
        self.errISRFSRLP = []
        self.errISRFSRResolved = []

ggHWWNWA = SignalUncertainty()
ggHWWNWA.Name = "ggHWWNWA"
ggHWWNWA.errPDF = {
    300 : 0.005,
    400 : 0.005,
    500 : 0.005,
    600 : 0.005,
    700 : 0.005,
    750 : 0.005,
    800 : 0.005,
    900 : 0.005,
    1000 : 0.005,
    1200 : 0.005,
    1400 : 0.005,
    1600 : 0.005,
    1800 : 0.005,
    2000 : 0.005,
    2200 : 0.005,
    2400 : 0.005,
    2600 : 0.005,
    2800 : 0.005,
    3000 : 0.005
}
ggHWWNWA.errISRFSRHP = {
    500 : 0.15,
    600 : 0.14,
    700 : 0.12,
    750 : 0.11,
    800 : 0.10,
    900 : 0.09,
    1000 : 0.07,
    1200 : 0.06,
    1400 : 0.04,
    1600 : 0.04,
    1800 : 0.04,
    2000 : 0.04,
    2200 : 0.04,
    2400 : 0.04,
    2600 : 0.04,
    2800 : 0.04,
    3000 : 0.04
}
ggHWWNWA.errISRFSRLP = {
    500 : -0.15,
    600 : -0.14,
    700 : -0.12,
    750 : -0.11,
    800 : -0.10,
    900 : -0.09,
    1000 : -0.07,
    1200 : -0.06,
    1400 : -0.04,
    1600 : -0.04,
    1800 : -0.04,
    2000 : -0.04,
    2200 : -0.04,
    2400 : -0.04,
    2600 : -0.04,
    2800 : -0.04,
    3000 : -0.04
}
ggHWWNWA.errISRFSRResolved = {
    300 : -0.10,
    400 : -0.08,
    500 : -0.06,
    600 : -0.06,
    700 : -0.06,
    750 : -0.06,
    800 : -0.06,
    900 : -0.07,
    1000 : -0.09,
    1200 : -0.15,
    1400 : -0.25
}




HVT = SignalUncertainty()
HVT.Name = "HVT"
HVT.errPDF = {
    300 : 0.02,
    400 : 0.02,
    500 : 0.02,
    600 : 0.02,
    650 : 0.02,
    700 : 0.02,
    800 : 0.02,
    900 : 0.02,
    1000 : 0.02,
    1100 : 0.02,
    1200 : 0.02,
    1300 : 0.02,
    1400 : 0.02,
    1500 : 0.02,
    1600 : 0.02,
    1700 : 0.02,
    1800 : 0.02,
    1900 : 0.02,
    2000 : 0.02,
    2200 : 0.02,
    2400 : 0.02,
    2600 : 0.02,
    2800 : 0.02,
    3000 : 0.02,
    3500 : 0.02,
    4000 : 0.02,
    4500 : 0.02,
    5000 : 0.02
}
HVT.errISRFSRHP = {
    500 : 0.08,
    600 : 0.08,
    650 : 0.08,
    700 : 0.07,
    800 : 0.07,
    900 : 0.06,
    1000 : 0.06,
    1100 : 0.06,
    1200 : 0.05,
    1300 : 0.05,
    1400 : 0.05,
    1500 : 0.04,
    1600 : 0.04,
    1700 : 0.04,
    1800 : 0.04,
    1900 : 0.04,
    2000 : 0.03,
    2200 : 0.03,
    2400 : 0.03,
    2600 : 0.03,
    2800 : 0.03,
    3000 : 0.03,
    3500 : 0.03,
    4000 : 0.03,
    4500 : 0.03,
    5000 : 0.03
}
HVT.errISRFSRLP = {
    500 : -0.08,
    600 : -0.08,
    650 : -0.08,
    700 : -0.07,
    800 : -0.07,
    900 : -0.06,
    1000 : -0.06,
    1100 : -0.06,
    1200 : -0.05,
    1300 : -0.05,
    1400 : -0.05,
    1500 : -0.04,
    1600 : -0.04,
    1700 : -0.04,
    1800 : -0.04,
    1900 : -0.04,
    2000 : -0.03,
    2200 : -0.03,
    2400 : -0.03,
    2600 : -0.03,
    2800 : -0.03,
    3000 : -0.03,
    3500 : -0.03,
    4000 : -0.03,
    4500 : -0.03,
    5000 : -0.03
}
HVT.errISRFSRResolved = {
    300 : -0.12,
    400 : -0.10,
    500 : -0.08,
    600 : -0.07,
    650 : -0.07,
    700 : -0.07,
    750 : -0.07,
    800 : -0.07,
    900 : -0.08,
    1000 : -0.10,
    1100 : -0.13,
    1200 : -0.17,
    1300 : -0.21,
    1400 : -0.26
}



RSG = SignalUncertainty()
RSG.Name = "RSG"
RSG.errPDF = {
    300 : 0.005,
    400 : 0.005,
    500 : 0.005,
    600 : 0.005,
    700 : 0.005,
    800 : 0.005,
    900 : 0.005,
    1000 : 0.005,
    1100 : 0.005,
    1200 : 0.005,
    1300 : 0.005,
    1400 : 0.005,
    1500 : 0.005,
    1600 : 0.005,
    1700 : 0.005,
    1800 : 0.005,
    1900 : 0.005,
    2000 : 0.005,
    2200 : 0.005,
    2400 : 0.005,
    2600 : 0.005,
    2800 : 0.005,
    3000 : 0.005,
    3500 : 0.005,
    4000 : 0.005,
    4500 : 0.005,
    5000 : 0.005
}
RSG.errISRFSRHP = {
    500 : 0.15,
    600 : 0.14,
    700 : 0.12,
    800 : 0.10,
    900 : 0.09,
    1000 : 0.07,
    1100 : 0.06,
    1200 : 0.06,
    1300 : 0.05,
    1400 : 0.04,
    1500 : 0.04,
    1600 : 0.04,
    1700 : 0.04,
    1800 : 0.04,
    1900 : 0.04,
    2000 : 0.04,
    2200 : 0.04,
    2400 : 0.04,
    2600 : 0.04,
    2800 : 0.04,
    3000 : 0.04,
    3500 : 0.04,
    4000 : 0.04,
    4500 : 0.04,
    5000 : 0.04
}
RSG.errISRFSRLP = {
    500 : -0.15,
    600 : -0.14,
    700 : -0.12,
    800 : -0.10,
    900 : -0.09,
    1000 : -0.07,
    1100 : -0.06,
    1200 : -0.06,
    1300 : -0.05,
    1400 : -0.04,
    1500 : -0.04,
    1600 : -0.04,
    1700 : -0.04,
    1800 : -0.04,
    1900 : -0.04,
    2000 : -0.04,
    2200 : -0.04,
    2400 : -0.04,
    2600 : -0.04,
    2800 : -0.04,
    3000 : -0.04,
    3500 : -0.04,
    4000 : -0.04,
    4500 : -0.04,
    5000 : -0.04
}
RSG.errISRFSRResolved = {
    300 : -0.10,
    400 : -0.08,
    500 : -0.06,
    600 : -0.06,
    700 : -0.06,
    750 : -0.06,
    800 : -0.06,
    900 : -0.07,
    1000 : -0.09,
    1100 : -0.10,
    1200 : -0.15,
    1300 : -0.18,
    1400 : -0.25
}


VBFWWNWA = SignalUncertainty()
VBFWWNWA.Name = "VBFWWNWA"
VBFWWNWA.errPDF = {
    300 : 0.01,
    400 : 0.01,
    500 : 0.01,
    600 : 0.01,
    700 : 0.01,
    750 : 0.01,
    800 : 0.01,
    900 : 0.01,
    1000 : 0.01,
    1200 : 0.01,
    1400 : 0.01,
    1600 : 0.01,
    1800 : 0.01,
    2000 : 0.01,
    2200 : 0.01,
    2400 : 0.01,
    2600 : 0.01,
    2800 : 0.01,
    3000 : 0.01
}
VBFWWNWA.errISRFSRHP = {
    500 : 0.08,
    600 : 0.08,
    700 : 0.07,
    750 : 0.07,
    800 : 0.07,
    900 : 0.06,
    1000 : 0.06,
    1100 : 0.06,
    1200 : 0.05,
    1300 : 0.05,
    1400 : 0.05,
    1500 : 0.04,
    1600 : 0.04,
    1700 : 0.04,
    1800 : 0.04,
    1900 : 0.04,
    2000 : 0.03,
    2200 : 0.03,
    2400 : 0.03,
    2600 : 0.03,
    2800 : 0.03,
    3000 : 0.03,
    3500 : 0.03,
    4000 : 0.03,
    4500 : 0.03,
    5000 : 0.03
}
VBFWWNWA.errISRFSRLP = {
    500 : -0.08,
    600 : -0.08,
    700 : -0.07,
    750 : -0.07,
    800 : -0.07,
    900 : -0.06,
    1000 : -0.06,
    1100 : -0.06,
    1200 : -0.05,
    1300 : -0.05,
    1400 : -0.05,
    1500 : -0.04,
    1600 : -0.04,
    1700 : -0.04,
    1800 : -0.04,
    1900 : -0.04,
    2000 : -0.03,
    2200 : -0.03,
    2400 : -0.03,
    2600 : -0.03,
    2800 : -0.03,
    3000 : -0.03,
    3500 : -0.03,
    4000 : -0.03,
    4500 : -0.03,
    5000 : -0.03
}
VBFWWNWA.errISRFSRResolved = {
    300 : -0.07,
    400 : -0.06,
    500 : -0.05,
    600 : -0.05,
    700 : -0.04,
    750 : -0.04,
    800 : -0.04,
    900 : -0.05,
    1000 : -0.06,
    1100 : -0.07,
    1200 : -0.09,
    1300 : -0.11,
    1400 : -0.14
}




VBFHVT = SignalUncertainty()
VBFHVT.Name = "VBFHVT"
VBFHVT.errPDF = {
    300 : 0.02,
    400 : 0.02,
    500 : 0.02,
    600 : 0.02,
    700 : 0.02,
    800 : 0.02,
    900 : 0.02,
    1000 : 0.02,
    1100 : 0.02,
    1200 : 0.02,
    1300 : 0.02,
    1400 : 0.02,
    1500 : 0.02,
    1600 : 0.02,
    1700 : 0.02,
    1800 : 0.02,
    1900 : 0.02,
    2000 : 0.02,
    2200 : 0.02,
    2400 : 0.02,
    2600 : 0.02,
    2800 : 0.02,
    3000 : 0.02,
    3500 : 0.02,
    4000 : 0.02,
    4500 : 0.02,
    5000 : 0.02
}
VBFHVT.errISRFSRHP = {
    500 : 0.08,
    600 : 0.08,
    700 : 0.07,
    800 : 0.07,
    900 : 0.06,
    1000 : 0.06,
    1100 : 0.06,
    1200 : 0.05,
    1300 : 0.05,
    1400 : 0.05,
    1500 : 0.04,
    1600 : 0.04,
    1700 : 0.04,
    1800 : 0.04,
    1900 : 0.04,
    2000 : 0.03,
    2200 : 0.03,
    2400 : 0.03,
    2600 : 0.03,
    2800 : 0.03,
    3000 : 0.03,
    3500 : 0.03,
    4000 : 0.03,
    4500 : 0.03,
    5000 : 0.03
}
VBFHVT.errISRFSRLP = {
    500 : -0.08,
    600 : -0.08,
    700 : -0.07,
    800 : -0.07,
    900 : -0.06,
    1000 : -0.06,
    1100 : -0.06,
    1200 : -0.05,
    1300 : -0.05,
    1400 : -0.05,
    1500 : -0.04,
    1600 : -0.04,
    1700 : -0.04,
    1800 : -0.04,
    1900 : -0.04,
    2000 : -0.03,
    2200 : -0.03,
    2400 : -0.03,
    2600 : -0.03,
    2800 : -0.03,
    3000 : -0.03,
    3500 : -0.03,
    4000 : -0.03,
    4500 : -0.03,
    5000 : -0.03
}
VBFHVT.errISRFSRResolved = {
    300 : -0.07,
    400 : -0.06,
    500 : -0.05,
    600 : -0.05,
    700 : -0.04,
    800 : -0.04,
    900 : -0.05,
    1000 : -0.06,
    1100 : -0.07,
    1200 : -0.09,
    1300 : -0.11,
    1400 : -0.14
}

