#!/usr/env python
from sys import argv, stdout
myargv = argv[:]

import ROOT
from ROOT import RooFit as RF

from math import sqrt, log, exp
from array import array
from RooFitUtil import *

from VHRegionsAndChannels import formatProcessName, formatRegionNameROOT, setStyle, cmpBkg

ROOT.RooMsgService.instance().setGlobalKillBelow(RF.WARNING)
ROOT.gROOT.ProcessLine(".L AtlasStyle.C+")
ROOT.SetAtlasStyle()



def plots(ws, rfr, prefix="", binHistFile=None):

    mc = ws.obj("ModelConfig")
    data = ws.data("obsData")
    combPdf = ws.pdf("combPdf")
    if not combPdf:
        combPdf = ws.pdf("simPdf")

    channelCat = combPdf.indexCat()
    channelCatName = channelCat.GetName()

    d = {}

    for chan in categories(channelCat):
        print "inspecting channel", chan

        chanName = chan.GetName()
        d[chanName] = {}

        pdftmp = combPdf.getPdf(chanName)
        datatmp = data.reduce("{0}=={1}::{2}".format(channelCatName,channelCatName,chanName))

        obs = pdftmp.getObservables(mc.GetObservables()).first()

        # do we have a file to rebin from?
        binHist = None
        if binHistFile:
            for k in binHistFile.GetListOfKeys():
                if k.GetName() in chanName:
                    binHist = binHistFile.Get(k.GetName())
                    break
                else:
                    continue

            if not binHist:
                print "cannot find appropriate rebin histogram for channel", \
                        chanName
                exit(-1)


        csigs = []
        cbkgs = []
        cbkgstemp = []
        for c in components(pdftmp, chanName):
            compname = c.GetName()
            print "looking at component", compname

            if "HVT" in compname:
                csigs.append(c)
            else:
                print "adding to bkgs."
                cbkgs.append(c)

        if not csigs:
            print "unable to find signal histogram for channel", \
                    chanName, "component", compname
            exit(-1)


        hbkgs = map(lambda c: compToHist(c, obs, rfr), cbkgs)
        hbkgs = map(setStyle, hbkgs)
        map(lambda h: h.SetName(prefix + h.GetName()), hbkgs)
        hbkgs.sort(cmpBkg)
        hbkgs.reverse()

        cbkg = sumComponents("bkgsum_"+chanName,
                "bkgsum_"+chanName, cbkgs)
        hbkg = setStyle(compToHist(cbkg, obs, rfr))

        csig = sumComponents("sigsum_"+chanName,
                "sigsum_"+chanName, csigs)
        hsig = setStyle(compToHist(csig, obs, rfr))

        if binHist:
            hbkg = scaleBins(hbkg, binHist)
        else:
            hbkg = scaleBins(hbkg, hbkg)

        hbkg.SetName(prefix + hbkg.GetName())


        hdata = datatmp.plotOn(obs.frame(),
                RF.DataError(ROOT.RooAbsData.Poisson)).getHist()
        hdata.SetName(prefix+"_data_"+chanName)

        hbkgs = map(lambda h: scaleBins(h, hbkg), hbkgs)
        hsig = scaleBins(hsig, hbkg)
        hdata = scaleBinsTG(hdata, hbkg)

        d[chanName]["hsig"] = hsig
        d[chanName]["hbkg"] = hbkg
        d[chanName]["hbkgs"] = hbkgs
        d[chanName]["hdata"] = hdata

    return d



def main(outfolder, wsfname, wsname, fitfname, binfname=None):
    doCombine = True
    xs = 9.314E-05
    fws = ROOT.TFile(wsfname)
    ws = fws.Get(wsname)

    f = ROOT.TFile.Open(fitfname)

    rfr = f.Get("fitresult_combPdf_obsData")
    if not rfr:
        rfr = f.Get("fitresult_simPdf_obsData")

    rfr = rfr.Clone()

    mc = ws.obj("ModelConfig")
    np = ROOT.RooArgSet(mc.GetNuisanceParameters())

    pois = mc.GetParametersOfInterest()
    for n in args(pois):
        np.add(n)

    # don't float mu in the plots
    np["mu"].setVal(5000)
    np["mu"].setError(0.0)

    ws.saveSnapshot("vars_initial", np)

    # add all const parameters of the RooFitResult to the floating
    # ones
    fpf = rfr.floatParsFinal().Clone()
    fpf.add(rfr.constPars())

    """
    iter = fpf.iterator()
    i = iter.Next()
    while i != None:
        name = i.GetName()
        if name.startswith("gamma_") \
                or name.startswith("alpha_") \
                or name.startswith("norm_"):

            n = np.find(name)

            if n:
                n.setVal(i.getVal())
                n.setError((i.getErrorHi() + i.getErrorLo()) / 2)
            else:
                print "WARNING: parameter", n, "not found in workspace!!!"
        i = iter.Next()
    """

    ROOT.RooAbsCollection.__assign__(np, fpf)

    # don't float mu in the plots
    np["mu"].setVal(5000)
    np["mu"].setError(0.0)

    ws.saveSnapshot("vars_final", np)



    if binfname != None:
        binf = ROOT.TFile.Open(binfname)
    else:
        binf = None


    ws.loadSnapshot("vars_final")
    dfinal = plots(ws, rfr, prefix="final", binHistFile=binf)

    ws.loadSnapshot("vars_initial")
    dinit = plots(ws, rfr, prefix="init", binHistFile=binf)

    # D2, mJ plots #
    str_SRHP = "SRComb"
    str_SRLP = "SRComb_LP"
    str_SidebandHP = "SRComb_LP"
    if "D2" in wsfname and doCombine:
        # Get Hists
        D2data_HP = setStyle(dinit[str_SRHP]["hdata"])
        D2data_HP.Rebin()
        D2signal_HP = setStyle(dfinal[str_SRHP]["hsig"])
        D2signal_HP.Rebin()
        D2uncert_HP = setStyle(dfinal[str_SRHP]["hbkg"])
        D2uncert_HP.Rebin()
        D2bkgs_HP   = ROOT.THStack("sHP", "sHP")
        map(lambda h: D2bkgs_HP.Add(setStyle(h)), dfinal[str_SRHP]["hbkgs"])
        D2data_LP = setStyle(dinit[str_SRLP]["hdata"])
        D2data_LP.Rebin()
        D2signal_LP = setStyle(dfinal[str_SRLP]["hsig"])
        D2signal_LP.Rebin()
        D2uncert_LP = setStyle(dfinal[str_SRLP]["hbkg"])
        D2uncert_LP.Rebin()
        D2bkgs_LP   = ROOT.THStack("sLP", "sLP")
        map(lambda h: D2bkgs_LP.Add(setStyle(h)), dfinal[str_SRLP]["hbkgs"])
        D2data_HP.Add(D2data_LP)
        D2data_HP.Scale(0.2)
        D2signal_HP.Scale(xs)
        D2signal_LP.Scale(xs)
        D2signal_HP.Add(D2signal_LP)
        D2signal_HP.Scale(0.2)
        D2uncert_HP.Add(D2uncert_LP)
        D2uncert_HP.Scale(0.2)

        bkghists_HP = D2bkgs_HP.GetHists()
        bkghists_LP = D2bkgs_LP.GetHists()
        D2bkgs      = ROOT.THStack("sComb", "sComb")
        for bkghist in bkghists_HP:
            bkghist.Rebin()
            name = bkghist.GetName()
            for bkghist_LP in bkghists_LP:
                LPname = bkghist_LP.GetName()
                if("Wjets" in name and "Wjets" in LPname):
                    bkghist_LP.Rebin()
                    bkghist.Add(bkghist_LP)
                    bkghist.Scale(0.2)
                    D2bkgs.Add(bkghist)
                if("Zjets" in name and "Zjets" in LPname):
                    bkghist_LP.Rebin()
                    bkghist.Add(bkghist_LP)
                    bkghist.Scale(0.2)
                    D2bkgs.Add(bkghist)
                if("ttbar" in name and "ttbar" in LPname):
                    bkghist_LP.Rebin()
                    bkghist.Add(bkghist_LP)
                    bkghist.Scale(0.2)
                    D2bkgs.Add(bkghist)
                if("singletop" in name and "singletop" in LPname):
                    bkghist_LP.Rebin()
                    bkghist.Add(bkghist_LP)
                    bkghist.Scale(0.2)
                    D2bkgs.Add(bkghist)
                if("Diboson" in name and "Diboson" in LPname):
                    bkghist_LP.Rebin()
                    bkghist.Add(bkghist_LP)
                    bkghist.Scale(0.2)
                    D2bkgs.Add(bkghist)

        # TCanvas
        c_D2 = ROOT.TCanvas("c_D2", "c_D2", 600, 600)
        c_D2.cd()
        c_D2.Clear()
        pmain_D2 = ROOT.TPad("pmain_D2", "pmain_D2", 0, 0.28, 1, 1)
        pmain_D2.SetBottomMargin(0.09)
        pmain_D2.SetTickx()
        pmain_D2.SetTicky()
        pmain_D2.Draw()

        pratio_D2 = ROOT.TPad("pratio_D2", "pratio_D2", 0, 0, 1, 0.34)
        pratio_D2.SetBottomMargin(0.35)
        pratio_D2.SetTopMargin(0.01)
        pratio_D2.SetTickx()
        pratio_D2.SetTicky()
        pratio_D2.Draw()
        pmain_D2.cd()
        # Draw
        maxHist = max(maxWithUncert(D2uncert_HP), maxWithUncert(D2data_HP))
        maxHist = max(maxHist, maxWithUncert(D2signal_HP))
        maxHist *= 0.5
        maxHist = 70000
        minHist = minH(D2data_HP, maxHist)*0.3
        minHist = 0.002
        xmin=0.0
        xmax=4.0
        ymin=0.02
        ymax=maxHist
        frame=ROOT.gPad.DrawFrame(xmin, ymin,  xmax, ymax)
        frame.Draw()
        frame.GetXaxis().SetTitleSize(0.0375/0.75)
        frame.GetYaxis().SetTitleSize(0.0375/0.75)
        frame.GetXaxis().SetLabelSize(0.0375/0.75)
        frame.GetYaxis().SetLabelSize(0.0375/0.75)
        frame.GetYaxis().SetTitle("Events / 0.4")

        D2bkgs.Draw("same:hist")
        D2bkgs.GetYaxis().SetTitleSize(0.05)
        D2bkgs.GetYaxis().SetTitleFont(42)
        D2bkgs.GetYaxis().SetTitleOffset(1.7)

        D2bkgs.SetMaximum(maxHist*3.0)
        D2bkgs.GetXaxis().SetLabelSize(0)
        D2bkgs.GetXaxis().SetTitle("")

        D2bkgs.Draw("same:hist")
        D2bkgs.GetXaxis().SetNdivisions(505)

        D2bkgs.GetYaxis().SetTitle("Events / 0.2")
        D2uncert_HP.Draw("e2:same")
        D2signal_HP.Draw("hist:same")
        D2data_HP.Draw("e:same")   ####

        D2histopts = map(lambda h: (h, "F"), D2bkgs.GetHists())
        D2histopts.reverse()
        D2histopts = [(D2data_HP, "EP")]+ D2histopts \
                     + [(D2uncert_HP, "F"), (D2signal_HP, "L")]
        D2leg = buildLegend(0.59, 0.45, 0.78, 0.9, D2histopts)
        D2leg.Draw()

        drawAtlasLabel(0.23, 0.85, suffix="Internal")
#        drawText(0.23, 0.67, "SRWW+SRWZ+Sideband", size=0.04)
        drawLumi(0.23, 0.76)
        region=ROOT.TLatex(0.23, 0.69, "ggF/q#bar{q} Category");
        region.SetNDC();
        region.SetTextFont(42);
        region.SetTextSize(0.045);  
        region.Draw();
        
        D2data_HP.SetMarkerStyle(20)
        D2data_HP.SetMarkerSize(1.)
        pratio_D2.cd()
        bframe=ROOT.gPad.DrawFrame(xmin, 0.51, xmax, 1.49)
        bframe.GetXaxis().SetTitleSize(0.0375/0.34)
        bframe.GetYaxis().SetTitleSize(0.0375/0.34)
        bframe.GetXaxis().SetLabelSize(0.0375/0.34)
        bframe.GetYaxis().SetLabelSize(0.0375/0.34)
        bframe.GetYaxis().SetTitleOffset(0.45)
        bframe.GetYaxis().SetNdivisions(505)
        bframe.GetXaxis().SetTitle("D_{2}")
        bframe.GetYaxis().SetTitle("Data / SM")
        bframe.GetYaxis().CenterTitle()
        bframe.Draw()

        hbkg = stripUncert(D2uncert_HP)
        D2uncertratio = D2uncert_HP.Clone()
        D2uncertratio.Divide(hbkg)
        D2dataratio = D2data_HP.Clone()
        D2dataratio.Divide(hbkg)
        D2uncertratio.Draw("same:e2")
        xax = D2uncertratio.GetXaxis()
        xax.SetNdivisions(505)
        xax.SetLabelSize(0.15)
        xax.SetTitleSize(0.20)
        xax.SetTitleOffset(0.8)
        xax.SetTickLength(0.06)
        xax.SetTitle("D_{2}^{(#beta=1)}")

        yax = D2uncertratio.GetYaxis()
        yax.SetRangeUser(0.01, 1.99)
        yax.SetTitle("Data / MC")
        yax.SetLabelSize(0.15)
        yax.SetTitleSize(0.15)
        yax.SetTitleOffset(1.7)
        yax.SetNdivisions(505)

        ratioLine = ROOT.TLine(D2dataratio.GetBinLowEdge(1), 1,
                               4.0, 1)
        ratioLine.SetLineColor(ROOT.kRed)
        ratioLine.SetLineWidth(2)
        ratioLine.Draw("same")
        for iBin in xrange(1, D2dataratio.GetNbinsX()+1):
            if (D2dataratio.GetBinContent(iBin)) == 0 :
                D2dataratio.SetBinContent(iBin,-9999)
        D2dataratio.Draw("e0same") ####

        pmain_D2.RedrawAxis()
        c_D2.SaveAs(outfolder+"/D2_HPLP_Combined.pdf")
        c_D2.SaveAs(outfolder+"/D2_HPLP_Combined.eps")
        c_D2.SaveAs(outfolder+"/D2_HPLP_Combined.C")
        pmain_D2.SetLogy()
        frame.SetMaximum(maxHist*500000.0)
        pmain_D2.Update()
        c_D2.SaveAs(outfolder+"/D2_HPLP_Combined_logy.pdf")
        c_D2.SaveAs(outfolder+"/D2_HPLP_Combined_logy.eps")
        c_D2.SaveAs(outfolder+"/D2_HPLP_Combined_logy.C")

    if "mJ" in wsfname and doCombine:
        # Get Hists
        mJdata_HP = setStyle(dinit[str_SRHP]["hdata"])
        mJsignal_HP = setStyle(dfinal[str_SRHP]["hsig"])
        mJuncert_HP = setStyle(dfinal[str_SRHP]["hbkg"])
        mJbkgs_HP   = ROOT.THStack("sHP", "sHP")
        map(lambda h: mJbkgs_HP.Add(setStyle(h)), dfinal[str_SRHP]["hbkgs"])
        mJdata_LP = setStyle(dinit[str_SidebandHP]["hdata"])
        mJsignal_LP = setStyle(dfinal[str_SidebandHP]["hsig"])
        mJuncert_LP = setStyle(dfinal[str_SidebandHP]["hbkg"])
        mJbkgs_LP   = ROOT.THStack("sLP", "sLP")
        map(lambda h: mJbkgs_LP.Add(setStyle(h)), dfinal[str_SidebandHP]["hbkgs"])
        mJdata_HP.Add(mJdata_LP)
        mJdata_HP.Scale(5)
        mJsignal_HP.Scale(xs)
        mJsignal_LP.Scale(xs)
        mJsignal_HP.Add(mJsignal_LP)
        mJsignal_HP.Scale(5)
        mJuncert_HP.Add(mJuncert_LP)
        mJuncert_HP.Scale(5)
        bkghists_HP = mJbkgs_HP.GetHists()
        bkghists_LP = mJbkgs_LP.GetHists()
        mJbkgs      = ROOT.THStack("sComb", "sComb")
        for bkghist in bkghists_HP:
            name = bkghist.GetName()
            for bkghist_LP in bkghists_LP:
                LPname = bkghist_LP.GetName()
                if("Wjets" in name and "Wjets" in LPname):
                    bkghist.Add(bkghist_LP)
                    bkghist.Scale(5)
                    mJbkgs.Add(bkghist)
                if("Zjets" in name and "Zjets" in LPname):
                    bkghist.Add(bkghist_LP)
                    bkghist.Scale(5)
                    mJbkgs.Add(bkghist)
                if("ttbar" in name and "ttbar" in LPname):
                    bkghist.Add(bkghist_LP)
                    bkghist.Scale(5)
                    mJbkgs.Add(bkghist)
                if("singletop" in name and "singletop" in LPname):
                    bkghist.Add(bkghist_LP)
                    bkghist.Scale(5)
                    mJbkgs.Add(bkghist)
                if("Diboson" in name and "Diboson" in LPname):
                    bkghist.Add(bkghist_LP)
                    bkghist.Scale(5)
                    mJbkgs.Add(bkghist)

        # TCanvas
        c_mJ = ROOT.TCanvas("c_mJ", "c_mJ", 600, 600)
        c_mJ.cd()
        c_mJ.Clear()
        pmain_mJ = ROOT.TPad("pmain_mJ", "pmain_mJ", 0, 0.28, 1, 1)
        pmain_mJ.Draw()
        pmain_mJ.SetBottomMargin(0.09)
        pmain_mJ.SetTickx()
        pmain_mJ.SetTicky()
        pmain_mJ.Draw()

        pratio_mJ = ROOT.TPad("pratio_mJ", "pratio_mJ", 0, 0, 1, 0.34)
        pratio_mJ.SetBottomMargin(0.35)
        pratio_mJ.SetTopMargin(0.01)
        pratio_mJ.SetTickx()
        pratio_mJ.SetTicky()
        pratio_mJ.Draw()
        pmain_mJ.cd()


        maxHist = max(maxWithUncert(mJuncert_HP), maxWithUncert(mJdata_HP))
        maxHist = max(maxHist, maxWithUncert(mJsignal_HP))
        maxHist*=0.7
        maxHist = 70000
        minHist = minH(mJdata_HP, maxHist)*0.05
        minHist = 0.002
        xmin=50.0
        xmax=200.0
        ymin=0.02
        ymax=maxHist
        frame=ROOT.gPad.DrawFrame(xmin, ymin,  xmax, ymax)
        frame.Draw()
        frame.GetXaxis().SetTitleSize(0.0375/0.75)
        frame.GetYaxis().SetTitleSize(0.0375/0.75)
        frame.GetXaxis().SetLabelSize(0.0375/0.75)
        frame.GetYaxis().SetLabelSize(0.0375/0.75)
        frame.GetYaxis().SetTitle("Entries / 5 GeV")

        # Draw
        mJbkgs.Draw("same:hist")
        mJbkgs.GetYaxis().SetTitleSize(0.05)
        mJbkgs.GetYaxis().SetTitleFont(42)
        mJbkgs.GetYaxis().SetTitleOffset(1.7)

        # mJbkgs.SetMaximum(maxHist*3.0)
        mJbkgs.GetXaxis().SetLabelSize(0)
        mJbkgs.GetXaxis().SetTitle("")

        mJbkgs.Draw("same:hist")
        mJbkgs.GetXaxis().SetNdivisions(505)

        mJbkgs.GetYaxis().SetTitle("Events / 5 GeV")        
        mJuncert_HP.Draw("e2:same")
        mJsignal_HP.Draw("hist:same")
        mJdata_HP.Draw("e:same")   ####

        mJhistopts = map(lambda h: (h, "F"), mJbkgs.GetHists())
        mJhistopts.reverse()
        mJhistopts = [(mJdata_HP, "EP")]+ mJhistopts \
                     + [(mJuncert_HP, "F"), (mJsignal_HP, "L")]
        mJleg = buildLegend(0.6, 0.45, 0.8, 0.9, mJhistopts)
        mJleg.Draw()

        drawAtlasLabel(0.23, 0.85, suffix="Internal")
#        drawText(0.23, 0.67, "SRWW+SRWZ+Sideband", size=0.04)
        drawLumi(0.23, 0.76)
        mJdata_HP.SetMarkerStyle(20)
        mJdata_HP.SetMarkerSize(1.)

        pratio_mJ.cd()
        bframe=ROOT.gPad.DrawFrame(xmin, 0.51, xmax, 1.49)
        bframe.GetXaxis().SetTitleSize(0.0375/0.34)
        bframe.GetYaxis().SetTitleSize(0.0375/0.34)
        bframe.GetXaxis().SetLabelSize(0.0375/0.34)
        bframe.GetYaxis().SetLabelSize(0.0375/0.34)
        bframe.GetYaxis().SetTitleOffset(0.45)
        bframe.GetYaxis().SetNdivisions(505)
        bframe.GetXaxis().SetTitle("m_{J} [GeV]")
        bframe.GetYaxis().SetTitle("Data / SM")
        bframe.Draw()

        hbkg = stripUncert(mJuncert_HP)
        mJuncertratio = mJuncert_HP.Clone()
        mJuncertratio.Divide(hbkg)
        mJdataratio = mJdata_HP.Clone()
        mJdataratio.Divide(hbkg)
        mJuncertratio.Draw("same:e2")
        xax = mJuncertratio.GetXaxis()
        xax.SetNdivisions(505)
        xax.SetLabelSize(0.15)
        xax.SetTitleSize(0.20)
        xax.SetTitleOffset(0.8)
        xax.SetTickLength(0.06)
        xax.SetTitle("m_{J} [GeV]")

        yax = mJuncertratio.GetYaxis()
        yax.SetRangeUser(0.01, 1.99)
        yax.SetTitle("Data / SM")
        yax.SetLabelSize(0.15)
        yax.SetTitleSize(0.15)
        yax.SetTitleOffset(1.7)
        yax.SetNdivisions(505)

        ratioLine = ROOT.TLine(mJdataratio.GetBinLowEdge(1), 1,
                               mJdataratio.GetBinLowEdge(mJdataratio.GetNbinsX()+1), 1)
        ratioLine.SetLineColor(ROOT.kRed)
        ratioLine.SetLineWidth(2)
        ratioLine.Draw("same")
        for iBin in xrange(1, mJdataratio.GetNbinsX()+1):
            if (mJdataratio.GetBinContent(iBin)) == 0 :
                mJdataratio.SetBinContent(iBin,-9999)
        mJdataratio.Draw("e0same") ####

        pmain_mJ.RedrawAxis()
        c_mJ.SaveAs(outfolder+"/mJ_SR_Sideband_Combined.pdf")
        c_mJ.SaveAs(outfolder+"/mJ_SR_Sideband_Combined.eps")
        c_mJ.SaveAs(outfolder+"/mJ_SR_Sideband_Combined.C")
        pmain_mJ.SetLogy()
        frame.SetMaximum(maxHist*500000)
        pmain_mJ.Update()
        c_mJ.SaveAs(outfolder+"/mJ_SR_Sideband_Combined_logy.pdf")
        c_mJ.SaveAs(outfolder+"/mJ_SR_Sideband_Combined_logy.eps")
        c_mJ.SaveAs(outfolder+"/mJ_SR_Sideband_Combined_logy.C")

    c = ROOT.TCanvas("c", "c", 1000, 1000)
    for reg in dinit:
        c.cd()
        c.Clear()
        pmain = ROOT.TPad("pmain", "pmain", 0, 0.25, 1, 1)
        pmain.Draw()
        pmain.SetBottomMargin(0.02)
        pmain.SetLeftMargin(0.15)
        pmain.SetRightMargin(0.075)

        pratio = ROOT.TPad("pratio", "pratio", 0, 0, 1, 0.25)
        pratio.Draw()
        pratio.SetTopMargin(0.06)
        pratio.SetBottomMargin(0.4)
        pratio.SetLeftMargin(0.15)
        pratio.SetRightMargin(0.075)

        pmain.cd()
        bkgs = ROOT.THStack("s", "s")

        prefit = setStyle(dinit[reg]["hbkg"])
        signal = setStyle(dfinal[reg]["hsig"])
        uncert = setStyle(dfinal[reg]["hbkg"])
        map(lambda h: bkgs.Add(setStyle(h)), dfinal[reg]["hbkgs"])

        prefit.SetTitle("pre-fit")
        prefit.SetFillStyle(0)
        prefit.SetLineColor(ROOT.kBlue)
        prefit.SetLineWidth(4)
        prefit.SetLineStyle(2)

        signal.Scale(xs)
        #signal.SetTitle(signal.GetTitle() + " x 10")
        signal.SetTitle(signal.GetTitle())
        
        uncert.SetFillStyle(3345)
        name = uncert.GetName().replace("finalhist_bkgsum_", "")
        #name = name[:name.find("_file")]
               
        data = setStyle(dinit[reg]["hdata"])
        data.SetMarkerStyle(20)
        data.SetMarkerSize(1.)



        bkgs.Draw("hist")
        allhists = [uncert, data, signal] + list(bkgs.GetHists())
        
        fout = ROOT.TFile.Open(outfolder + "/" + name +".root", "RECREATE")
        for hist in allhists:
            hist.Write()
        fout.Close()

        if "mVH" in name:
            map(lambda h: h.Scale(100), allhists)
            bkgs.GetYaxis().SetTitle("Events / 100 GeV")
        elif "mJ" in wsfname:
            bkgs.GetYaxis().SetTitle("Events / 5 GeV")
        elif "D2" in wsfname:
            bkgs.GetYaxis().SetTitle("Events / 0.2")
        elif "mJet" in name:
            map(lambda h: h.Scale(25), allhists)
            bkgs.GetYaxis().SetTitle("Events / 25 GeV")
        elif "pTV" in name:
            map(lambda h: h.Scale(50), allhists)
            bkgs.GetYaxis().SetTitle("Events / 50 GeV")
        elif "mBB" in name:
            map(lambda h: h.Scale(50), allhists)
            bkgs.GetYaxis().SetTitle("Events / 10 GeV")


        maxHist = max(maxWithUncert(uncert), maxWithUncert(data))
        maxHist = max(maxHist, prefit.GetMaximum())

        minHist = minH(data, maxHist)*0.3
        bkgs.GetYaxis().SetTitleSize(0.05)
        bkgs.GetYaxis().SetTitleFont(42)

        bkgs.SetMaximum(maxHist*3.0)
        bkgs.GetXaxis().SetLabelSize(0)
        bkgs.GetXaxis().SetTitle("")

        bkgs.Draw("hist")
        bkgs.GetXaxis().SetNdivisions(505)

#        prefit.Draw("histsame")
        uncert.Draw("e2same")
        signal.Draw("histsame")
        data.Draw("esame")   ####

        
        histopts = map(lambda h: (h, "F"), bkgs.GetHists())
        histopts.reverse()

        histopts = [(data, "EP"), (signal, "L")] + histopts \
                   + [(uncert, "F")]

        leg = buildLegend(0.65, 0.45, 0.85, 0.9, histopts)
        leg.Draw()

        drawAtlasLabel(0.2, 0.85, suffix="Internal")
        # drawAtlasLabel(0.2, 0.85, suffix="")
        drawLumi(0.2, 0.76)

        rn = formatRegionNameROOT(name).split(", ", 1)
        drawText(0.2, 0.67, rn[0], size=0.04)
        if len(rn) > 1:
            drawText(0.2, 0.61, rn[1], size=0.04)


        pratio.cd()

        #uncert.GetXaxis().SetRangeUser(70,150)
        #data.GetXaxis().SetRangeUser(70,150)
        hbkg = stripUncert(uncert)
        #hbkg.GetXaxis().SetRangeUser(70,150)
        uncertratio = uncert.Clone()
        uncertratio.Divide(hbkg)

        dataratio = data.Clone()
        dataratio.Divide(hbkg)
        #dataratio.GetXaxis().SetRangeUser(70,150)
        #uncertratio.GetXaxis().SetRangeUser(70,150)

        uncertratio.Draw("e2")
        xax = uncertratio.GetXaxis()
        xax.SetNdivisions(505)
        xax.SetLabelSize(0.15)
        xax.SetTitleSize(0.20)
        xax.SetTitleOffset(0.8)
        xax.SetTickLength(0.06)

        if "mJet" in name:
            xax.SetTitle("m_{jet} [GeV]")
        elif "mJ" in wsfname:
            xax.SetTitle("m_{J} [GeV]")
        elif "D2" in wsfname:
            xax.SetTitle("D_{2}^{(#beta=1)}")
        elif "pTV" in name:
            xax.SetTitle("p_{T,V} [GeV]")
        #elif "mBB" in name:
            #xax.SetTitle("m_{BB} [GeV]")
        elif "mVH" in name:
            if "vv" in name:
                xax.SetTitle("m_{T,VH} [GeV]")
            else:
                xax.SetTitle("m_{VH} [GeV]")

        yax = uncertratio.GetYaxis()
        yax.SetRangeUser(0, 2.0)
        yax.SetTitle("Data / MC")
        yax.SetLabelSize(0.15)
        yax.SetTitleSize(0.15)
        yax.SetTitleOffset(0.45)
        yax.SetNdivisions(505)
        ratioLine = ROOT.TLine(dataratio.GetBinLowEdge(1), 1,
                               dataratio.GetBinLowEdge(dataratio.GetNbinsX()+1), 1)
        ratioLine.SetLineColor(ROOT.kBlack)
        ratioLine.SetLineWidth(2)
        ratioLine.Draw("same")
        for iBin in xrange(1, dataratio.GetNbinsX()+1):
            if (dataratio.GetBinContent(iBin)) == 0 :
                dataratio.SetBinContent(iBin,-9999)

        dataratio.Draw("e0same") ####

        
        arrows = ratioArrows(dataratio, 2.0, 0.0)
        if arrows:
            arrows.Draw("epsame")



        pmain.SetLogy(False)

        #c.SaveAs(outfolder + "/" + name + ".png")
        c.SaveAs(outfolder + "/" + name + ".eps")
        c.SaveAs(outfolder + "/" + name + ".pdf")    
        c.SaveAs(outfolder + "/" + name + ".C")

        pmain.cd()
        bkgs.SetMinimum(minHist)
        bkgs.SetMaximum(exp((log(maxHist) - log(minHist))*1.5))

        bkgs.Draw("hist")
#        prefit.Draw("histsame")
        uncert.Draw("e2same")
        signal.Draw("histsame")
        data.Draw("esame")  ####
        leg.Draw()

        drawAtlasLabel(0.2, 0.85,suffix="Internal")
        drawLumi(0.2, 0.76)
        drawText(0.2, 0.67, rn[0], size=0.04)
        if len(rn) > 1:
            drawText(0.2, 0.61, rn[1], size=0.04)

        pmain.SetLogy(True)
        #pmain.SetLogx(True)
        #pratio.SetLogx(True)

        #c.SaveAs(outfolder + "/" + name + "_log.png")
        c.SaveAs(outfolder + "/" + name + "_log.eps")
        c.SaveAs(outfolder + "/" + name + "_log.pdf")       
        c.SaveAs(outfolder + "/" + name + "_log.C")


        continue

    return


if __name__ == "__main__":
    main(argv[1], argv[2], argv[3],
            argv[4], argv[5] if len(argv) > 5 else None)
