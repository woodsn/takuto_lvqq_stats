#!/usr/bin/env python

# TODO Treat automatically 2011/2012. Partially done
# TODO: put ifs to handle h as both TGraph and TH1 gracefully

# README
# configuration of samples, colors, etc... at the bottom of this file, in Config
# there is possibility to merge samples in one color/block in the plots: see stop(Wt/st) case
# signal can be stacked or overprinted as a line
# customisation of title of X axis: see get_xTitle()

import ROOT
import array
import math
import bisect

class Setup(object):
    """ A class that contains data, signal MC, background MC..., and that knows how to plot them """
    def __init__(self, title="", guess_properties=True):
        self.data = 0
        self.fiterr = 0
        self.prefit = 0
        self.bkgs = []
        self.sig = 0
        self.add_sig = 0
        self.determine_weighted(title)
        if "Higgsweighted" in title.rsplit('_', 1)[-1]:
            self.title = title.rsplit('_', 1)[0]
        elif "Dibosonweighted" in title.rsplit('_', 1)[-1]:
            self.title = title.rsplit('_', 1)[0]
        else:
            self.title = title
        self.isBkgSub = False
        self.chi2 = -1
        self.initialized = False
        ROOT.TGaxis.SetMaxDigits(4)
        if guess_properties:
            self.properties = getPropertiesFromTag(self.title)
        else:
            self.properties = None

    def determine_weighted(self, title):
        if "Higgsweighted" in title:
            Config.weighted = 'Higgs'
        elif "Dibosonweighted" in title:
            Config.weighted = 'Diboson'
        else:
            Config.weighted = ''

    def append_bkg(self, bkg):
        is_merged = False
        for name in bkg.mergelist:
            for b in self.bkgs:
                if b.name == name:
                    b.append(bkg)
                    is_merged = True
                    break
        if not is_merged:
            self.bkgs.append(bkg)

    def init_finished(func):
        """ to be used as a decorator:

        Ensures that backgrounds are sorted
        Ensures that sum of backgrounds is available
        Ensures that stack of backgrounds is available
        Blind data and creates a data TH1 from the data TGraphAsymmErrors
        Scales the signal by the desired amount
        """
        def f(self, *args, **kwargs):
            if not self.initialized:
                self.finish_initialize()
            return func(self, *args, **kwargs)
        return f

    def finish_initialize(self):
        if not self.initialized:
            self.bkgs.sort()
            if len(self.bkgs) != 0:
                self.hsum = clone(self.bkgs[0].h)
                self.hsum.Reset()
                self.stack= ROOT.THStack("stack","stack")
                for bkg in self.bkgs:
                    self.hsum.Add(bkg.h)
                    self.stack.Add(bkg.h)
            self.data.clean()
            self.exp_sig = self.sig.make_expected_signal()
            if self.plot_with_additional_signal():
                self.add_sig = self.sig.make_additional_signal(self.hsum.Integral(), self.properties["dist"], self.properties["L"])
            if Config.blind:
                self.blind_data()
            self.data.make_data_hist(self.hsum)
            self.sig.scale()
            self.initialized = True

    def blind_data(self):
        """ Blind data according to the region we are in """
        if "T2" in self.title and "mjj" in self.title:
            self.data.blind(100, 150)
        #else:
        # Add general blinding at 2% S/B
        for i in range(1, self.hsum.GetNbinsX()+1):
            if self.hsum.GetBinContent(i) > 0:
                sob = self.exp_sig.h.GetBinContent(i) / ( self.hsum.GetBinContent(i) )
                if sob > 0.02:
                    self.data.blind(self.hsum.GetBinLowEdge(i), self.hsum.GetBinLowEdge(i+1))
            elif self.exp_sig.h.GetBinContent(i) > 0:
                self.data.blind(self.hsum.GetBinLowEdge(i), self.hsum.GetBinLowEdge(i+1))

    def find_bkg(self, name):
        """ Returns a background of the given name """
        for b in self.bkgs:
            if b.name == name:
                return b
        return None

    @init_finished
    def find_maximum(self):
        """ Find maximum of data(+error bar) and MC """
        h1 = self.data.hist
        tobjarray = self.stack.GetStack()
        h2 = tobjarray.Last()
        maximum = max(h1.GetBinContent(h1.GetMaximumBin())+h1.GetBinError(h1.GetMaximumBin()), h2.GetBinContent(h2.GetMaximumBin()))
        if maximum < self.prefit.h.GetMaximum():
          maximum=self.prefit.h.GetMaximum()
        print "found maximum of", maximum
        return maximum

    @init_finished
    def find_minimum(self):
        """ Find minimum of data(-error bar) and MC
        Function is a bit complicated so that empty bins are not taken into account
        and log plots are not spoiled
        """
        h1 = self.data.hist
        tobjarray = self.stack.GetStack()
        h2 = tobjarray.Last()
        min_data = h1.GetBinContent(h1.GetMinimumBin())
        if min_data != 0:
            min_data -= h1.GetBinError(h1.GetMinimumBin())
        min_MC = h2.GetBinContent(h2.GetMinimumBin())
        mini = min(min_data, min_MC)
        if mini == 0:
            print "found minimum of 0"
            newmin = min(h1.GetMinimum(1.e-5), h2.GetMinimum(1.e-5))
            print "next to minimum is found at", newmin
            return newmin
        else:
            print "found minimum of ", mini
        return mini

    def make_complete_plot(self, can_name = "", do_ratio = False, logy = False, ytitle = "Events", ybounds=(0.4, 1.6)):
        """ Main function to create plots.

        Divide canvas if needed, call functions to fill the pads, and call functions
        to print all the stuff (legend, titles, labels...)
        """
        if can_name:
            cname = can_name
        else:
            cname = "can"
        print "plotting", cname
        tc = ROOT.TCanvas(cname, cname, 600, 600)
        if do_ratio:
            up, do = divide_canvas(tc, 0.25)
            up.cd()
            self.smallFontSize = 0.04
            self.legFontSize = 0.03
            self.normalFontSize = 0.05
            print "done ratio"
        else:
            ROOT.gPad.SetLeftMargin(0.13)
            ROOT.gPad.SetRightMargin(0.075)
            self.smallFontSize = 0.03
            self.legFontSize = 0.025
            self.normalFontSize = 0.0375
        xbounds = None
        print "properties"
        print self.properties["dist"]
        if self.properties["dist"] == "pTB1":
            xbounds = (40, 400)
        if self.properties["dist"] == "mVH":
          xbounds = (0, 3000)
        self.draw_everything(do_ratio, logy, ytitle, xbounds=xbounds)
        print "drawn stuff"
        if logy:
            ROOT.gPad.SetLogy()
        if do_ratio:
            do.cd()
            self.draw_dataMC_ratio(True, ybounds, xbounds=xbounds)
            up.cd()
        #leg_pos = [0.64, 0.46, 0.84, 0.93]
        leg_pos = [0.62, 0.42, 0.865, 0.925]
        if self.properties["L"] == '0' and self.properties["dist"] == "VpT":
            #leg_pos = [0.15, 0.13, 0.35, 0.6]
            leg_pos = [0.155, 0.13, 0.375, 0.65]
        if self.properties["dist"] == "dPhiVBB":
            leg_pos = [0.16, 0.16, 0.38, 0.68]
        leg = self.build_legend(leg_pos)
        draw(leg)
        if do_ratio:
            ref_up = 0.87
        else:
            ref_up = 0.90
        xref = 0.17
        self.draw_ATLAS_label((xref, ref_up))
        self.draw_lumi((xref, ref_up - self.normalFontSize - 0.01))
        if Config.year == 4023:
            height = 3.5
        else:
            height = 2
        self.draw_title((xref, ref_up - height*self.normalFontSize - 0.01))
        return tc

    @init_finished
    def make_bkg_substr_plot(self, can_name = ""):
        """ Function to create background-subtracted plots.

        Subtract backgrounds from everything, and pass the resulting objects
        to make_complete_plot()
        """
        bkgsum = clone(self.hsum)
        model = clone(self.hsum)
        model.Reset()
        diboson = Bkg(model, "Diboson")
        for sample in ["diboson", "Diboson", "WZ", "ZZ", "VZ"]:
            #continue   # use to subtract diboson as well
            res = self.find_bkg(sample)
            if res:
                bkgsum.Add(res.h, -1)
                diboson.h.Add(res.h)

        setup_tmp = Setup(self.title)
        setup_tmp.determine_weighted(can_name)
        setup_tmp.isBkgSub = True
        setup_tmp.data = self.data.subtract(bkgsum)
        setup_tmp.append_bkg(diboson)
        setup_tmp.sig = self.sig
        setup_tmp.exp_sig = self.exp_sig
        if self.fiterr:
            setup_tmp.fiterr = self.fiterr.center_at_0()
        setup_tmp.chi2 = self.chi2
        if Config.weighted:
            return setup_tmp.make_complete_plot(can_name, ytitle="Weighted events after subtraction")
        else:
            return setup_tmp.make_complete_plot(can_name, ytitle="Events after subtraction")

    # not sure all the draw functions should be there

    @init_finished
    def draw_everything(self, plot_ratio = False, logy = False, ytitle = "Events", xtitle="", xbounds = None):
        """ Function in charge of plotting histos on the upper pad

        Take care of the nightmarish Y axis ranges
        """
        the_min = self.find_minimum()
        the_max = self.find_maximum()
        # avoid to have stack and legend overlapping
        if self.properties:
            if self.properties["dist"] == "MV1cB1" or self.properties["dist"] == "MV1cB2" or self.properties["dist"] == "MV1cBTag":
                if logy: the_max *= 1.0
                else : the_max *= 1.5
            if self.properties["dist"] == "dPhiVBB" :
                if logy: the_max *= 5
                else : the_max *= 0.7
            if self.properties["dist"] == "dPhiLBmin" :
                if logy: the_max *= 1
                else : the_max *= 1.3
            if self.properties["dist"] == "mjj" :
                if logy: the_max *= 1
                else : the_max *= 1.1
            if self.properties["dist"] == "dRBB" :
                if logy: the_max *= 500
            if self.properties["dist"] == "MV1cBTag" :
                if logy: the_max *= 1.0
                else : the_max *= 0.75
            if self.properties["L"] == "0" : 
                if self.properties["dist"] == "MV1cB1" or self.properties["dist"] == "MV1cB2" or self.properties["dist"] == "mjj" :
                    if logy: the_max *= 1.0
                    else : the_max *= 1.1
                if self.properties["dist"] == "MET" :
                    if logy: the_max *= 1.0
                    else : the_max /= 1.15

        #
        no_trafo=False
        if self.data.hist.GetNbinsX()==25 : no_trafo=True
        if self.properties and self.properties["dist"] == "mva" and no_trafo:
            if logy: the_max *= 120

        if not logy and the_min > 0:
            the_min = 0
        if logy and the_min <= 1:
            the_min = 0.25
        mini = the_min
        if self.sig.mode == Signal.OVERPRINT:
            print "DRAWING UNSTACKED"
            hist = self.draw_bkg(True)
            if not self.exp_sig:
                self.draw_sig(False)
            else:
                self.draw_sig(False, is_expected=True)
        else:
            print "DRAWING STACKED", mini, " ", the_min, " ", the_max
            if not self.exp_sig:
                hist = self.draw_bkg_plus_sig(True)
            else:
                hist = self.draw_bkg_plus_sig(True, is_expected=True)
                #if self.sig.muhat < 1:  # Change order of plotting whether muhat <> 1
                    #hist = self.draw_bkg_plus_sig(True, is_expected=True)
                    #self.draw_bkg_plus_sig(False, is_expected=False)
                #else:
                    #hist = self.draw_bkg_plus_sig(True, is_expected=False)
                    #self.draw_bkg_plus_sig(False, is_expected=True)

        if self.add_sig:
            self.draw_sig(False, False, True)

        if mini < 0:
            print "IsNEG ", self.title, " ", mini
            hist.SetMinimum(mini*1.25)
        else:
            mini = 0
            # fix 0 cut in the Y axis
            #hist.SetMinimum(0.01)
        if logy:
            print "max will be", the_max * 100
            hist.SetMaximum(the_max * 100)
            print "min will be", the_min / 2.5
            hist.SetMinimum(the_min / 2.5)
        else:
            hist.SetMaximum(mini + (the_max - mini) * 1.5) # TODO can be configurable ?
            # fix 0 cut in the Y axis
            #hist.SetMinimum(0.01)

        if self.properties and "dist" in self.properties:
            #if "MV1c" in self.properties["dist"]:
                #self.relabel_mv1c_axis(hist)
            print "setting divisions"
            print hist.GetXaxis().GetNdivisions()
            #hist.GetXaxis().SetNdivisions(505, ROOT.kFALSE)
            print hist.GetXaxis().GetNdivisions()
            #    hist.GetXaxis().SetLabelSize(0.07)

        if plot_ratio: # tricks to draw pretty ratio plots
            hist.GetYaxis().SetLabelSize(0.05)
            hist.GetYaxis().SetTitleSize(0.05)
            hist.GetYaxis().SetTitleOffset(1.3)
            if hist.GetMaximum() > 0 and hist.GetMinimum() / hist.GetMaximum() < 0.25 and not logy:
                hist.SetMinimum(hist.GetMaximum() / 1000000)
            hist.Draw("sameaxis")
        else:
            hist.GetYaxis().SetTitleOffset(1.3)
        if self.fiterr:
            self.draw_FitErr(False)
        else:
            self.draw_MCstat(False)
        if self.prefit:
            self.draw_PreFit(False)
        self.draw_data(False)
        if xtitle:
            hist.GetXaxis().SetTitle(xtitle)
        else:
            trafo_mva_extra = ""
            if self.properties and self.properties["dist"] == "mva" and no_trafo: trafo_mva_extra = "Untransformed "
            #
            hist.GetXaxis().SetTitle(trafo_mva_extra+self.get_xTitle())
        print "YTITLE ", ytitle
        extra_unit = ""
        extra_unit = " / 25 GeV"
        if self.properties:
        #    if self.properties["dist"] == "MEff" :  extra_unit = " GeV"
        #    if self.properties["dist"] == "MEff3" : extra_unit = " GeV"
        #    if self.properties["dist"] == "MET" :   extra_unit = " GeV"
        #    if self.properties["dist"] == "mLL" :   extra_unit = " GeV"
        #    if self.properties["dist"] == "mTW" :   extra_unit = " GeV"
        #    if self.properties["dist"] == "pTB1" :  extra_unit = " GeV"
        #    if self.properties["dist"] == "pTB2" :  extra_unit = " GeV"
        #    if self.properties["dist"] == "pTJ3" :  extra_unit = " GeV"
        #    if self.properties["dist"] == "pTV" :   extra_unit = " GeV"
        #    #if self.properties["dist"] == "VpT" :   extra_unit = " GeV" # new
        #    if self.properties["dist"] == "mjj" :   extra_unit = " GeV" # hack -> comment when trafoD
        #    if self.properties["dist"] == "mBB" :   extra_unit = " GeV"
            if self.properties["dist"] == "mVH" :   extra_unit = " / 100 GeV"
        #    if self.properties["dist"] == "mBBJ" :  extra_unit = " GeV"
        #y_ratio = round(self.data.hist.GetBinWidth(1),2)
        #if (y_ratio*10) % 10 == 0 and (y_ratio*100) % 100 == 0 : y_ratio = int(y_ratio)
        #if self.properties["dist"] == "VpT" : extra_str = " / bin" # new
        #else : extra_str = " / "+str(y_ratio)+extra_unit # new
        #if self.properties:
        #    if self.properties["dist"] == "MV1cB1" :   extra_str = ""
        #    if self.properties["dist"] == "MV1cB2" :   extra_str = ""
        #    if self.properties["dist"] == "MV1cBTag" : extra_str = ""
        #if "bin" in ytitle: extra_str = ""
        ytitle = ytitle+extra_unit
        extra_str=""
        hist.GetYaxis().SetTitle(ytitle+extra_str)
        if xbounds:
            hist.GetXaxis().SetRangeUser(xbounds[0], xbounds[1])
        return hist

    @init_finished
    def make_SoB_plot(self, can_name = "",do_ratio = True, Ratioybounds=(0.4, 1.6), ratioType = 0):
        """ Main function to create S/B plot
        """
        if can_name:
            cname = can_name
        else:
            cname = "can"
        print "plotting", cname

        tc = ROOT.TCanvas(cname, cname, 600, 600)
        if do_ratio:
            up, do = divide_canvas(tc, 0.25)
            up.cd()
            self.smallFontSize = 0.045
            self.legFontSize = 0.035
            self.normalFontSize = 0.05
        else:
            ROOT.gPad.SetLeftMargin(0.13)
            ROOT.gPad.SetRightMargin(0.075)
            self.smallFontSize = 0.03
            self.legFontSize = 0.025
            self.normalFontSize = 0.0375

        #first_h = self.draw_everything(True, True, "Events / bin", xtitle = "log_{10}(S/B)")
        first_h = self.draw_everything(True, True, xtitle = "log_{10}(S/B)")
        first_h.Draw("sameaxis")
        if do_ratio:
            do.cd()
            self.draw_SoB_ratio(True, Ratioybounds, "log_{10}(S/B)", ratioType=ratioType)
            up.cd()
        ROOT.gPad.SetLogy()
        leg_pos = [0.62, 0.48, 0.84, 0.88]
        leg = self.build_legend(leg_pos)
        draw(leg)
        ref_up = 0.85
        xref = 0.17
        self.draw_ATLAS_label((xref, ref_up))
        self.draw_lumi((xref, ref_up - self.normalFontSize - 0.01))
        return tc
    

    @init_finished
    def draw_data(self, first):
        """ Draw data """
        suff = suffix(first)
        if type(self.data.h) == ROOT.TH1F:
            draw(self.data.h, "e"+suff)
        else:
            if first:
                draw(self.data.h, "pza")
            else:
                draw(self.data.h, "pz")
        return self.data.h

    @init_finished
    def draw_bkg(self, first):
        """ Draw backgrounds alone """
        suff = suffix(first)
        draw(self.stack, "ahist"+suff)
        return self.stack

    @init_finished
    def draw_sig(self, first, is_expected=False, is_additional=False):
        """ Draw signal alone 

        Signal can be nominal or expected (mu=1)
        """
        if is_expected:
            histo = self.exp_sig.h
        elif is_additional:
            histo = self.add_sig.h
        else:
            histo = self.sig.h
        suff = suffix(first)
        draw(histo, "hist"+suff)
        return histo

    @init_finished
    def draw_bkg_plus_sig(self, first, is_expected=False, is_additional=False):
        """ Draw signal+backgrounds alone

        Signal can be nominal or expected (mu=1)
        """
        if is_expected:
            histo = self.exp_sig.h
            print "sig int is3 ",(self.exp_sig.h).Integral()
        elif is_additional:
            histo = self.add_sig.h
            print "sig int is2 ",(self.add_sig.h).Integral()
        else:
            histo = self.sig.h
            print "sig int is ",(self.sig.h).Integral()
        suff = suffix(first)
        tempstack = clone(self.stack)
        tempstack.Add(histo)
        draw(tempstack, "hist"+suff)
        return tempstack

    @init_finished
    def draw_MCstat(self, first):
        """ Draw MC stat error.

        WARNING: meaningless if called from a RooFit setup, as MC error bars are sqrt(value)
        """
        suff = suffix(first)
        self.hsum.SetFillColor(1)
        self.hsum.SetFillStyle(3004)
        self.hsum.SetMarkerStyle(0)
        draw(self.hsum, "e2"+suff)
        return self.hsum

    def GetGraphPoints(self, thebin, nbins):
      #gets the top of the error
      loweredge1=(2*thebin)+1
      loweredge2=(loweredge1+1)

      totaledges=(((nbins+3)*2)+1)*2
      #gets the bottom of the error
      upperedge1=(totaledges-(2+2*thebin))
      upperedge2=(totaledges-(3+2*thebin))
      return [loweredge1, loweredge2, upperedge1, upperedge2]

    @init_finished
    def draw_FitErr(self, first):
        #TODO modify fit error position here
        """ Draw Fit Error """
        shiftSys=True
        print "Drawing Fit Err"
        print type(self.bkgs)
        #print hist.GetStack().Last()
        #shiftSys=False
        if shiftSys:
          #get stack max?
          first=0
          tothist=clone(self.bkgs[0].h)#.Clone("newhist")
          for i in range(len(self.bkgs)):
            if i==0:
              continue
            print self.bkgs[i].h
            print self.bkgs[i].h.GetBinContent(2)
            tothist.Add(self.bkgs[i].h)
       
          #for backgrounds in self.bkgs:
          #  print backgrounds.h
          #  if first==0:
          #    first=first+1
          #  else:
          #    tothist.Add(backgrounds.h)
          #tothist=hist.GetStack().Last()
          nbins=tothist.GetNbinsX()
          for i in range(tothist.GetNbinsX()+2):
            #print "bin number", i
            #print "contents", tothist.GetBinContent(i)
            binheight=tothist.GetBinContent(i)
            print "bin no",i,binheight
            top1, top2, bottom1, bottom2 = self.GetGraphPoints(i, nbins)
            #print "topval1 is ",self.fiterr.h.GetX()[top1],self.fiterr.h.GetY()[top1] 
            #print "topval2 is ",self.fiterr.h.GetX()[top2],self.fiterr.h.GetY()[top2] 
            #print "bottomval1 is ",self.fiterr.h.GetX()[bottom1],self.fiterr.h.GetY()[bottom1] 
            #print "bottomval2 is ",self.fiterr.h.GetX()[bottom2],self.fiterr.h.GetY()[bottom2] 
            height1=(self.fiterr.h.GetY()[top1]-self.fiterr.h.GetY()[bottom1])/2.
            height2=(self.fiterr.h.GetY()[top2]-self.fiterr.h.GetY()[bottom2])/2.
 
            #print binheight+height1
            #print binheight-height1
            self.fiterr.h.GetY()[top1]=binheight+height1
            self.fiterr.h.GetY()[top2]=binheight+height2

            self.fiterr.h.GetY()[bottom1]=binheight-height1
            self.fiterr.h.GetY()[bottom2]=binheight-height2
            #print "new topval1 is ",self.fiterr.h.GetX()[top1],self.fiterr.h.GetY()[top1] 
            #print "new topval2 is ",self.fiterr.h.GetX()[top2],self.fiterr.h.GetY()[top2] 
            #print "new bottomval1 is ",self.fiterr.h.GetX()[bottom1],self.fiterr.h.GetY()[bottom1] 
            #print "new bottomval2 is ",self.fiterr.h.GetX()[bottom2],self.fiterr.h.GetY()[bottom2] 


            #need to shift the error centroid to be at the same point as the hist total
            #first get error "height", then divide by 2, find max of hist, set err max to be hist max + height/2, set err min to be hist max - height/2
            #add if switch to be able to turn off


        #print self.fiterr.h

        if first:
            draw(self.fiterr.h, "fa")
        else:
            draw(self.fiterr.h, "f")
        return self.fiterr.h

    def draw_PreFit(self, first):
        """ Draw PreFit line """
        suff = suffix(first)
        draw(self.prefit.h, "hist"+suff)
        return self.prefit.h

    def build_legend(self, coords):
        """ Create a legend with all existing stuff in the plot """
        leg = ROOT.TLegend(*coords)
        leg.SetFillColor(-1)
        leg.SetFillStyle(4000)
        leg.SetBorderSize(0)
        everything = [self.data]
        everything.append(self.exp_sig)
        sumbkgs = self.hsum.Integral()
        for b in reversed(self.bkgs):
            if b.h.Integral() > sumbkgs * Config.thresh_drop_legend:
                everything.append(b)
        if self.fiterr:
            everything.append(self.fiterr)

        if self.prefit:
            everything.append(self.prefit)
        if self.add_sig:
            everything.append(self.add_sig)
        props = self.properties
        is_not_topcr = True
        isVpT = False
        is2tag = False
        if props:
            if "Spc" in props:
                if props["Spc"] == "topcr" or props["Spc"] == "topemucr":
                    is_not_topcr = False
            if "dist" in self.properties:
                if props["dist"] == "VpT":
                    isVpT = True
            if "T" in self.properties:
                if props["T"] > 1:
                    is2tag = True
        #if self.exp_sig and not (isVpT and "dist" in self.properties) and (is2tag or "T" not in self.properties) and is_not_topcr:
            #everything.append(self.exp_sig)
        for ds in everything:
            ds.add_to_legend(leg)
        nobj = len(everything)
        fontSize = min(self.legFontSize, 0.5/nobj)
        #fontSize *= 1.12 # init
        fontSize *= 1.18
        leg.SetTextSize(fontSize)
        leg.SetY1(leg.GetY2() - 1.02*fontSize*nobj)
        return leg

    @init_finished
    def draw_dataMC_ratio(self, plot_ratio = False, ybounds=(.5, 1.5), xbounds=None, xtitle=""):
        """ Function to draw the bottom pad, i.e ratios

        Take care of horrendous things like adjusting all fontsizes
        """
        htemp = clone(self.data.h)
        xvals  = htemp.GetX()
        yvals  = htemp.GetY()
        yerrup = htemp.GetEYhigh()
        yerrdo = htemp.GetEYlow()
        ymax=-999
        ymin=999

        no_trafo=False
        if self.data.hist.GetNbinsX()==25 : no_trafo=True

        for i in range(htemp.GetN()):
            b = self.hsum.FindBin(xvals[i])
            sum_val = self.hsum.GetBinContent(b)
            #sum_val += self.exp_sig.h.GetBinContent(b)
            if sum_val != 0:
                yvals[i] /= sum_val
                yerrup[i] /= sum_val
                yerrdo[i] /= sum_val
            else:
                yvals[i] = 0
                yerrup[i] = 0
                yerrdo[i] = 0
            # for automatic y range 
            #tmp_max_criteria = 1 # init
            tmp_max_criteria = 0.4 # new
            if yerrup[i]<tmp_max_criteria or (self.properties and (self.properties["dist"] == "mva" or self.properties["dist"] == "mvaVZ")) :
                tmp_err_criteria = yerrup[i]
                if self.properties and self.properties["dist"] == "mva" and no_trafo: tmp_err_criteria = yvals[i]*0.05
                #
                if yvals[i]+tmp_err_criteria-1 > ymax:
                    ymax = yvals[i]+tmp_err_criteria-1
            if yerrdo[i]<tmp_max_criteria or (self.properties and (self.properties["dist"] == "mva" or self.properties["dist"] == "mvaVZ")) :
                tmp_err_criteria = yerrdo[i]
                if self.properties and self.properties["dist"] == "mva" and no_trafo: tmp_err_criteria = yvals[i]*0.05
                #
                if yvals[i]-tmp_err_criteria-1 < ymin:
                    ymin = yvals[i]-tmp_err_criteria-1
        tmp_range = max( abs(ymax), abs(ymin) )
        tmp_range*=1.05
        # avoid to go in blank place
        final_range = round(tmp_range,2)
        if (tmp_range*100)>=10. : final_range = round(tmp_range,1) # don't want to see 11% for example

        # round correctly
        bounds = [ 0.01 ]
        bounds += [ 0.05 * i for i in range(1, 20) ]
        bounds += [ 0.5 * i for i in range(2, 11) ]
        idx = bisect.bisect_left(bounds, final_range)
        if idx < len(bounds):
            final_range = bounds[idx]
        # round fix (checked on dPhiLB L1 J2 T2lmt)
        if final_range <= 1 : 
            if (final_range*100) % 10 == 0: final_range = round(final_range, 1)
            else : final_range = round(final_range, 2)
        else :
            if (final_range*10) % 10 == 0: final_range = int(final_range)
            else : final_range = round(final_range, 1)
        # to avoid horrible numbers due to bad ndiv
        ndiv=503
        if (final_range*100)%5==0 : ndiv=504
        # avoid overlap with higher canvas
        final_range_extra = 0.002
        if final_range > 0.01 and final_range < 1 : final_range_extra = 0.02
        if final_range >= 1 : final_range_extra = 0.2
        final_range += final_range_extra
        # fix 0.42 and 0.47 problem
        if final_range > 0.4  and final_range < 0.45 : final_range = 0.39
        if final_range > 0.45 and final_range < 0.5  : final_range = 0.52
        # max range = [2, 0] to avoid negative lower boundary
        if final_range > 1. : final_range = 1.

        hdash = clone(self.data.hist)
        hdash.Reset()
        for i in range(hdash.GetNbinsX()+1):
            hdash.SetBinContent(i, 1)
        hdash.SetLineStyle(ROOT.kDashed)
        #hdash.SetMaximum(ybounds[1])
        #hdash.SetMinimum(ybounds[0])
        hdash.SetMaximum(1+final_range)
        hdash.SetMinimum(1-final_range)
        if plot_ratio:
            hdash.GetXaxis().SetTitleSize(0.15)
            hdash.GetXaxis().SetLabelSize(0.15)
            hdash.GetYaxis().SetLabelSize(0.15)
            hdash.GetYaxis().SetTitleSize(0.15)
#            hdash.GetYaxis().SetNdivisions(503) # init
            hdash.GetYaxis().SetNdivisions(ndiv)
            #hdash.GetXaxis().SetNdivisions(505, ROOT.kFALSE)
            hdash.GetYaxis().SetTickLength(0.035)
            hdash.GetXaxis().SetTickLength(0.1)
            hdash.GetYaxis().SetTitleOffset(0.4)
            hdash.GetXaxis().SetTitleOffset(1.1)

        if self.properties:
            if "dist" in self.properties:
                if "MV1c" in self.properties["dist"]:
                    #self.relabel_mv1c_axis(hdash)
                    hdash.GetXaxis().SetLabelSize(0.2)

        draw(hdash, "hist")
        draw(htemp, "pz0")


        ## Put arrows in ratio for points outside the range
        scale_range = 0.9*final_range
        scale_err = 0.5*final_range
        g_val_x_up = []
        g_val_y_up = []
        g_err_x_up = []
        g_err_y_up = []
        g_val_x_do = []
        g_val_y_do = []
        g_err_x_do = []
        g_err_y_do = []
        for i in range(htemp.GetN()):
            b = self.hsum.FindBin(xvals[i])
            if yvals[i] - yerrdo[i] > 1+final_range :
                g_val_x_up.append(self.hsum.GetBinCenter(b))
                g_val_y_up.append(1+scale_range)
                g_err_x_up.append(0.)
                g_err_y_up.append(scale_err)
            elif yvals[i] + yerrup[i] < 1-final_range :
                g_val_x_do.append(self.hsum.GetBinCenter(b))
                g_val_y_do.append(1-scale_range)
                g_err_x_do.append(0.)
                g_err_y_do.append(scale_err)
        if len(g_val_x_up) > 0 :
            htemp_up = ROOT.TGraphErrors(len(g_val_x_up), array.array('f',g_val_x_up), array.array('f',g_val_y_up), array.array('f',g_err_x_up), array.array('f',g_err_y_up))
            htemp_up.SetMarkerStyle(22)
            draw(htemp_up, "ep")
        if len(g_val_x_do) > 0 :
            htemp_do = ROOT.TGraphErrors(len(g_val_x_do), array.array('f',g_val_x_do), array.array('f',g_val_y_do), array.array('f',g_err_x_do), array.array('f',g_err_y_do))
            htemp_do.SetMarkerStyle(23)
            draw(htemp_do, "ep")

        if self.fiterr:
            # creation of the ratio hist done here. Could be moved if needed
            errtmp = clone(self.fiterr.h)
            npoints = errtmp.GetN()
            yvals = errtmp.GetY()
            xvals = errtmp.GetX()
            for i in range(errtmp.GetN()/2):
                if abs(yvals[i] - yvals[npoints-i-1]) < 0.0001:
                    continue
                midpoint = (yvals[i] + yvals[npoints-i-1])/2.
                if(abs(midpoint) > 1e-6):
                    yvals[i] /= midpoint
                    yvals[npoints-i-1] /= midpoint
            draw(errtmp, "f")
        if xtitle:
            hdash.GetXaxis().SetTitle(xtitle)
        else:
            trafo_mva_extra = ""
            if self.properties and self.properties["dist"] == "mva" and no_trafo: trafo_mva_extra = "Untransformed "
            #
            hdash.GetXaxis().SetTitle(trafo_mva_extra+self.get_xTitle())
        #hdash.GetYaxis().SetTitle("Data/MC")
        hdash.GetYaxis().SetTitle("Data/Pred")
        if xbounds:
            hdash.GetXaxis().SetRangeUser(xbounds[0], xbounds[1])

        # cheat with latex for y axis mv1c
        if self.properties:
            if "dist" in self.properties:
                if "MV1c" in self.properties["dist"]:
                    tex_mv1c_axis(hdash)

    @init_finished
    def draw_SoB_ratio(self, plot_ratio = False, ybounds=(.5, 1.5), xtitle="", ratioType=0):
        """ Function to draw the bottom pad, i.e ratios, for S/B plot

        Take care of horrendous things like adjusting all fontsizes
        """
        hdata = clone(self.data.hist)
        hdata.Sumw2(True)
        # FIXME wtf why the Divide does not work ? Error bars are wrong !?
        #hdata.Divide(self.hsum)
        hsig = clone(self.exp_sig.h)
        for i in range(hdata.GetNbinsX()):
            if self.hsum.GetBinContent(i) != 0:
                D = hdata.GetBinContent(i)
                B = self.hsum.GetBinContent(i)
                S = hsig.GetBinContent(i)
                # data / bkg
                hdata.SetBinContent(i, D / B)
                hdata.SetBinError(i, math.sqrt(D) / B)
                hsig.SetBinContent(i, (S + B) / B)
                if ratioType is 1:
                    # (data - bkg) / sqrt(bkg)
                    hdata.SetBinContent(i, (D - B) / math.sqrt(B))
                    hdata.SetBinError(i, math.sqrt(D) / math.sqrt(B))
                    hsig.SetBinContent(i, S / math.sqrt(B))
                if ratioType is 2:
                    # (data - bkg) / signal
                    hdata.SetBinContent(i, (D - B) / S)
                    hdata.SetBinError(i, math.sqrt(D) / S)
                    hsig.SetBinContent(i, 1)
        #hsig = clone(self.exp_sig.h)
        #hsig.Add(self.hsum)
        #hsig.Divide(self.hsum)
        hsig.SetFillColor(0)
        hsig.SetLineColor(ROOT.kRed)
        hsig.SetLineWidth(4)
        gsig = make_tgraph_from_hist(hsig)

        hdash = clone(self.data.hist)
        hdash.Reset()
        for i in range(hdash.GetNbinsX()+1):
            hdash.SetBinContent(i, 1)
            if ratioType is not 0:
                hdash.SetBinContent(i, 0)
        hdash.SetLineStyle(ROOT.kDashed)
        hdash.SetMaximum(ybounds[1])
        hdash.SetMinimum(ybounds[0])

        if plot_ratio:
            hdash.GetXaxis().SetTitleSize(0.15)
            hdash.GetXaxis().SetLabelSize(0.15)
            hdash.GetYaxis().SetLabelSize(0.15)
            hdash.GetYaxis().SetTitleSize(0.15)
            #hdash.GetYaxis().SetNdivisions(503)
            #hist.GetXaxis().SetNdivisions(-1, ROOT.kFALSE)
            hist.GetXaxis().SetNdivisions(506, ROOT.kFALSE)
            hdash.GetYaxis().SetTickLength(0.035)
            hdash.GetXaxis().SetTickLength(0.1)
            hdash.GetYaxis().SetTitleOffset(0.4)
            hdash.GetXaxis().SetTitleOffset(1.1)
        draw(hdash, "hist")
        #draw(hsig, "histsame")
        draw(gsig, "l")
        draw(hdata, "esame")
        if xtitle:
            hdash.GetXaxis().SetTitle(xtitle)
        else:
            hdash.GetXaxis().SetTitle(self.get_xTitle())
        #hdash.GetYaxis().SetTitle("Data/Bkg")
        hdash.GetYaxis().SetTitle("Data/Pred")
        if ratioType is 1:
            hdash.GetYaxis().SetTitle("Pull (stat.)")
        if ratioType is 2:
            hdash.GetYaxis().SetTitle("(D-B)/S")

    def draw_title(self, pos):
        """ Draw title of the plot, i.e the category it shows """
        props = self.properties
        if not props:
            return
        nleps = props["L"]
        if nleps == '3':
            nleps = "0+1+2"
#            nleps = "0"
#            nleps = "1"
#            nleps = "2"
        njets = props.get("J", "2+3")
        ntags = props["T"]
        typedict = {"tt": " Tight", "mm": " Medium", "ll":" Loose", "xx":" Medium+Tight", "":""}
        if "TType" in props:
            ntags += typedict[props["TType"]]

        vptbin = int(props["B"])
        # year = props["Y"] # not used
        # dist = blocks[4] # not used
        l = self.new_TLatex(small = True)
        vptbin_dict = {}
        if "isMVA" in props:
            if props["isMVA"] == "0":
                vptbin_dict = {0: "p_{T}^{V}<90 GeV", 1: "90<p_{T}^{V}<120 GeV",
                               2: "120<p_{T}^{V}<160 GeV", 3: "160<p_{T}^{V}<200 GeV",
                               4: "p_{T}^{V}>200 GeV"}
                if nleps == "0":
                    vptbin_dict[1] = "100<p_{T}^{V}<120 GeV"
            else:
                vptbin_dict = {0: "p_{T}^{V}<120 GeV", 1: "100<p_{T}^{V}<120 GeV",
                               2: "p_{T}^{V}>120 GeV"}

        # search if there is Flv in props
        do_Flv = False
        for tmp_props in props :
            if tmp_props == "Flv" : do_Flv = True
        #
        flav_extra = ""
        if do_Flv :
            if props["Flv"] == "0" : flav_extra = " #mu"
            if props["Flv"] == "1" : flav_extra = " e"
        part1 = "{0} lep.{1}, ".format(nleps, flav_extra)
        if "Spc" in props and props["Spc"] == "topcr":
            part1 = "#splitline{" + part1
            part2 = "n>=2 jets, 2 tags}{m_{ll} sidebands}"
        elif "Spc" in props and props["Spc"] == "topemucr":
            part1 = "#splitline{" + part1
            part2 = "n>=2 jets, 2 tags}{e#mu selection}"
        else:
            print "nleps is ",nleps
            print "ntags is ",ntags
            if ntags == "1":
#                part2 = "{0} jets, 1 tag".format(njets)
                if int(nleps) != 2:
                  part2 = "1 tag, {1} add. tags".format(ntags, njets)
                else:
                  part2 = "1 tag"
                if "e" in njets:
                  part2 = "{0} tags, e#mu".format(ntags)
            else:
                #part2 = "{0} jets, {1} tags".format(njets, ntags)
                if int(nleps) != 2:
                  part2 = "{0} tags, {1} add. tags".format(ntags, njets)
                else:
                  part2 = "2 tag"
                if "e" in njets:
                  part2 = "{0} tags, e#mu".format(ntags)

        if ntags == -1 or ntags == -2:
            pos[1] -= 0.02

        theRegion = props["Reg"]
        print "using region", theRegion
        if "highMH" in theRegion: 
            part3 = "m_{H} > 145 GeV"
            pos_next = pos[1] - 2*self.normalFontSize
        elif  "lowMH" in theRegion: 
            part3 = "m_{H} < 75 GeV"
            pos_next = pos[1] - 2*self.normalFontSize
        elif  "mergedMH" in theRegion: 
            part3 = "m_{H} < 75 GeV || m_{H} > 145 GeV"
            pos_next = pos[1] - 2*self.normalFontSize
        elif  "SR" in theRegion: 
            part3 = "75 GeV < m_{H} < 145 GeV"
            pos_next = pos[1] - 2*self.normalFontSize
        else:
            part3=""

        print part3

        #if vptbin in vptbin_dict:
        #    part3 = "{0}".format(vptbin_dict[vptbin])
        #    pos_next = pos[1] - 2*self.normalFontSize
        #else:
        #    part3 = ""
        #    pos_next = pos[1] - self.normalFontSize

        #label = part1 + part2 + part3 # init
        #l.DrawLatex(pos[0], pos[1], label) # init
        l.DrawLatex(pos[0], pos[1], part1 + part2)
        if part3 != "":
            l.DrawLatex(pos[0], pos[1]-self.normalFontSize, part3)

        if not Config.weighted:
            pass
            #self.draw_chi2((pos[0], pos_next))
        else:
            self.draw_weight_title((pos[0], pos_next))

    def draw_chi2(self, pos):
        """ Draw chi2 if present """
        if self.chi2 == -1:
            return
        l = self.new_TLatex(small = True)
        ts_chi2 = "#chi^{{2}}={0:1.1f}".format(self.chi2)
        l.DrawLatex(pos[0], pos[1], ts_chi2)

    def draw_weight_title(self, pos):
        """ Draw special line for weighted plots """
        l = self.new_TLatex(small = True)
        text = "Weighted by {0} S/B".format(Config.weighted)
        l.DrawLatex(pos[0], pos[1], text)

    def draw_lumi(self, pos):
        """ Draw Lumi for 1 or 2 years """
        l = self.new_TLatex(small = True)
        l.SetTextFont(42)
        def draw_line(x, y, s, lumi):
            l.DrawLatex(x, y, "#sqrt{{s}} = {1} TeV #scale[0.65]{{#int}}Ldt = {0} fb^{{-1}} ".format(lumi, s))
        lumi7 = Config.lumi_7
        if Config.year == 4023:
            draw_line(pos[0], pos[1], 7, lumi7)
            draw_line(pos[0], pos[1] - 1.7*self.smallFontSize, 8, Config.lumi_8)
        elif Config.year == 2015:
            draw_line(pos[0], pos[1], 13, Config.lumi_8)
        elif Config.year == 2011:
            draw_line(pos[0], pos[1], 7, lumi7)

    def draw_ATLAS_label(self, pos):
        """ Reimplementation of the official macro with better spacing """
        x = pos[0]
        y = pos[1]
        l = self.new_TLatex()
        delx = 0.13
        l.DrawLatex(x, y, "ATLAS")
        p = self.new_TLatex()
        p.SetTextFont(42)
        p.DrawLatex(x+delx, y, Config.ATLAS_suffix)

    def get_xTitle(self):
        """ get title of X-axis from 'self.title' """
        if not self.properties:
            return ""
        varname = self.properties["dist"]
        labels = {
            # new
            #"MV1cB1": "MV1c(b_{1}) OP",
            #"MV1cB2": "MV1c(b_{2}) OP",
            #"MV1cBTag": "MV1c(b) OP",
            #"dEtaBB": "#Delta#eta(b_{1},b_{2})",
            #"dEtaVBB": "#Delta#eta(V,bb)",
            #"dPhiLBmin": "#Delta#phi(lep,b)_{min}",
            #"dPhiVBB": "#Delta#phi(V,bb)",
            #"dRBB": "#DeltaR(b_{1},b_{2})",
            #"MEff": "M_{eff} [GeV]",
            #"MEff3": "M_{eff3} [GeV]",
            #"MEff": "H_{T} [GeV]",
            #"MEff3": "H_{T} [GeV]",
            "MET": "E_{T}^{miss} [GeV]",
            #"mLL": "M_{ll} [GeV]",
            "mJet":"m_{jet} [GeV]",
            "pTJet":"p_{T, jet} [GeV]",
            "nBTag" :"n_{b-tag}",
            #"mVH":"m_{VH} [GeV]",
            "mVH":"m_{T, VH} [GeV]",
            "mtw" :"m_{T, W} [GeV]",
            "ptw" :"p_{T, Z} [GeV]",
            "pTL1" :"p_{T, l} [GeV]",
            "pTL" :"p_{T, l} [GeV]",
            "mll" :"m_{l l} [GeV]",
            "pTV":"p_{T, Z} [GeV]"


            #"mTW": "m_{T}(W) [GeV]",
            #"mva": "BDT_{VH}",
            #"mvaVZ": "BDT_{VZ}",
            #"pTB1": "p_{T}(b_{1}) [GeV]",
            #"pTB2": "p_{T}(b_{2}) [GeV]",
            #"pTJ3": "p_{T}(j_{3}) [GeV]",
            #"pTV": "p_{T}^{V} [GeV]",
            #"mVH": "m_{VH} [GeV]",
            #"VpT": "p_{T}^{V} [GeV]"
        }

        if "pTV" in varname:
          if self.properties["L"] == 0:
            return "p_{T, Z} [GeV]"
          if self.properties["L"] == 1:
            return "p_{T, W} [GeV]"
          if self.properties["L"] == 2:
            return "p_{T, Z} [GeV]"

        if "mVH" in varname:
          if self.properties["L"] == 0:
            return "m_{T, VH} [GeV]"
          if self.properties["L"] == 1:
            return "m_{VH} [GeV]"
          if self.properties["L"] == 2:
            return "m_{VH} [GeV]"
          
        if "mjj" in varname:
            # nominal
            tmp_extra = ""
            tmp_extra2 = " [GeV]"
            # hack for mjj trafo D
            #tmp_extra = "Transformed "
            #tmp_extra2 = ""
            #
            if self.properties["T"] == "2":
                return tmp_extra+"m_{bb}"+tmp_extra2
            elif self.properties["T"] == "1":
                return tmp_extra+"m_{bj}"+tmp_extra2
            else:
                return tmp_extra+"m_{jj}"+tmp_extra2
        elif "mBBJ" in varname:
            if self.properties["T"] == "2":
                return "m_{bbj} [GeV]"
            elif self.properties["T"] == "1":
                return "m_{bjj} [GeV]"
            else:
                return "m_{jjj} [GeV]"
        elif "mBB" in varname:
            if self.properties["T"] == "2":
                return "m_{bb} [GeV]"
            elif self.properties["T"] == "1":
                return "m_{bj} [GeV]"
            else:
                return "m_{jj} [GeV]"
        elif varname in labels:
            return labels[varname]
            #for k in labels:
                #if k in varname:
                    #return labels[k]
        return varname

    def new_TLatex(self, small = False):
        """ Create a simple TLatex object with correct properties """
        l = ROOT.TLatex()
        l.SetNDC()
        l.SetTextFont(72)
        if small:
            l.SetTextSize(self.smallFontSize)
        else:
            l.SetTextSize(self.normalFontSize)
        l.SetTextColor(1)
        l.SetTextAlign(11)
        return l

    def plot_with_additional_signal(self):
        return False
        if self.isBkgSub:
            return False
        props = self.properties
        if props:
            if props["dist"] == "MEff" :      return True 
            if props["dist"] == "MEff3" :     return True 
            if props["dist"] == "MET" :       return True 
            if props["dist"] == "mLL" :       return True 
            if props["dist"] == "mTW" :       return True 
            if props["dist"] == "pTB1" :      return True 
            if props["dist"] == "pTB2" :      return True 
            if props["dist"] == "pTJ3" :      return True 
            if props["dist"] == "pTV" :       return True 
            if props["dist"] == "VpT" :       return True 
            if props["dist"] == "mva" :       return True
            if props["dist"] == "mvaVZ" :     return True
            if props["dist"] == "mjj" :       return True 
            if props["dist"] == "mBB" :       return True 
            if props["dist"] == "mBBJ" :      return True 
            if props["dist"] == "MV1cB1" :    return True
            if props["dist"] == "MV1cB2" :    return True
            if props["dist"] == "MV1cBTag" :  return True
            if props["dist"] == "dEtaBB" :    return True
            if props["dist"] == "dEtaVBB"  :  return True
            if props["dist"] == "dPhiLBmin" : return True
            if props["dist"] == "dPhiVBB" :   return True
            if props["dist"] == "dRBB" :      return True
        return False



class SetupMaker(object):
    """ A class that creates a Setup from various configurations """
    def __init__(self, title="", mass=125, muhat=None, guess_properties=True):
        #if "2012" in title:
        Config.year = 2015
        Config.mass = mass
        #elif "2011" in title:
        #    Config.year = 2011
        #elif "both" in title:
        #    Config.year = 4023
        self.setup = Setup(title, guess_properties)
        print "setting mass as ", mass
        self.mass = mass
        self.muhat = muhat

    def make_from_directory(self, tdir):
        """ To be revived FIXME """
        for hist,name in ((k.ReadObj(), k.GetName()) for k in tdir.GetListOfKeys() if k.ReadObj().InheritsFrom("TH1")):
            self.add(name, hist)

    def make_from_ws_dir(self, tdir):
        """ To be revived FIXME """
        for key in tdir.GetListOfKeys():
            name = key.GetName()
            subdir = key.ReadObj()
            hist = [k.ReadObj() for k in subdir.GetListOfKeys() if not "_Sys" in k.GetTitle()]
            if len(hist) == 0:
                print "make_from_directory():: could not find histogram "+name+" in region "+tdir.GetName()
                return
            if len(hist)>1:
                print "make_from_directory():: found more than 1 histogram "+name+" in region "+tdir.GetName()
                return
            self.add(name, hist[0])

    def rebin_plots(func):
        """ decorator to add() to extend functionality
        """
        def f(self, name, hist):
            new_hist = hist
            # first, tackle bin label issues with MV1c
            if isinstance(hist, ROOT.TH1):
                props = self.setup.properties
                if props:
                    if "MV1c" in props["dist"]:
                        relabel_mv1c_axis(hist)
            # Then, rebin 1 lepton histos that were made in MeV
            if isinstance(hist, ROOT.TH1) or isinstance(hist, ROOT.TGraph):
                props = self.setup.properties
                if props:
                    # Changes for MeV/GeV
                    affected_dists = ["MEff", "MEff3", "MET", "mLL", "mTW", "pTB1", "pTB2", "pTJ3", "pTV", "mBB", "mBBJ"]
                    if props["L"] == "1" and props["dist"] in affected_dists:
                        new_hist = change_MeV_GeV(hist)
            return func(self, name, new_hist)
        return f

    @rebin_plots
    def add(self, name, hist):
        """ Add objects one by one to the setup """
        print "doing", name
        if name == "data" or name == "My_Data":
            self.setup.data = Data(hist)
        elif "BeforeFit" in name:
            return
        #elif "prefit" in name:
        #    return
        elif "DataAsym" in name:
            return
        elif "My_FitError_AfterFit" in name:
            self.setup.fiterr = FitError(hist)
        elif name == "error":
            self.setup.fiterr = FitError(hist)
        elif True in (signame in name for signame in Signal.names): # signal samples
            print "found signal"
            if not str(self.mass) in name:
                return
            if self.setup.sig:
                print "added sig"
                self.setup.sig.append(hist)
            else:
                print "added si2g"
                print "mass" , self.mass
                print "muhat" , self.muhat
                self.setup.sig = Signal(hist, self.mass, muhat=self.muhat)
                print "sig int early", (self.setup.sig.h).Integral()
        elif name == "mass":
            self.setup.mass = hist
            self.mass = hist
        elif name == "chi2":
            self.setup.chi2 = hist
        elif name == "prefit":
            self.setup.prefit = PreFit(hist)

        else:
            try :
                print "if"
                if name.startswith("My"):
                    name = name[3:]
                print "config"
                Config.__getattribute__(Config, name)
                print "self"
                self.setup.append_bkg(Bkg(hist, name))
            except AttributeError:
                print "add():: MC named {0} is unknown. Skip it !".format(name)

def change_MeV_GeV(hist):
    if isinstance(hist, ROOT.TH1):
        new_hist = hist.Clone()
        bins = new_hist.GetXaxis().GetXbins()
        for i in range(bins.GetSize()):
            bins[i] /= 1000.
        new_hist.SetBins(bins.GetSize()-1, bins.GetArray())
        for i in range(new_hist.GetNbinsX()+2):
            new_hist.SetBinContent(i, hist.GetBinContent(i))
            new_hist.SetBinError(i, hist.GetBinError(i))
    elif isinstance(hist, ROOT.TGraph):
        new_hist = hist
        xbins = new_hist.GetX()
        for i in range(new_hist.GetN()):
            xbins[i] /= 1000.
        if isinstance(hist, ROOT.TGraphAsymmErrors):
            xbinsup = new_hist.GetEXhigh()
            xbinsdo = new_hist.GetEXlow()
            for i in range(new_hist.GetN()):
                xbinsup[i] /= 1000.
                xbinsdo[i] /= 1000.
    return new_hist

def relabel_mv1c_axis(hist):
    nbins = hist.GetXaxis().GetNbins()
    if nbins is 4 or 5:
        print "Adjusting x-axis labels..."
        for ibin in range(1, nbins+1):
            hist.GetXaxis().SetBinLabel(ibin, "")
            #hist.GetXaxis().SetNdivisions(-1)
            hist.GetXaxis().SetNdivisions(505, ROOT.kFALSE)

def tex_mv1c_axis(hist):
    ranges = ["100", "80", "70", "60", "50", "0"]
    nbins = hist.GetXaxis().GetNbins()
    if nbins is 4 or 5:
        print "write tex x-axis labels..."
        for ibin in range(1, nbins+2):
            binlabel = ranges[ibin - nbins + 4]
            pos_x = 0.11 + (ibin-1)*(0.907-0.11)/nbins
            if binlabel == "0" : pos_x += 0.01
            if binlabel == "100" : pos_x -= 0.01
            pos_y = 0.26
            l = ROOT.TLatex()
            l.SetNDC()
            l.SetTextFont(42)
            l.SetTextSize(0.15)
            l.SetTextColor(1)
            l.SetTextAlign(11)
            l.DrawLatex(pos_x, pos_y, binlabel)

# Types of samples:

class Data(object):
    """ A class that represents data 
    At present self.h is a TGraphAsymmErrors, self.hist is a TH1 built from it """
    # TODO: put ifs to handle h as both TGraph and TH1 gracefully
    def __init__(self, h):
        self.h = clone(h)
        self.h.SetMarkerColor(ROOT.kBlack)
        self.h.SetMarkerStyle(20)
        self.h.SetMarkerSize(1)
        if Config.year>2100: # UGLY
            self.title = "Data"
        else:
            self.title = "Data {0}".format(Config.year)

    def blind(self, minval, maxval):
        """ Remove data points between minval and maxval """
        if type(self.h) == ROOT.TH1F:
            for i in range(1, self.h.GetNbinsX()+1):
                bincenter = self.h.GetXaxis().GetBinCenter(i)
                if bincenter > minval and bincenter < maxval:
                    self.h.SetBinContent(i, 0)
                    self.h.SetBinError(i, 0)
        else:
            xvals = self.h.GetX()
            xerrup = self.h.GetEXhigh()
            xerrdo = self.h.GetEXlow()
            to_remove = [i for i in range(self.h.GetN())
                         if (xvals[i]+xerrup[i])>minval and (xvals[i]-xerrdo[i])<maxval]
            for i in reversed(to_remove):
                self.h.RemovePoint(i)

    def clean(self):
        """ Remove empty data points """
        if type(self.h) == ROOT.TH1F:
            return
        yvals = self.h.GetY()
        to_remove = [i for i in range(self.h.GetN()) if yvals[i] == 0]
        for i in reversed(to_remove):
            self.h.RemovePoint(i)

    def add_to_legend(self,leg):
        """ Add object to legend """
        leg.AddEntry(self.h, self.title, "pl")

    def make_data_hist(self, hist_model):
        """ Create histogram from TGraphAsymmErrors """
        if type(self.h) == ROOT.TH1F:
            self.hist = clone(self.h)
        else:
            self.hist = clone(hist_model)
            self.hist.Reset()
            yvals = self.h.GetY()
            xvals = self.h.GetX()
            yerrup = self.h.GetEYhigh()
            yerrdo = self.h.GetEYlow()
            for i in range(self.h.GetN()):
                b = self.hist.FindBin(xvals[i])
                self.hist.SetBinContent(b, self.hist.GetBinContent(b) + yvals[i])
                errmean = (yerrup[i] + yerrdo[i]) / 2
                curr_err = self.hist.GetBinError(b)
                new_err = (errmean**2 + curr_err**2)**.5
                self.hist.SetBinError(b, new_err)
        self.hist.SetMarkerColor(ROOT.kBlack)
        self.hist.SetMarkerStyle(20)
        self.hist.SetMarkerSize(1)
        self.hist.SetFillStyle(0)

    def subtract(self, hist):
        """ Subtract contents of hist from self TGraphAsymmErrors """
        res = clone(self.h)
        if type(res) == ROOT.TH1F:
            res.Add(hist, -1)
        else:
            yvals = res.GetY()
            xvals = res.GetX()
            for i in range(res.GetN()):
                b = hist.FindBin(xvals[i])
                val = hist.GetBinContent(b)
                yvals[i] -= val
        return Data(res)


class PreFit(object):
    """ A class that represents prefit sum of MC """
    def __init__(self, couple):
        hist = couple[0]
        leg = couple[1]
        self.h = clone(hist)
        self.h.SetLineColor(ROOT.kBlue)
        self.h.SetLineWidth(4)
        self.h.SetLineStyle(ROOT.kDashed)
        self.title = leg

    def add_to_legend(self,leg):
        """ Add object to legend """
        leg.AddEntry(self.h, self.title, "l")

    def subtract(self, hist):
        """ Subtract contents of hist from self """
        res = clone(self.h)
        res.Add(hist, -1)
        return PreFit((res, self.title))



class FitError(object):
    """ A class that represents postfit error """
    def __init__(self, hist):
        self.h = clone(hist)
        self.h.SetFillColor(ROOT.kBlack)
        self.h.SetLineColor(ROOT.kBlack)
        #self.h.SetFillStyle(3444) # > 3100 problems -> need to find new bands
        self.h.SetFillStyle(3004) # init
        self.h.SetMarkerStyle(0)
        self.title = "Uncertainty"

    def add_to_legend(self,leg):
        """ Add object to legend """
        leg.AddEntry(self.h, self.title, "f")

    def subtract(self, hist):
        """ Subtract contents of hist from self

        LOOKS BUGGED FIXME
        """
        res = clone(self.h)
        yvals = res.GetY()
        xvals = res.GetX()
        for i in range(res.GetN()):
            b = hist.FindBin(xvals[i])
            val = hist.GetBinContent(b)
            if yvals[i]!=0:
                yvals[i] -= val
        return FitError(res)

    def center_at_0(self):
        """ Center this crazy TGraphAsymmErrors around 0 """
        res = clone(self.h)
        yvals = res.GetY()
        xvals = res.GetX()
        size = res.GetN()
        for i in range(size):
            mean = (yvals[i] + yvals[size-1-i]) /2
            yvals[i] -= mean
            yvals[size-1-i] -= mean
        return FitError(res)


class Signal(object):
    """ A class that represents signal MC """
    names = ["HVTZH", "HVTWH"]#,"WlvH", "ZllH", "ZvvH", "VH", "WH", "ZH"]
    STACK = 1
    OVERPRINT = 2

    def __init__(self, hist, mass, muhat=None, is_expected=False, is_additional=False):
        self.h = clone(hist)
        self.mass = mass
        if is_expected:
            sigconf = Config.expected_signal
        elif is_additional:
            sigconf = Config.additional_signal
        else:
            sigconf = Config.signal
        print "signal label",sigconf
        print mass
        if str(mass/100.) not in sigconf[0]:
          sigconf[0] = sigconf[0]+"(M="+str(mass/100.)+" TeV)"
          #sigconf[0] = sigconf[0]+"(M="+str(mass)+"0)"
        self.title = sigconf[0]
        self.mode = sigconf[1]
        if self.mode == Signal.STACK:
            self.h.SetFillColor(sigconf[2])
            self.h.SetLineWidth(0)
        else:
            print "MODE IS NOT Signal.STACK", Signal.STACK, " ", self.mode
            self.h.SetFillColor(0)
            self.h.SetLineColor(sigconf[2])
            #self.h.SetLineStyle(ROOT.kDashed)
            self.h.SetLineWidth(5)
        self.mult_factor = sigconf[3]
        #if self.mult_factor > 1:
        #    self.title += "#times{0}".format(self.mult_factor)
        self.has_scaled = False
        self.muhat = muhat

    def scale(self):
        """ Scale histogram by a multiplicative factor """
        if not self.has_scaled:
            self.h.Scale(self.mult_factor)
            self.has_scaled = True

    def append(self, hist):
        """ Merge different signal samples """
        self.h.Add(hist)

    def add_to_legend(self,leg):
        """ Add object to legend """
        if self.mode == Signal.STACK:
            leg.AddEntry(self.h, self.title, "f")
        else:
            leg.AddEntry(self.h, self.title, "l")

    def make_expected_signal(self):
        """ Create a new Signal object with muhat = 1 """
        print "muhat:", self.muhat
        print (self.h).Integral()
        if self.muhat is None:
            return None
        newsig = Signal(self.h, self.mass, 1., True)
        if self.muhat != 0:
            newsig.mult_factor /= self.muhat
        newsig.scale()
        return newsig

    def make_additional_signal(self, new_mult_factor, dist, prop):
        """ Create a new Signal object with muhat = 1 """
        print "muhat:", self.muhat
        if self.muhat is None:
            return None
        newsig = Signal(self.h, self.mass, 1., False, True)
        if self.muhat != 0:
            tmp_scale=0.5
            new_mult_factor=tmp_scale*new_mult_factor/self.h.Integral()
            if dist == "mBB"  : new_mult_factor /= 4
            if dist == "mjj"  : new_mult_factor /= 2
            if dist == "mva"  : new_mult_factor /= 2
            if dist == "mBBJ" : new_mult_factor /= 2
            if dist == "dRBB" : new_mult_factor /= 2
            if dist == "mva" and prop == "0" : new_mult_factor *= 0.8
            if dist == "MV1cBTag" : new_mult_factor *= 0.5
            # do new_mult_factor rounded
            new_mult_factor = int(new_mult_factor)
            if new_mult_factor > 9 :
              tmp_new_mult_factor = new_mult_factor%10
              if tmp_new_mult_factor != 0 :
                if tmp_new_mult_factor < 5 : new_mult_factor = new_mult_factor - tmp_new_mult_factor
                else                       : new_mult_factor = new_mult_factor + (10 - tmp_new_mult_factor)
            else :
              if new_mult_factor > 4 : new_mult_factor = 10
              else : new_mult_factor = 1
            # pass final new_mult_factor
            newsig.mult_factor = new_mult_factor
            newsig.title += "#times{0}".format(int(new_mult_factor))
        newsig.scale()
        return newsig



class Bkg(object):
    """ A class that represents background MC """
    def __init__(self, hist, name):
        self.h = clone(hist)
        bkgconf = Config.__getattribute__(Config, name)
        print "bname", name
        print bkgconf
        self.title = bkgconf[0]
        self.h.SetFillColor(bkgconf[2])
        self.h.SetLineWidth(0)
        self.h.SetFillStyle(1001)
        if len(bkgconf) > 4:
            self.h.SetFillStyle(bkgconf[4])
            self.h.SetLineWidth(2)
            self.h.SetLineColor(ROOT.kBlack)
        self.priority = bkgconf[1]
        self.name = name
        self.mergelist = bkgconf[3]


    def __lt__(self, other):
        return self.priority < other.priority

    def __eq__(self, other):
        return self.priority == other.priority

    def add_to_legend(self,leg):
        """ Add object to legend """
        leg.AddEntry(self.h, self.title, "f")

    def append(self, other):
        """ Merge a Bkg object with the current one """
        self.h.Add(other.h)


def suffix(first_drawn):
    """ False->"same" or True->"" """
    if first_drawn:
        return ""
    else:
        return "same"


def divide_canvas(canvas, ratio_fraction):
    """ Divide the canvas into two pads; the bottom is ratio_fraction tall.
    """
    canvas.Clear()
    p1 = ROOT.TPad("p1", "p1", 0, ratio_fraction, 1, 1)
    #p1.SetFillStyle(4000) # transparent
    p1.Draw()
    #p1.SetBottomMargin(0.00) # init
    p1.SetBottomMargin(0.012) # test
    p2 = ROOT.TPad("p2", "p2", 0, 0 ,1 , ratio_fraction)
    #p2.SetFillStyle(4000) # transparent
    #p2.SetTopMargin(0.04) # top margin -> init
    p2.SetTopMargin(0.045) # top margin -> test
    p2.SetBottomMargin(0.4)
    p1.SetLeftMargin(0.13)
    p1.SetRightMargin(0.075)
    p2.SetLeftMargin(0.13)
    p2.SetRightMargin(0.075)
    p2.Draw()
    return p1, p2


# All util functions to workaround pyROOT shortcomings concerning pointers and scope

def clone(hist):
    """ Create real clones that won't go away easily """
    h_cl = hist.Clone()
    if hist.InheritsFrom("TH1"):
        h_cl.SetDirectory(0)
    release(h_cl)
    return h_cl

def draw(obj, opt=""):
    """ Draw something that will stay, even when current file is closed or function returns """
    # if is already released, do not clone/release
    if obj in pointers_in_the_wild:
        obj.Draw(opt)
    elif obj.InheritsFrom("TH1"):
        clone(obj).Draw(opt)
    else:
        release(obj).Draw(opt)

pointers_in_the_wild = set()
def release(obj):
    """ Tell python that no, we don't want to lose this one when current function returns """
    global pointers_in_the_wild
    pointers_in_the_wild.add(obj)
    ROOT.SetOwnership(obj, False)
    return obj

def purge():
    """ Delete all objects that we took control of """
    global pointers_in_the_wild
    for p in pointers_in_the_wild:
        p.Delete()
    pointers_in_the_wild.clear()


def getPropertiesFromTag(regname):
    """ decompose a region name in nleptons, ntags, njets, VpT, year, distribution... with WSMaker 2 conventions """
    properties = {}
    blocks = regname.split('_')[1:] # remove "Region_"
    lepstring = regname.split('_')[0]
    print "blocks",blocks
    print "leps",lepstring
    properties.update({"dist":blocks[-1]})
    properties.update({"T":blocks[0].replace("tag", "")})
    properties.update({"J": blocks[1][0]})
    properties.update({"Reg": blocks[-2]})
    if "lv" in lepstring:
      properties.update({"L": 1})
    elif "ll" in lepstring:
      properties.update({"L": 2})
    elif "vv" in lepstring:
      properties.update({"L": 0})
      #else:
    else:
        properties.update({"L": -1})
    properties.update({"B": -1})

    #for b in blocks:
    #    properties.update(getTagValuePair(b))
    return properties

def getTagValuePair(block):
    tags = ["Y", "L", "J", "T", "TType", "B", "Flv", "Sgn", "isMVA", "dist", "Spc"]
    print "block",block
    matching_tags = [t for t in tags if block.startswith(t)]
    # if several matches, the right one is the one that macthes with most letters
    the_tag = max(matching_tags, key=len)
    val = block.replace(the_tag, '', 1)
    print block, the_tag, val
    return {the_tag : val}

def make_tgraph_from_hist(h):
    g = ROOT.TGraph(h)
    g.Expand(2*h.GetNbinsX())
    for i in range(h.GetNbinsX()):
        g.SetPoint(2*i, h.GetBinLowEdge(i+1), h.GetBinContent(i+1))
        g.SetPoint(2*i+1, h.GetBinLowEdge(i+2), h.GetBinContent(i+1))
    yvals = g.GetY()
    #to_remove = [i for i in range(g.GetN()) if yvals[i]==0]
    #for i in reversed(to_remove):
        #g.RemovePoint(i)
    return g


# Configuration of samples and plotting
class Config(object):
    """ Configuration of the plots """
    #norm=1#nothing
    #norm=2.4#0Lep
    #norm=13.7#1Lep
    #norm=3.3 #2Lep
    #norm=16 #fake
    norm=68 #fake
    #norm=12 #fake
    signal = ["HVT", Signal.STACK, ROOT.kRed + 1, norm] # last = mult factor
    expected_signal = ["HVT", Signal.STACK, ROOT.kRed + 1, norm] # last = mult factor
    additional_signal = ["HVT", Signal.STACK, ROOT.kRed + 1, norm] # last = mult factor
    #expected_signal = ["VH(bb) (#mu=1.0)", Signal.STACK, ROOT.kRed +1, 1]
    #additional_signal = ["VH(bb)", Signal.OVERPRINT, ROOT.kRed +1, 1.]
    ttbar = ("t#bar{t}", 42, ROOT.kOrange, ())
    TTbar = ("t#bar{t}", 42, ROOT.kOrange, ())
    #stopst = ("t", 41, ROOT.kYellow - 7, ["stopWt"]) # merges the two samples
    #stopWt = ("t", 40, ROOT.kYellow - 7, ["stopst"])
    stopt = ("t, s+t chan", 41, ROOT.kOrange - 1, ["stops"])
    stops = ("t, s+t chan", 41, ROOT.kOrange - 1, ["stopt"])
    stopWt = ("Wt", 40, ROOT.kYellow - 7, [])
    STop = ("Single top", 40, ROOT.kOrange - 1, [])
    Zb = ("Z+b", 25, ROOT.kAzure + 3, [])
    Zbc = ("Z+bc", 24, ROOT.kAzure + 2, [])
    Zbl = ("Z+bl", 23, ROOT.kAzure + 1, [])
    Zcl = ("Z+cl", 21, ROOT.kAzure - 8, [])
    Zc = ("Z+c", 22, ROOT.kAzure - 4, [])
    Zhf = ("Z+hf", 22, ROOT.kAzure + 2, [])
    Zl = ("Z+l", 20, ROOT.kAzure - 9, [])
    Wbl = ("W+bl", 33, ROOT.kGreen + 2, [])
    Wbb = ("W+bb", 35, ROOT.kGreen + 4, [])
    Wbc = ("W+bc", 34, ROOT.kGreen + 3, [])
    Wcc = ("W+cc", 32, ROOT.kGreen + 1, [])
    Whf = ("W+hf", 32, ROOT.kGreen + 3, [])
    Wcl = ("W+cl", 31, ROOT.kGreen - 6, [])
    Wl = ("W+l", 30, ROOT.kGreen - 9, [])
    Wb = ("W+b", 35, ROOT.kGreen + 4, [])
    Wc = ("W+c", 32, ROOT.kGreen + 1, [])
    WZ = ("WZ", 53, ROOT.kGray + 1, ["ZZ"])
    ZZ = ("ZZ", 52, ROOT.kGray + 1, ["WZ"])
    VZ = ("VZ", 51, ROOT.kGray + 1, [])
    diboson = ("Diboson", 51, ROOT.kGray + 1, [])
    VV = ("Diboson", 51, ROOT.kGray + 1, [])
    WW = ("WW", 50, ROOT.kGray + 3, [])
    Diboson = ("Diboson", 50, ROOT.kGray + 1, [])
    multijet = ("Multijet", 39, ROOT.kViolet-9, ["multijetMu", "multijetEl"])
    VH = ("VH", 39, ROOT.kViolet-9, [])
    qqWlvH125 = ("VH", 39, ROOT.kViolet-9, [])
    SMZh = ("VH", 39, ROOT.kViolet-9, [])
    multijetEl = ("Multijet", 39, ROOT.kViolet-9, ["multijetMu", "multijet"])
    multijetMu = ("Multijet", 39, ROOT.kViolet-9, ["multijetEl", "multijet"])
    bkg = ("Background", 60, ROOT.kGray+1, [], 3353)
    year = 2015
    blind = False
    mass = "125"
    ATLAS_suffix = "Internal"
    #ATLAS_suffix = "Simulation"
    #ATLAS_suffix = "Preliminary"
    #ATLAS_suffix = ""
    lumi_7 = "4.7"
    lumi_8 = "3.2"
    weighted = False
    #thresh_drop_legend = 0.01
    thresh_drop_legend = 0

