import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine(ROOT.gSystem.ExpandPathName('.x $ROOTCOREDIR/scripts/load_packages.C'))

from ROOT import RF as RF
from ROOT import PullPlotter as PullPlotter


if __name__ == '__main__':
  
  masspoint='700'
  SignalSample='ggHWWNWA'

  releaseDir = '/a/data/xenia/users/kiord/2016_09_13_TAincl'
  analysis = 'VVlvqq_mc15_v2016'
  inputListTag = 'itest01'
  outputWSTag = 'wsResolvedggFOnly_'+SignalSample+'{mass}'.format(mass=masspoint)

  # retrieve results
  results = RF.StatisticalResultsCollection('lvqqtest')
  filename = '/data/users/kiord/StatAnalysis/ArcondCalib/submit_statanalysis_lvJ_700ranking_v2/Job/run0_xenia00.nevis.columbia.edu/Job.ShellScript.xenia00.nevis.columbia.edu.vXdbp/results_VVlvqq_mc15_v2016_wsResolvedggFOnly_ggHWWNWA700.root'
  results.retrieve(filename)

  # plot
  pullMasses =  [700]
  
  pullplotter = PullPlotter('lvJpulltest', '.')

  for i in pullMasses : 
    pullplotter.addOneMassPoint(i)
  pullplotter.setOutputFormat(RF.Plotter.pdf)

  lvqq = RF.VVlvqqAnalysisRunner(analysis)
  pullplotter.process(lvqq.getStatResults())


  
  exit()
  plotter = RF.Plotter('lvqqtest', '.')
  plotter.setVarName('m_{HVT}')
  plotter.setVarUnit('GeV')

  k=1;
  BrZqq = 0.6991 ;
  BrWlv = (0.1075+0.1057+0.1125)
  Br=BrZqq*BrWlv ;

  plotter.setSpec(2600, Br, 0.0008401*k) # note the 1000x factor
  plotter.setSpec(2400, Br, 0.0013530*k)
  plotter.setSpec(2200, Br, 0.002225*k)
  plotter.setSpec(2000, Br, 0.003748*k)
  plotter.setSpec(1900, Br, 0.004905*k)
  plotter.setSpec(1800, Br, 0.006494*k)
  plotter.setSpec(1700, Br, 0.00866*k)
  plotter.setSpec(1600, Br, 0.01172*k)
  plotter.setSpec(1400, Br, 0.02216*k)
  plotter.setSpec(1000, Br, 0.1008*k)

  plotter.setOutputFormat(RF.Plotter.root)
  plotter.process(results)




