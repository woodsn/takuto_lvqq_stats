# Statistical interpreatstion for lvqq analysis
# Francesco De Lorenzi - Iowa State University
#


import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine(ROOT.gSystem.ExpandPathName('.x $ROOTCOREDIR/scripts/load_packages.C'))
from array import *

from ROOT import RF as RF
from ROOT import PullPlotter as PullPlotter


def createDirectoryStructure(input_dir, releaseDir, analysis, inputListTag, outputWSTag):
  import os
  import shutil
  import glob

  # create directories
  newdir = '{releaseDir}/{analysis}/data/{inputListTag}'.format(releaseDir=releaseDir, analysis=analysis, inputListTag=inputListTag, outputWSTag=outputWSTag)
  diagnosticsdir = '{releaseDir}/{analysis}/ws/diagnostics'.format(releaseDir=releaseDir, analysis=analysis, inputListTag=inputListTag, outputWSTag=outputWSTag)
  tmpdir = '{releaseDir}/{analysis}/tmp'.format(releaseDir=releaseDir, analysis=analysis, inputListTag=inputListTag, outputWSTag=outputWSTag)

  # first, we remove the previously existing directories
  dirs = [newdir, diagnosticsdir, tmpdir]
  for dir in dirs:
    if os.path.exists(dir):
      shutil.rmtree(dir)

  # we use makedirs, which takes care of creating intermediate directories
  os.makedirs(newdir)
  os.makedirs(diagnosticsdir)
  os.makedirs(tmpdir)
  
  # copy input trees from input location
  files = glob.glob(os.path.join(input_dir, '*.*'))
  for file in files:
    shutil.copy2(file, newdir)
  

if __name__ == '__main__':
 
  import sys
  configModel =  sys.argv[1] 
  

  ####################################################################################
  ###
  ### Configure the run here
  ###
  #################################################################################### 

  doConstrainBtagCR     = True
  doConstrainSidebandCR = True
  producePull           = False
  doProfiling           = True
  doSyst                = False 
  doInjection           = False
  doToys                = False
  doOptimisedScan       = True
  setUseStatError       = False


  ####################################################################################
  ###
  ### Choose here the sample and signal region
  ###
  ####################################################################################

  import SignalSamples
  if configModel == "RSGWW" : Model=SignalSamples.RSGWW  #HVTWW HVTWZ RSGWW
  if configModel == "HVTWW" : Model=SignalSamples.HVTWW   #HVTWW HVTWZ RSGWW
  if configModel == "HVTWZ" : Model=SignalSamples.HVTWZ  #HVTWW HVTWZ RSGWW

  
  
  # define which signal region/sample to use (WW or WZ)
  SignalRegion=Model.SR   #"WZ" #"WW"
  SignalSample=Model.Name      #"HVT"+SignalRegion


  releaseDir = './4ifb'
  analysis = 'VVlvqq_mc15_v1'
  inputListTag = 'itest01'
  outputWSTag = 'w'+SignalSample






 
  # print config 
  print "----------------------------------"
  print "----------------------------------"
  print "--- Running on sample     " , SignalSample
  print "--- with signal region    " , SignalRegion
  print "----------------------------------"
  print "--- doConstrainBtagCR     " , doConstrainBtagCR   
  print "--- doConstrainSidebandCR " , doConstrainSidebandCR
  print "--- producePull           " , producePull
  print "--- doProfiling           " , doProfiling
  print "--- doSyst                " , doSyst
  print "--- doInjection           " , doInjection
  print "--- doToys                " , doToys
  print "--- doOptimisedScan       " , doOptimisedScan
  print "--- setUseStatError       " , setUseStatError
  print "----------------------------------"
  print "--- releaseDir            " , releaseDir
  print "--- analysis              " , analysis
  print "--- inputListTag          " , inputListTag
  print "--- outputWSTag           " , outputWSTag
  print "----------------------------------"
  print "----------------------------------"

  
  #exit()


  # import files and create directory structure
  createDirectoryStructure('/afs/cern.ch/work/l/lorenzi/DbxStatTrunk/ResonanceFinder/data', releaseDir, analysis, inputListTag, outputWSTag)


  # chose do e only, mu only, combined or sum e+mu

  # e only
  #flav = ["EL"]
  # mu only
  #flav = ["MU"]
  # el and mu combined
  #flav = ["EL","MU"]
  # sum e+mu 
  flav = [""]

  # define running options
  masses = [1000, 1400, 1600, 1700, 1800, 1900, 2000, 2200, 2400, 2600]


  poi_setups = { # for optimised mu scan
                 2600 : [190, 0, 30],
                 2400 : [190, 0, 30],
                 2200 : [180, 0, 30],               
                 2000 : [160, 0, 10],
                 1900 : [185, 0, 10],
                 1800 : [185, 0, 10],
                 1700 : [185, 0, 10],
                 1600 : [185, 0, 5],
                 1400 : [20 , 0, 5],
                 1200 : [50, 0, 5],
                 1000 : [175, 0, 5]
               }

  Lumi = 1.

  freg = ["SR"+SignalRegion]

  if doConstrainSidebandCR :   freg.append("Sideband")
  if doConstrainBtagCR :       freg.append("Btag")

  regions = []
  # adding el or mu channels
  for r in freg:
    for f in flav:
      regions.append(r+f)

  print regions
  #exit()
      

  samples_setup = { # configure here the fit
                    # sample : [ minmu, maxmu, constrianType, setNormByLumi, setUseStatError ]
                "Top"     : [ 0   , 2  , RF.MultiplicativeFactor.GAUSSIAN, False , setUseStatError],
                "W+Jets"  : [ 0   , 2  , RF.MultiplicativeFactor.GAUSSIAN, False , setUseStatError],
                "Dibosons": [ 0.8 , 1.2, RF.MultiplicativeFactor.GAUSSIAN, False , False],
                "Z+Jets"  : [ 0.8 , 1.2, RF.MultiplicativeFactor.GAUSSIAN, False , False]
  }



  lvqq = RF.VVlvqqAnalysisRunner(analysis)
  lvqq.doPull(producePull)

  # set here systematics
  # these are systematics that apply to all samples and to all regions


  listSyst =[]
  listtopSyst  =[]
  if doSyst:
    import SystematicVariations
    listSyst = SystematicVariations.Variations
    # these are the 1 sided systematics
    one_sided_var = SystematicVariations.OneSided
    for one in one_sided_var:
       lvqq.addOneSideVariation(one)

    listtopSyst = SystematicVariations.TopSyst

  topSyst=set(listtopSyst)
  syst = set(listSyst)
  print syst
  print topSyst
  # define input tree format
  lvqq.setTreeObs('mass')
  lvqq.setTreeWeight('weight')

  # define histogram binning

  bins = [500,575,660,755,860,975,1100,1235,1380,1535,1700,1875,2060,2255,2460,2675,2900,3135,3380,3500] 
  lvqq.setNbins(20-1)

  binArray = array('d',bins)
  lvqq.setBins( binArray  ) 

  # apply directory structure
  lvqq.setReleaseDir(releaseDir)
  lvqq.setInputListTag(inputListTag)
  lvqq.setOutputWSTag(outputWSTag)


  scale=1.#5000.
  
  # loop over regions
  for region in regions:  
      # define the channels
      lvqq.addChannel(region, 'weight != 0')
      lvqq.channel(region).setStatErrorThreshold(0.05) # means that errors < 5% will be ignored

      # loop over samples
      # signal samples
      for mass in masses:
          sname = SignalSample+'{mass}'.format(mass=mass)
          lvqq.addSignal(sname, mass)
          lvqq.channel(region).addSample(sname)
          lvqq.channel(region).sample(sname).multiplyBy('mu', scale, 0, 200)
          lvqq.channel(region).sample(sname).setNormByLumi(False)
          lvqq.channel(region).sample(sname).multiplyBy('LumiNP', Lumi, Lumi*0.9, Lumi*1.1,RF.MultiplicativeFactor.GAUSSIAN)
          # add Syst
          for var in syst:
            # if not profiling to not add syst in CR
             if not doProfiling and region is not "SR": continue
             #print region, " ", sname, " " , var
             lvqq.channel(region).sample(sname).addVariation(var)
      # background samples
      for sample  in samples_setup:
        muname = "XS_"+sample
        muname = muname.replace("+","")
        lvqq.channel(region).addSample(sample)
        lvqq.channel(region).sample(sample).multiplyBy(muname, scale, samples_setup[sample][0],samples_setup[sample][1],samples_setup[sample][2])
        lvqq.channel(region).sample(sample).setNormByLumi(samples_setup[sample][3])
        lvqq.channel(region).sample(sample).setUseStatError(samples_setup[sample][4])
        lvqq.channel(region).sample(sample).multiplyBy('LumiNP', Lumi, Lumi*0.9, Lumi*1.1,RF.MultiplicativeFactor.GAUSSIAN)

        # add Syst
        for var in syst:
            # if not profiling to not add syst in CR
            if not doProfiling and region is not "SR": continue
            #print region, " ", sample, " " , var
            lvqq.channel(region).sample(sample).addVariation(var)
      # adding systematics relevant to Top only
      for ttvar in topSyst:
        print ttvar
        lvqq.channel(region).sample("Top").addVariation(ttvar) 
        
  lvqq.channel("SR"+SignalRegion).sample("W+Jets").addVariation("VJetsShapeModeling")
  lvqq.Print()
  #exit()
  # define POI
  lvqq.setPOI('mu')

  # configure mu scan
  if not doOptimisedScan:
    lvqq.limitRunner().setPOIScan(20, 0, 20) # nsteps, min, max
  else:
    for mass in masses:
      # deal with this signal in an optimised way
      lvqq.limitRunner().setPOIScan(mass, poi_setups[mass][0], poi_setups[mass][1], poi_setups[mass][2])

  # optional: inject signal
  injmu = 2
  if doInjection:
    lvqq.setInjectionSample('HVT2000')
    lvqq.setInjectionStrength(injmu)  #W' is mu=1.44 HVT mu =2
  # optional: use toys instead of asymptotics
  if doToys:
    lvqq.limitTool().setCalcType(RF.Frequentist)
    lvqq.limitTool().setTestStatType(RF.PL1sided)
    lvqq.limitTool().setNToys(100000)
    lvqq.limitTool().hypoTestInvTool().SetParameter('UseProof', True)
    lvqq.limitTool().hypoTestInvTool().SetParameter('GenerateBinned', True) # speeds up

  #lvqq.produceWS()
  #exit()
  # run
  lvqq.run()


  # plot
  ROOT.gROOT.ProcessLine('.L AtlasStyle.C+')
  ROOT.SetAtlasStyle()

  # output name
  name ='lvqq_Unconstrained'
  if( doConstrainBtagCR and doConstrainSidebandCR  ) : name = 'lvqq_Constrained'
  if( doInjection ) : name+="_Injectingmu"
  name+="_"+SignalSample

  plotter = RF.Plotter(name, '.')
  plotter.setVarName('m_{X}')
  plotter.setVarUnit('GeV')

  k=1000 # 1000
  Br=Model.Br

  # adding mass points to the plotter
  for m in masses:
    plotter.setSpec(m, Br, Model.XS[m]*k)
 
  plotter.setOutputFormat(RF.Plotter.root)
  plotter.process(lvqq.getStatResults())



  pullMasses =  [1400]

  if producePull :
    pullplotter = PullPlotter('vvjjpulltest', '.')
    for i in pullMasses : pullplotter.addOneMassPoint(i)
    pullplotter.setOutputFormat(RF.Plotter.pdf)
    pullplotter.process(lvqq.getStatResults())


