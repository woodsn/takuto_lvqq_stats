root -b -q labels.C\(\"results_v4_3p2fb/results_v4_3p2fb_v4_v4_WZ_HVT_obs_wNtrk_limit.C\"\)
root -b -q labels.C\(\"results_v4_3p2fb/results_v4_3p2fb_v4_v4_WW_HVT_obs_wNtrk_limit.C\"\)
root -b -q labels.C\(\"results_v4_3p2fb/results_v4_3p2fb_v4_v4_WW_Gravi_obs_wNtrk_limit.C\"\)
root -b -q labels.C\(\"results_v4_3p2fb/results_v4_3p2fb_v4_v4_ZZ_Gravi_obs_wNtrk_limit.C\"\)

sed -i '' 's/nan/0.5/g' results_v4_3p2fb/results_v4_3p2fb_v4_v4_WZ_HVT_obs_wNtrk_p0.C
sed -i '' 's/nan/0.5/g' results_v4_3p2fb/results_v4_3p2fb_v4_v4_WW_HVT_obs_wNtrk_p0.C
sed -i '' 's/nan/0.5/g' results_v4_3p2fb/results_v4_3p2fb_v4_v4_WW_Gravi_obs_wNtrk_p0.C
sed -i '' 's/nan/0.5/g' results_v4_3p2fb/results_v4_3p2fb_v4_v4_ZZ_Gravi_obs_wNtrk_p0.C

root -b -q labels.C\(\"results_v4_3p2fb/results_v4_3p2fb_v4_v4_WZ_HVT_obs_wNtrk_p0.C\"\)
root -b -q labels.C\(\"results_v4_3p2fb/results_v4_3p2fb_v4_v4_WW_HVT_obs_wNtrk_p0.C\"\)
root -b -q labels.C\(\"results_v4_3p2fb/results_v4_3p2fb_v4_v4_WW_Gravi_obs_wNtrk_p0.C\"\)
root -b -q labels.C\(\"results_v4_3p2fb/results_v4_3p2fb_v4_v4_ZZ_Gravi_obs_wNtrk_p0.C\"\)
