#!/usr/bin/env python

# ATLAS VH resonance combinations -- Run2

# Load the shared library. To compile it, follow the instructions given in the
# README.
import ROOT
import sys
ROOT.PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem

try:
    gSystem.Load("lib/libCombinationTool.so")
except:
    print ("Could not load library. Make sure that it was compiled correctly.")

if len(sys.argv)<3:
    print """ 
Usage:
  python %prog [outfile name] [ws1] [ws2] [ws3]
  ** only two workspaces are required for this processor
"""
    sys.exit()

oFileName = sys.argv[1]

fFile1= sys.argv[2]

fFile2 = sys.argv[3]

if len(sys.argv)>4:
    isThreeFile = True
    fFile3 = sys.argv[4]
else:
    isThreeFile = False


# Define
combined = ROOT.CombinedMeasurement("combined_master", "combined", "ModelConfig", "obsData")

#Add the first measurement to the combination
fileOne = ROOT.Measurement("fileOne")
fileOne.SetSnapshotName("NominalParamValues")
fileOne.SetFileName(fFile1)
fileOne.SetWorkspaceName("combined")
fileOne.SetModelConfigName("ModelConfig")
fileOne.SetDataName("obsData")
combined.AddMeasurement(fileOne)

#Add the second measurement to the combiantion
fileTwo = ROOT.Measurement("fileTwo")
fileTwo.SetSnapshotName("NominalParamValues")
fileTwo.SetFileName(fFile2)
fileTwo.SetWorkspaceName("combined")
fileTwo.SetModelConfigName("ModelConfig")
fileTwo.SetDataName("obsData")
combined.AddMeasurement(fileTwo)

if isThreeFile:
    #Add the second measurement to the combiantion
    fileThree = ROOT.Measurement("fileThree")
    fileThree.SetSnapshotName("NominalParamValues")
    fileThree.SetFileName(fFile3)
    fileThree.SetWorkspaceName("combined")
    fileThree.SetModelConfigName("ModelConfig")
    fileThree.SetDataName("obsData")
    combined.AddMeasurement(fileThree)

# Define a correlation scheme that should be used when combining the specified
# measurements. Parameters in the scheme that are not present in the
# measurements will be ignored.
correlation = ROOT.CorrelationScheme("CorrelationScheme")

# Define parameters of interest for the combined measurement. Only parameters
# present in the final workspace will be considered.
correlation.SetParametersOfInterest("mu")

# Signal strengths for different production modes. Use a placeholder for the
# ditau signal strength as it needs to be corrected for using 125.5 GeV.
# Signal strength parameters are unconstrained by default.
if isThreeFile:
    correlation.CorrelateParameter("fileOne::mu,fileTwo::mu,fileThree::mu", "mu")
else:
    correlation.CorrelateParameter("fileOne::mu,fileTwo::mu", "mu")

corrParams = ["alpha_Luminosity",
"alpha_XS_ttbar",
"alpha_XS_st",
"alpha_XS_diboson",
"alpha_XS_Wb",
"alpha_XS_Wc",
"alpha_XS_Wl",
"alpha_XS_Zb",
"alpha_XS_Zc",
"alpha_XS_Zl",
"alpha_XS_vh",
"alpha_XS_SMZh",
"alpha_PDF",
"alpha_IFSR",
"alpha_EG_RESOLUTION_ALL",
"alpha_EG_SCALE_ALL",
"alpha_EL_EFF_ID_TotalCorrUncertainty",
"alpha_EL_EFF_Iso_TotalCorrUncertainty",
"alpha_EL_EFF_Reco_TotalCorrUncertainty",
"alpha_EL_EFF_Trigger_TotalCorrUncertainty",
"alpha_FATJET_JER",
"alpha_FATJET_JMR",
"alpha_FT_EFF_Eigen_B_0",
"alpha_FT_EFF_Eigen_B_1",
"alpha_FT_EFF_Eigen_B_2",
"alpha_FT_EFF_Eigen_B_3",
"alpha_FT_EFF_Eigen_C_0",
"alpha_FT_EFF_Eigen_C_1",
"alpha_FT_EFF_Eigen_C_2",
"alpha_FT_EFF_Eigen_C_3",
"alpha_FT_EFF_Eigen_Light_0",
"alpha_FT_EFF_Eigen_Light_1",
"alpha_FT_EFF_Eigen_Light_2",
"alpha_FT_EFF_Eigen_Light_3",
"alpha_FT_EFF_Eigen_Light_4",
"alpha_FT_EFF_extrapolation",
"alpha_FT_EFF_extrapolation_from_charm",
"alpha_JET_GroupedNP_1",
"alpha_JET_GroupedNP_2",
"alpha_JET_GroupedNP_3",
"alpha_JET_Rtrk_Baseline",
"alpha_JET_Rtrk_Modelling",
"alpha_JET_Rtrk_Tracking",
"alpha_JET_JER_SINGLE_NP",
"alpha_MET_JetTrk_Scale",
"alpha_MET_SoftTrk_ResoPara",
"alpha_MET_SoftTrk_ResoPerp",
"alpha_MET_SoftTrk_Scale",
"alpha_MODEL_TTbar_aMcAtNlo",
"alpha_MODEL_TTbar_Herwig",
"alpha_MODEL_TTbar_rad",
"alpha_MODEL_Vjets_MadGraph",
"alpha_MUON_EFF_STAT",
"alpha_MUON_EFF_SYS",
"alpha_MUON_EFF_TrigStatUncertainty",
"alpha_MUON_EFF_TrigSystUncertainty",
"alpha_MUON_ISO_STAT",
"alpha_MUON_ISO_SYS",
"alpha_MUON_TTVA_STAT",
"alpha_MUON_TTVA_SYS",
"alpha_MUONS_ID",
"alpha_MUONS_MS",
"alpha_MUONS_SCALE",
"alpha_EXTRAP"]

for iPar in corrParams:
    corrString = "fileOne::"+iPar+",fileTwo::"+iPar
    if isThreeFile:
        corrString = corrString+",fileThree::"+iPar
    outString = iPar
    outString = outString[6:]
    correlation.CorrelateParameter(corrString,outString);
    
# Use the correlation scheme for the combined measurement.
combined.SetCorrelationScheme(correlation)

# Run the combination. First all measurements are regularised, i.e. the
# structure of the PDF will be unified, parameters and constraint terms renamed
# according to a common convention, etc. Then a new simultaneous PDF and
# dataset is build.
combined.CollectMeasurements()
combined.CombineMeasurements()



# Add common signal strength parameter for all signal samples
signalstrength = ROOT.ParametrisationScheme("signalstrength")
signalstrength.AddExpression("mu[1.0,-1000.0,1000.0]")


# Combine the different parametrisations
parametrisation = ROOT.ParametrisationSequence("parametrisation")
#parametrisation.AddScheme(corrections)
parametrisation.AddScheme(signalstrength)
parametrisation.SetParametersOfInterest("mu")

# Carry out the re-parametrisation
combined.SetParametrisationSequence(parametrisation)
combined.ParametriseMeasurements()

# Generate Asimov data (NP measured in unconditional fit, generated for mu = 0)
combined.MakeAsimovData(ROOT.kFALSE, ROOT.CombinedMeasurement.ucmles, ROOT.CombinedMeasurement.background)
#combined.MakeSnapshots(ROOT.CombinedMeasurement.nominal, ROOT.kTRUE)

# Save the combined workspace
combined.writeToFile(oFileName);

# Print useful information like the correlation scheme, re-namings, etc.
combined.Print()
