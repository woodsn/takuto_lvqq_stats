independent_variables:
- header: {name: Mass, units: GeV}
  values:
  - {value: 500}
  - {value: 600}
  - {value: 700}
  - {value: 750}
  - {value: 800}
  - {value: 850}
  - {value: 900}
  - {value: 950}
  - {value: 1000}
  - {value: 1050}
  - {value: 1100}
  - {value: 1150}
  - {value: 1200}
  - {value: 1300}
  - {value: 1400}
dependent_variables:
- header: {name: 'Cross section upper limit at 95% CL', units: pb}
  qualifiers:
  - {name: Limit, value: Observed}
  - {name: SQRT(S), units: GeV, value: 13000}
  - {name: LUMINOSITY, units: 'fb$^{-1}$', value: 36.1}
  values:
  - {value: 0.616196}
  - {value: 0.189399}
  - {value: 0.0582042}
  - {value: 0.0468235}
  - {value: 0.0367842}
  - {value: 0.027871}
  - {value: 0.0213574}
  - {value: 0.0170666}
  - {value: 0.012397}
  - {value: 0.0116565}
  - {value: 0.00999021}
  - {value: 0.0102684}
  - {value: 0.00915088}
  - {value: 0.00782636}
  - {value: 0.00776197}
- header: {name: 'Cross section upper limit at 95% CL', units: pb}
  qualifiers:
  - {name: Limit, value: Expected}
  - {name: SQRT(S), units: GeV, value: 13000}
  - {name: LUMINOSITY, units: 'fb$^{-1}$', value: 36.1}
  values:
  - value: 0.657035
    errors: 
    - {asymerror: {plus: 0.285779, minus: -0.183605}, label: '1 sigma'}
    - {asymerror: {plus: 0.670196, minus: -0.304387}, label: '2 sigma'}
  - value: 0.222604
    errors: 
    - {asymerror: {plus: 0.0896789, minus: -0.0622054}, label: '1 sigma'}
    - {asymerror: {plus: 0.203992, minus: -0.103127}, label: '2 sigma'}
  - value: 0.0856692
    errors: 
    - {asymerror: {plus: 0.0356153, minus: -0.0239398}, label: '1 sigma'}
    - {asymerror: {plus: 0.0825132, minus: -0.0396884}, label: '2 sigma'}
  - value: 0.0719726
    errors: 
    - {asymerror: {plus: 0.0287515, minus: -0.0201124}, label: '1 sigma'}
    - {asymerror: {plus: 0.0660089, minus: -0.0333431}, label: '2 sigma'}
  - value: 0.0512574
    errors: 
    - {asymerror: {plus: 0.0206786, minus: -0.0143236}, label: '1 sigma'}
    - {asymerror: {plus: 0.0481115, minus: -0.0237462}, label: '2 sigma'}
  - value: 0.0392104
    errors: 
    - {asymerror: {plus: 0.0160435, minus: -0.0109571}, label: '1 sigma'}
    - {asymerror: {plus: 0.0374064, minus: -0.0181652}, label: '2 sigma'}
  - value: 0.0312502
    errors: 
    - {asymerror: {plus: 0.012617, minus: -0.0087327}, label: '1 sigma'}
    - {asymerror: {plus: 0.0294765, minus: -0.0144774}, label: '2 sigma'}
  - value: 0.0256118
    errors: 
    - {asymerror: {plus: 0.0105272, minus: -0.00715709}, label: '1 sigma'}
    - {asymerror: {plus: 0.0247837, minus: -0.0118653}, label: '2 sigma'}
  - value: 0.0188462
    errors: 
    - {asymerror: {plus: 0.0078807, minus: -0.00526646}, label: '1 sigma'}
    - {asymerror: {plus: 0.0186121, minus: -0.00873094}, label: '2 sigma'}
  - value: 0.0179437
    errors: 
    - {asymerror: {plus: 0.00749664, minus: -0.00501427}, label: '1 sigma'}
    - {asymerror: {plus: 0.0177868, minus: -0.00831285}, label: '2 sigma'}
  - value: 0.0157148
    errors: 
    - {asymerror: {plus: 0.00673646, minus: -0.00439143}, label: '1 sigma'}
    - {asymerror: {plus: 0.0161776, minus: -0.00728028}, label: '2 sigma'}
  - value: 0.0161522
    errors: 
    - {asymerror: {plus: 0.00683174, minus: -0.00451363}, label: '1 sigma'}
    - {asymerror: {plus: 0.0163195, minus: -0.00748288}, label: '2 sigma'}
  - value: 0.0143988
    errors: 
    - {asymerror: {plus: 0.00613477, minus: -0.00402366}, label: '1 sigma'}
    - {asymerror: {plus: 0.0147481, minus: -0.00667059}, label: '2 sigma'}
  - value: 0.012056
    errors: 
    - {asymerror: {plus: 0.00530228, minus: -0.00336899}, label: '1 sigma'}
    - {asymerror: {plus: 0.0129878, minus: -0.00558524}, label: '2 sigma'}
  - value: 0.0118782
    errors: 
    - {asymerror: {plus: 0.00531916, minus: -0.00331931}, label: '1 sigma'}
    - {asymerror: {plus: 0.0131596, minus: -0.00550288}, label: '2 sigma'}
