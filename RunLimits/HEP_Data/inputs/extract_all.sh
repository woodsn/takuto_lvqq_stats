#!/bin/bash

# root -l -q Limit1D_to_YAML.C'("1D_BBS_Singlet_HEP.root")' > BB_Singlet_1D.yaml
# root -l -q Limit1D_to_YAML.C'("1D_BBS_HEP.root")' > BB_WtWt_1D.yaml
# root -l -q Limit1D_to_YAML.C'("1D_TTS_Singlet_HEP.root")' > TT_Singlet_1D.yaml
# root -l -q Limit1D_to_YAML.C'("1D_TTS_HEP.root")' > TT_WbWb_1D.yaml

# root -l -q Limit2D_to_YAML.C'("2D_TTS_BRscan_HEP.root")' > TT_2D.yaml
root -l -q Limit2D_to_YAML.C'("2D_BBS_BRscan_HEP.root")' > BB_2D.yaml
