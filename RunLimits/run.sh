#!/bin/bash

# Set the Signal type
# HVTWW, HVTWZ, RSGWW, ggHWWNWA, VBFWWNWA, All
sources=("ggHWWNWA" "HVTWW" "HVTWZ" "RSGWW" "RSGWW-SmReW" "VBF-HVTWW" "VBF-HVTWZ" "VBFWWNWA")

# Set the purity level
# 0: LP  1: HP  2: LPHP
purity=2
dir="../limits/"

# Set the production type
# 0: ggF  1: VBF  2: comb
prod=0

# Set options for running
doLims="true"
dop0="false"
doSens="false"
doBlind="false"


# Concatenate input line
for sr in "${sources[@]}"
do
    # VBF
    if echo $sr | grep 'VBF'; then
	prod=1
    else
	prod=0
    fi

    input_line=\"$sr\",\"$dir\",$purity,$prod,$doLims,$dop0,$doSens,$doBlind
# Run the macro
root -l -b << EOF
.x LimitPlotCombBR.cxx+($input_line)
.q
EOF
done