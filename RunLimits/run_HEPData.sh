#!/bin/bash -f

root -l -q 'HEP_Data/inputs/Limit1D_to_YAML.C("HEP_out/ggHWWNWA.root")' > HEP_out/ggHWWNWA.yaml
root -l -q 'HEP_Data/inputs/Limit1D_to_YAML.C("HEP_out/HVTWW.root")' > HEP_out/HVTWW.yaml
root -l -q 'HEP_Data/inputs/Limit1D_to_YAML.C("HEP_out/HVTWZ.root")' > HEP_out/HVTWZ.yaml
root -l -q 'HEP_Data/inputs/Limit1D_to_YAML.C("HEP_out/RSGWW.root")' > HEP_out/RSGWW.yaml
root -l -q 'HEP_Data/inputs/Limit1D_to_YAML.C("HEP_out/RSGWW-SmReW.root")' > HEP_out/RSGWW-SmReW.yaml
root -l -q 'HEP_Data/inputs/Limit1D_to_YAML.C("HEP_out/VBF-HVTWW.root")' > HEP_out/VBF-HVTWW.yaml
root -l -q 'HEP_Data/inputs/Limit1D_to_YAML.C("HEP_out/VBF-HVTWZ.root")' > HEP_out/VBF-HVTWZ.yaml
root -l -q 'HEP_Data/inputs/Limit1D_to_YAML.C("HEP_out/VBFWWNWA.root")' > HEP_out/VBFWWNWA.yaml
