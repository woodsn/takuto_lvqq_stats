import condor, time, os, commands,sys


signals = ("HVTWW", "blah") 
#signals = ("ggHWWNWA","HVTWW", "HVTWZ", "RSGWW", "VBFWWNWA", "VBF-HVTWW","VBF-HVTWZ", "RSGWW-SmReW")
masses_higgs =("1600", "1800","2000","2200", "2400", "2600", "2800", "3000")
masses_nothiggs =("1600", "1800","2000","2200", "2400", "2600", "2800", "3000", "3500", "4000", "4500", "5000")
#masses_nothiggs =("1600", "1700")
outputdir='ws_injec_syst_jan29'
directory_boosted = '/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/Run/test_run/VVlvqq_mc15_v2016/ws/'
for s in signals:
	if s == "ggHWWNWA":
		selection = "RES"
		binning = "ggF"
		for m in masses_higgs:
			file1 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws_*'+s+'_HP_'+m+'.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws_*'+s+'_LP_'+m+'.root')
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
#			print 'file3 is: ' + file3
			runstring='python lvqq_combine_fast.py %s %s'%(file1,file2)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/'+outputdir+'/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)
	if s == "VBFWWNWA":
		selection = "RESVBF"
		binning = "VBF"
		for m in masses_higgs:
			file1 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_100ws_*'+s+m+'_HP.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_100ws_*'+s+m+'_LP.root')
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			runstring='python lvqq_combine_fast.py %s %s'%(file1,file2)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/'+outputdir+'/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)
	if s == "HVTWW" or s == "HVTWZ":
		selection = "RES"
		binning = "ggF"
		for m in masses_nothiggs:
			file1 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws_*'+s+m+'_HP.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws_*'+s+m+'_LP.root')
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			runstring='python lvqq_combine_fast.py %s %s'%(file1,file2)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/'+outputdir+'/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)
	if s == "RSGWW" or s == "RSGWW-SmReW":
		selection = "RES"
		binning = "ggF"
		for m in masses_nothiggs:
			file1 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_HP.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_LP.root')
			file3 = commands.getoutput('ls ' + directory_resolved + '*'+s+m+'*'+selection + '_*'+m+'.root')
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			print 'file3 is: ' + file3
			runstring='python lvqq_combine_fast.py %s %s %s'%(file1,file2,file3)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/'+outputdir+'/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)

	if s == "VBF-HVTWW":
		selection = "RESVBF"
		binning = "VBF"
		string = "VBF-HVTWW"
		for m in masses_nothiggs:
			file1 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_HP.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_LP.root')
			file3 = commands.getoutput('ls ' + directory_resolved + '*'+s+m+'*'+selection + '_*'+m+'.root')
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			print 'file3 is: ' + file3
			runstring='python lvqq_combine_fast.py %s %s %s'%(file1,file2,file3)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/'+outputdir+'/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)
	if s == "VBF-HVTWZ":
		selection = "RESVBF"
		binning = "VBF"
		string2 = "VBF-HVTWZ"
		for m in masses_nothiggs:
			file1 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_HP.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_LP.root')
			file3 = commands.getoutput('ls ' + directory_resolved + '*'+s+m+'*'+selection + '_*'+m+'.root')
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			print 'file3 is: ' + file3
			runstring='python lvqq_combine_fast.py %s %s %s'%(file1,file2,file3)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/'+outputdir+'/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)




