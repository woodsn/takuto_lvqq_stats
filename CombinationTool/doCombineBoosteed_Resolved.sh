#!/bin/bash

eos_dir="../"

masses_hvt=(500 600 700 800 900 1000 1100 1200 1300 1400 1500 1600 1700 1800 1900 2000 2200 2400 2600 2800 3000 3500 4000 4500 5000 )
masses_vbf=(500 600 700 800 900 1000 1100 1200 1300 1400 1500 1600 1700 1800 1900 2000 2200 2400 2600 2800 3000 3500 4000 4500 5000 )
srs=( "HVTWW" )
# Which SR selection?
# ggF, VBF, comb
region="ggF"

#Loop over the masses
for sr in "${srs[@]}"
do
	# Set Mass points
	if [[ $sr == "VBFWWNWA" ]] || [[ $sr == "ggHWWNWA" ]]; then
		masses=("${masses_vbf[@]}")
	else
		masses=("${masses_hvt[@]}")
	fi

	#$(O)"(BLoop over masses
	for m1 in "${masses[@]}"
	do
    python lvqq_combine_fast_boosted_resolved.py $sr $m1 $region
    fout=$eos_dir"/ws_boosted_resolved_comb/"$sr"/ws_HPLP_"$m1".root" 
    echo "Saving workspace to: "$fout
		mv output.root $fout
	done
done
