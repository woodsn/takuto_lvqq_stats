source ../rcSetup.sh 
source /afs/cern.ch/sw/lcg/external/gcc/4.7.2/x86_64-slc6-gcc47-opt/setup.sh

prev_path=`pwd`

cd /afs/cern.ch/atlas/project/HSG7/root/root_v5-34-19/x86_64-slc6-gcc47 
#cd /afs/cern.ch/atlas/project/HSG7/root/root_v5-34-23/i686-slc6-gcc47/

source bin/thisroot.sh
cd $prev_path;

echo " "
echo "#===================================================#"
echo "Machine: " `uname -a`
echo "Root Path: " $ROOTSYS
echo "gcc: " `gcc -dumpversion`
echo "Path: " `pwd`
echo "#===================================================#"
echo " "

cd `dirname $0`/..
export VVCOMBSSYS=`pwd`
export PATH=$VVCOMBSYS/bin:$PATH
cd $prev_path;
