import condor, time, os, commands,sys


#signals = ("ggHWWNWA", "HVTWW", "HVTWZ", "RSGWW", "RSGWW-SmReW")
#signals = ("VBF-HVTWW", "VBF-HVTWZ")
signals = ("HVTWW", "blah") 
#signals = ("ggHWWNWA","HVTWW", "HVTWZ", "RSGWW", "VBFWWNWA", "VBF-HVTWW","VBF-HVTWZ", "RSGWW-SmReW")
#masses_higgs = ("900","800")
masses_higgs =("100","200","300","400","500", "600","700","800", "900", "1000","1100", "1200", "1300","1400")
#masses_nothiggs=("1000","11000")
#masses_higgs =("500", "600","700","800", "900", "1000", "1200", "1400")
masses_nothiggs =("300","400","500", "600","700","800", "900", "1000","1100", "1200", "1300","1400")
#mass_nothiggs =("1000","198")
outputdir='ws_injec_syst_jan29'
#directory_resolved='/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ws_v28_final/VBF2/'
#directory_boosted='/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/takuto_boosted_june5/'
#directory_boosted='/export/share/gauss/woodsn/ExtraDimensions/new_takuya_statFramework/ws_final2/VBF/'
directory_resolved = '/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/Run/test_run/VVlvqq_mc15_v2016/ws/'
directory_boosted = '/export/share/gauss/woodsn/ExtraDimensions/lvqq_stats/new_takuya_statFramework/Run/test_run/VVlvqq_mc15_v2016/ws/'
for s in signals:
	if s == "ggHWWNWA":
		selection = "RES"
		binning = "ggF"
		for m in masses_higgs:
			file1 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws_*'+s+'_HP_'+m+'.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws_*'+s+'_LP_'+m+'.root')
			file3 = commands.getoutput('ls ' + directory_resolved + '*'+s+m+'*'+selection + '_*'+m+'.root')
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			print 'file3 is: ' + file3
			runstring='python lvqq_combine_fast.py %s %s %s'%(file1,file2,file3)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/'+outputdir+'/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)
	if s == "VBFWWNWA":
		selection = "RESVBF"
		binning = "VBF"
		for m in masses_higgs:
			file1 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_100ws_*'+s+m+'_HP.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_100ws_*'+s+m+'_LP.root')
			file3 = commands.getoutput('ls ' + directory_resolved + '*'+s+m+'_100ws*'+selection + '_'+m+'.root') #VVlvqq_mc15_v2016_VBFWWNWA1000_ws_injec_VBFWWNWA_RESVBF_1000.root
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			print 'file3 is: ' + file3
			runstring='python lvqq_combine_fast.py %s %s %s'%(file1,file2,file3)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/'+outputdir+'/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)
	if s == "HVTWW" or s == "HVTWZ":
		selection = "RES"
		binning = "ggF"
		for m in masses_nothiggs:
			file1 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws_*'+s+m+'_HP.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws_*'+s+m+'_LP.root')
			file3 = commands.getoutput('ls ' + directory_resolved + '*'+s+m+'_ws*'+selection + '_*'+m+'.root')
#/VVlvqq_mc15_v2016_HVTWW700_ws_injec_HVTWW_RES_700.root
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			print 'file3 is: ' + file3
			runstring='python 3lvqq_combine_fast.py %s %s %s'%(file1,file2,file3)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/'+outputdir+'/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)
	if s == "RSGWW" or s == "RSGWW-SmReW":
		selection = "RES"
		binning = "ggF"
		for m in masses_nothiggs:
			file1 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_HP.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_LP.root')
			file3 = commands.getoutput('ls ' + directory_resolved + '*'+s+m+'*'+selection + '_*'+m+'.root')
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			print 'file3 is: ' + file3
			runstring='python lvqq_combine_fast.py %s %s %s'%(file1,file2,file3)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/'+outputdir+'/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)

	if s == "VBF-HVTWW":
		selection = "RESVBF"
		binning = "VBF"
		string = "VBF-HVTWW"
		for m in masses_nothiggs:
			file1 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_HP.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_LP.root')
			file3 = commands.getoutput('ls ' + directory_resolved + '*'+s+m+'*'+selection + '_*'+m+'.root')
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			print 'file3 is: ' + file3
			runstring='python lvqq_combine_fast.py %s %s %s'%(file1,file2,file3)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/'+outputdir+'/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)
	if s == "VBF-HVTWZ":
		selection = "RESVBF"
		binning = "VBF"
		string2 = "VBF-HVTWZ"
		for m in masses_nothiggs:
			file1 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_HP.root')
			file2 = commands.getoutput('ls ' + directory_boosted +'*'+s+m+'_ws*_'+s+m+'_LP.root')
			file3 = commands.getoutput('ls ' + directory_resolved + '*'+s+m+'*'+selection + '_*'+m+'.root')
			print 'file1 is: ' + file1
			print 'file2 is: ' + file2
			print 'file3 is: ' + file3
			runstring='python lvqq_combine_fast.py %s %s %s'%(file1,file2,file3)
			os.system(runstring)
			fout = 'ws_HPLP_'+m+'.root'
			rename_and_move='mv output.root ../ws_combined/'+outputdir+'/%s/%s'%(s,fout)
			print 'rename and move ' + rename_and_move
			os.system(rename_and_move)




