#!/bin/bash

sig="HVTWW"
mass="1000"
prod="ggF"

eos="../Run/test_run/WVlvqq_mc15_v28/ws"
fname="WVlvqq_mc15_v28_${sig}${mass}_ws_${sig}${mass}_LP.root"
#fname="ws_HPLP_${mass}.root"
inname="${sig}_${mass}_${prod}"
path="${eos}/${fname}"

root -l -b $path << EOF
.L GetCorrMatrix.C
GetCorrMatrix("${inname}")
.q
EOF

echo "Finished saving correlation matrix"

