#!/bin/bash

sig="HVT"
state="WW"
mass="2000"
prod="ggF"
isVBF="false"
sig_name="HVT WW 2.0 TeV"

saveName="postFit_${sig}${state}_${mass}"
sig_model="${sig}${state}${mass}_${prod}"
regions=("SR${state}_fileOne" "Sideband_fileOne" "Btag_fileOne" "SR${state}_LP_fileTwo" "Sideband_LP_fileTwo" "Btag_LP_fileTwo")
#regions=("SR${state}_fileOne")

doBlind="false"
fname="../NuisanceCheckCombination/mu_fixed/results_${sig_model}/FitCrossChecks.root"

for region in "${regions[@]}"
do
root -l -b << EOF
.x drawPostFit.C("${fname}","${sig_name}",${isVBF},${doBlind},"${region}","${saveName}")
.q
EOF
done;
	
