#!/bin/bash

#$(O)"(BSet the signal type: HVTWW, HVTWZ, RSGWW, ggHWWNWA, VBFWWNWA
sources=( "HVTWW" )
#sources=( "VBF-HVTWW" )
masses=("1000")

#eos=".."
#ws_dir="CombinationTool"
#ws_name="output.root"

# Concatenate input into string
for analysis in "${sources[@]}"
do
    for mass in "${masses[@]}"
    do
	eos="../"
	ws_dir="ws_combined/"$analysis"/"
	ws_name="ws_HPLP_"$mass".root"
	
	output_dir="results_"$analysis$mass"/"
	ws_type="combined"
	mc="ModelConfig"
#FIXME was obsData
	obs="obsData"
	
	echo "Running on file: "$eos"/"$ws_dir"/"$ws_name
	echo "     output dir: "$output_dir
	echo "        ws type: "$ws_type
	echo "   model config: "$mc
	echo "       obs data: "$obs
	input_line=\"$eos/$ws_dir/$ws_name\",\"$output_dir\",\"$ws_type\",\"$mc\",\"$obs\"
#$(O)"(BRun the macro
	root -l -b <<EOF
.L FitCrossCheckForLimits.C+
LimitCrossCheck::PlotFitCrossChecks( $input_line )  
.q
EOF
	
	echo "Finished running combined fit cross checks"
    done
done
