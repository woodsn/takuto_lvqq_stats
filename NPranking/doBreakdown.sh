#!/bin/bash

#$(O)"(BAdjust these
analyses=("HVTWW")
masses=("1000")

for analysis in "${analyses[@]}"
do
    for mass in "${masses[@]}"
    do
# Input paths
	eos_dir="/afs/cern.ch/work/t/tkunigo/public/ws_june25/"${analysis}"/"
	ws="ws_HPLP_"${mass}".root"
	output_dir=${analysis}"_"${mass}
	
# Required input parameters for runBreakdown.C
	inFileName=${eos_dir}/${ws}
	wsName="combined"
	modelConfigName="ModelConfig"
	dataName="obsData"
	poiName="mu"
	xmlName="config/breakdown.xml"
	technique="addition"
	catecory2eval="total"
	precision="0.005"
	corrCutoff="0.0"
	folder=${output_dir}
	loglevel="DEBUG"
	
# Print input config
	echo "Running on file: "${inFileName}
	echo "         wsName: "${wsName}
	echo "    Config name: "${modelConfigName}
	echo "      data name: "${dataName}
	echo "       poi name: "${poiName}
	echo "       xml name: "${xmlName}
	echo "      technique: "${technique}
	echo "    cat to eval: "${catecory2eval}
	echo "      precision: "${precision}
	echo "  output folder: "${folder}
	echo "      log level: "${loglevel}
	
# Concatenate input string
	input_line=\"$inFileName\",\"$wsName\",\"$modelConfigName\",\"$dataName\",\"$poiName\",\"$xmlName\",\"$technique\",\"$catecory2eval\",$precision,$corrCutoff,\"$folder\",\"$loglevel\"
#$(O)"(BRun the macro
	root -l -b << EOF
.L macros/runBreakdown.C+
runBreakdown($input_line)
.q
EOF
	echo "Finished running Breakdown"
	done
done
	
# # Concatenate input string
# input_line=\"$mass\",\"$output_dir\",1
# # Run the pull plot
# root -l -b << EOF2
# .x macros/drawPlot_pulls.C($input_line)
# EOF2
# echo "Finished running Plot Pulls"


