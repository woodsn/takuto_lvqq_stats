import glob 
import re
import os
import math

StatErrors   = glob.glob('StatError*')

for StatError in StatErrors:
    sample_name = re.split('[._]', StatError)[1]
    print "Sample: " + sample_name

    MCStatError = "./MCStatError_" + sample_name + ".log"
    if not os.path.exists( MCStatError ):
        print "MCStatError log not exists for " + sample_name
    B1=0
    B2=0
    D1=0
    D2=0
    StatFile = open(StatError, 'r')
    iEntry=0
    for line in StatFile:
        if "mu\t" in line:
            temp_str=line.split("\t")[2]
            value=float( temp_str.split("+/-")[1] )
            if(iEntry==0):
                B1=value
            elif(iEntry==1):
                D1=value
            else:
                print "ERROR!!!"
                break
            iEntry = iEntry + 1
    StatFile.close()
    MCStatFile = open(MCStatError, 'r')
    iEntry=0
    for line in MCStatFile:
        if "mu\t" in line:
            temp_str=line.split("\t")[2]
            value=float( temp_str.split("+/-")[1] )
            if(iEntry==0):
                B2=value
            elif(iEntry==1):
                D2=value
            else:
                print "ERROR!!!"
                break
            iEntry = iEntry + 1
    MCStatFile.close()

    if(B1!=B2):
        print "ERROR1"
    if(B1<D2):
        print "Error2"

    print "Syst.      = " + str( math.sqrt( B1**2-D1**2 )/B1 )
    print "Data stat. = " + str( D1/B1 )
    print "MC stat.   = " + str( math.sqrt(D2**2-D1**2)/B1 )
    print ""

