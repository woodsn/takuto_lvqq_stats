#!/bin/bash

#$(O)"(BWork space
# combined HP/LP
wsdir="/afs/cern.ch/work/t/tkunigo/public/ws_june25"

#$(O)"(BSignal: HVTWW, HVTWZ, RSGWW, ggHWWNWA, VBFWWNWA
analyses=("HVTWW")
masses=("500")

for analysis in "${analyses[@]}"
do
    for mass in "${masses[@]}"
    do
	
#$(O)"(BWork space name
	ws="ws_HPLP_"${mass}".root"
	
	
	input_file=${wsdir}/${analysis}/${ws}
	folder=${analysis}"_"${mass}
	wsName="combined"
	dataName="obsData"
	poiName="mu"
	
	
	
	echo "Running with--------------------"
	echo "             file: "${input_file}
	echo "           folder: "${folder}
	echo "           wsName: "${wsName}
	echo "         dataName: "${dataName}
	echo "          poiName: "${poiName}
	
	python runBatchPulls.py ${input_file} --folder ${folder} --workspaceName ${wsName} --dataName ${dataName} --poiName ${poiName}
	done
done
