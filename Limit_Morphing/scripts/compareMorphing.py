import sys
import numpy as np
from Hist1D import Hist1D
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from AtlasStyle import SetStyleAtlas, DrawAtlasLabel, SNFormatter
from matplotlib.ticker import AutoMinorLocator, MaxNLocator, FuncFormatter

def weighted_avg_and_std (values, weights):
    """
    Return the weighted average and standard deviation.
    values, weights -- Numpy ndarrays with the same shape.
    """
    average = np.average(values, weights=weights)
    # Fast and numerically precise
    variance = np.average((values-average)**2, weights=weights)
    return (average, np.sqrt(variance))

# Get the min and max values where the distribution drops below percent of peak
def GetXAxisLimits (hist, nStd0=6, nStd1=None):
    mean, std = weighted_avg_and_std (hist.centers, hist.values)
    #print mean, std
    if not nStd1: nStd1 = nStd0
    return (mean - std*nStd0, mean + std*nStd1)

if __name__=='__main__':

    mA = int(sys.argv[1])
    mH = int(sys.argv[2])
    sigfolder = 'output_morph'
    sigtitle = 'AZHllbbmA{0}mH{1}'.format (mA, mH)

    import ROOT
    f1 = ROOT.TFile.Open ('{0}/morph2D_mA{1}_mH{2}.root'.format(sigfolder, mA, mH))
    h_incl_orig = f1.Get (sigtitle + '_2tag2pjet_0ptv_SR_llbbMass_mHBinIncl_orig')
    h_bin1_orig = f1.Get (sigtitle + '_2tag2pjet_0ptv_SR_llbbMass_mHBin1_orig')
    h_bin2_orig = f1.Get (sigtitle + '_2tag2pjet_0ptv_SR_llbbMass_mHBin2_orig')
    h_bin3_orig = f1.Get (sigtitle + '_2tag2pjet_0ptv_SR_llbbMass_mHBin3_orig')
    h_bin4_orig = f1.Get (sigtitle + '_2tag2pjet_0ptv_SR_llbbMass_mHBin4_orig')
    h_bin5_orig = f1.Get (sigtitle + '_2tag2pjet_0ptv_SR_llbbMass_mHBin5_orig')

    h_incl = f1.Get (sigtitle + '_2tag2pjet_0ptv_SR_llbbMass_mHBinIncl')
    h_bin1 = f1.Get (sigtitle + '_2tag2pjet_0ptv_SR_llbbMass_mHBin1')
    h_bin2 = f1.Get (sigtitle + '_2tag2pjet_0ptv_SR_llbbMass_mHBin2')
    h_bin3 = f1.Get (sigtitle + '_2tag2pjet_0ptv_SR_llbbMass_mHBin3')
    h_bin4 = f1.Get (sigtitle + '_2tag2pjet_0ptv_SR_llbbMass_mHBin4')
    h_bin5 = f1.Get (sigtitle + '_2tag2pjet_0ptv_SR_llbbMass_mHBin5')

    hist_incl_orig = Hist1D (h_incl_orig); hist_incl_orig.buildHist1D (rebin=20)
    hist_bin1_orig = Hist1D (h_bin1_orig); hist_bin1_orig.buildHist1D (rebin=20)
    hist_bin2_orig = Hist1D (h_bin2_orig); hist_bin2_orig.buildHist1D (rebin=20)
    hist_bin3_orig = Hist1D (h_bin3_orig); hist_bin3_orig.buildHist1D (rebin=20)
    hist_bin4_orig = Hist1D (h_bin4_orig); hist_bin4_orig.buildHist1D (rebin=20)
    hist_bin5_orig = Hist1D (h_bin5_orig); hist_bin5_orig.buildHist1D (rebin=20)

    hist_incl = Hist1D (h_incl); hist_incl.buildHist1D (rebin=20)
    hist_bin1 = Hist1D (h_bin1); hist_bin1.buildHist1D (rebin=20)
    hist_bin2 = Hist1D (h_bin2); hist_bin2.buildHist1D (rebin=20)
    hist_bin3 = Hist1D (h_bin3); hist_bin3.buildHist1D (rebin=20)
    hist_bin4 = Hist1D (h_bin4); hist_bin4.buildHist1D (rebin=20)
    hist_bin5 = Hist1D (h_bin5); hist_bin5.buildHist1D (rebin=20)

    # =========================
    # Matplotlib awesome plots
    # =========================

    fig, axarr = plt.subplots(2, 3, sharex=False, figsize=(16,9))

    for axes_row in axarr:
        for axes in axes_row:
            axes.set_facecolor ('0.98')

    # Inclusive m(H) bin
    axarr[0, 0].set_title('Inclusive for mA{0} mH{1}'.format(mA, mH))
    axarr[0, 0].errorbar (hist_incl_orig.centers, hist_incl_orig.values_nozeros, xerr=hist_incl_orig.widths/2., yerr=hist_incl_orig.errorsTotal,
            label='Original', fmt='o', mfc='k', mec='k', ecolor='k', capsize=0., ms=5., lw=1.2)
    axarr[0, 0].hist (hist_incl.centers, bins=hist_incl.edges, weights=hist_incl.values,
            label='Morphed', histtype='step', align='mid', fc='none', ec='m', lw=1.5)
    xlimits = GetXAxisLimits (hist_incl)
    axarr[0, 0].set_xlim (xlimits[0], xlimits[1])
    axarr[0, 0].set_ylim (0., 1.2*np.nanmax([hist_incl.values, hist_incl_orig.values]))
    axarr[0, 0].set_ylabel ('Entries / {0:.0f} GeV'.format (hist_incl.widths[0]), y=1.0, ha='right')
    handles, labels = axarr[0, 0].get_legend_handles_labels()
    axarr[0, 0].legend (handles, labels, frameon=False, numpoints=1, loc="upper right", ncol=1, fontsize=12.)

    # m(H) bin 1
    #axarr[0, 1].set_title('$m_A-m_H\in[100, 200]$ GeV')
    #axarr[0, 1].set_title('$m_H\in[90, 150]$ GeV')
    axarr[0, 1].set_title('$m_H\in[80, 160]$ GeV')
    axarr[0, 1].errorbar (hist_bin1_orig.centers, hist_bin1_orig.values_nozeros, xerr=hist_bin1_orig.widths/2., yerr=hist_bin1_orig.errorsTotal,
            label='Original', fmt='o', mfc='k', mec='k', ecolor='k', capsize=0., ms=5., lw=1.2)
    axarr[0, 1].hist (hist_bin1.centers, bins=hist_bin1.edges, weights=hist_bin1.values,
            label='Morphed', histtype='step', align='mid', fc='none', ec='m', lw=1.5)
    xlimits = GetXAxisLimits (hist_bin1)
    axarr[0, 1].set_xlim (xlimits[0], xlimits[1])
    axarr[0, 1].set_ylim (0., 1.2*np.nanmax([hist_bin1.values, hist_bin1_orig.values]))
    handles, labels = axarr[0, 1].get_legend_handles_labels()
    axarr[0, 1].legend (handles, labels, frameon=False, numpoints=1, loc="upper right", ncol=1, fontsize=12.)

    # m(H) bin 2
    #axarr[0, 2].set_title('$m_A-m_H\in[200, 300]$ GeV')
    #axarr[0, 2].set_title('$m_H\in[150, 220]$ GeV')
    axarr[0, 2].set_title('$m_H\in[160, 260]$ GeV')
    axarr[0, 2].errorbar (hist_bin2_orig.centers, hist_bin2_orig.values_nozeros, xerr=hist_bin2_orig.widths/2., yerr=hist_bin2_orig.errorsTotal,
            label='Original', fmt='o', mfc='k', mec='k', ecolor='k', capsize=0., ms=5., lw=1.2)
    axarr[0, 2].hist (hist_bin2.centers, bins=hist_bin2.edges, weights=hist_bin2.values,
            label='Morphed', histtype='step', align='mid', fc='none', ec='m', lw=1.5)
    xlimits = GetXAxisLimits (hist_bin2)
    axarr[0, 2].set_xlim (xlimits[0], xlimits[1])
    axarr[0, 2].set_ylim (0., 1.2*np.nanmax([hist_bin2.values, hist_bin2_orig.values]))
    handles, labels = axarr[0, 2].get_legend_handles_labels()
    axarr[0, 2].legend (handles, labels, frameon=False, numpoints=1, loc="upper right", ncol=1, fontsize=12.)

    # m(H) bin 3
    #axarr[1, 0].set_title('$m_A-m_H\in[300, 400]$ GeV')
    #axarr[1, 0].set_title('$m_H\in[220, 300]$ GeV')
    axarr[1, 0].set_title('$m_H\in[260, 380]$ GeV')
    axarr[1, 0].errorbar (hist_bin3_orig.centers, hist_bin3_orig.values_nozeros, xerr=hist_bin3_orig.widths/2., yerr=hist_bin3_orig.errorsTotal,
            label='Original', fmt='o', mfc='k', mec='k', ecolor='k', capsize=0., ms=5., lw=1.2)
    axarr[1, 0].hist (hist_bin3.centers, bins=hist_bin3.edges, weights=hist_bin3.values,
            label='Morphed', histtype='step', align='mid', fc='none', ec='m', lw=1.5)
    xlimits = GetXAxisLimits (hist_bin3)
    axarr[1, 0].set_xlim (xlimits[0], xlimits[1])
    axarr[1, 0].set_xlabel ('$m(\ell\ell bb)$ [GeV]', x=1.0, ha='right')
    axarr[1, 0].set_ylabel ('Entries / {0:.0f} GeV'.format (hist_incl.widths[0]), y=1.0, ha='right')
    axarr[1, 0].set_ylim (0., 1.2*np.nanmax([hist_bin3.values, hist_bin3_orig.values]))
    handles, labels = axarr[1, 0].get_legend_handles_labels()
    axarr[1, 0].legend (handles, labels, frameon=False, numpoints=1, loc="upper right", ncol=1, fontsize=12.)

    # m(H) bin 4
    #axarr[1, 1].set_title('$m_A-m_H\in[400, 500]$ GeV')
    #axarr[1, 1].set_title('$m_H\in[300, 400]$ GeV')
    axarr[1, 1].set_title('$m_H\in[380, 520]$ GeV')
    axarr[1, 1].errorbar (hist_bin4_orig.centers, hist_bin4_orig.values_nozeros, xerr=hist_bin4_orig.widths/2., yerr=hist_bin4_orig.errorsTotal,
            label='Original', fmt='o', mfc='k', mec='k', ecolor='k', capsize=0., ms=5., lw=1.2)
    axarr[1, 1].hist (hist_bin4.centers, bins=hist_bin4.edges, weights=hist_bin4.values,
            label='Morphed', histtype='step', align='mid', fc='none', ec='m', lw=1.5)
    xlimits = GetXAxisLimits (hist_bin4)
    axarr[1, 1].set_xlabel ('$m(\ell\ell bb)$ [GeV]', x=1.0, ha='right')
    axarr[1, 1].set_xlim (xlimits[0], xlimits[1])
    axarr[1, 1].set_ylim (0., 1.2*np.nanmax([hist_bin4.values, hist_bin4_orig.values]))
    handles, labels = axarr[1, 1].get_legend_handles_labels()
    axarr[1, 1].legend (handles, labels, frameon=False, numpoints=1, loc="upper right", ncol=1, fontsize=12.)

    # m(H) bin 5
    #axarr[1, 2].set_title('$m_A-m_H\in[500, 700]$ GeV')
    #axarr[1, 2].set_title('$m_H\in[400, 520]$ GeV')
    axarr[1, 2].set_title('$m_H\in[520, 700]$ GeV')
    axarr[1, 2].errorbar (hist_bin5_orig.centers, hist_bin5_orig.values_nozeros, xerr=hist_bin5_orig.widths/2., yerr=hist_bin5_orig.errorsTotal,
            label='Original', fmt='o', mfc='k', mec='k', ecolor='k', capsize=0., ms=5., lw=1.2)
    axarr[1, 2].hist (hist_bin5.centers, bins=hist_bin5.edges, weights=hist_bin5.values,
            label='Morphed', histtype='step', align='mid', fc='none', ec='m', lw=1.5)
    xlimits = GetXAxisLimits (hist_bin5)
    axarr[1, 2].set_xlabel ('$m(\ell\ell bb)$ [GeV]', x=1.0, ha='right')
    axarr[1, 2].set_xlim (xlimits[0], xlimits[1])
    axarr[1, 2].set_ylim (0., 1.2*np.nanmax([hist_bin5.values, hist_bin5_orig.values]))
    handles, labels = axarr[1, 2].get_legend_handles_labels()
    axarr[1, 2].legend (handles, labels, frameon=False, numpoints=1, loc="upper right", ncol=1, fontsize=12.)

    fig.subplots_adjust (hspace=0.2)
    plt.subplots_adjust(top = 0.95, right = 0.95, left = 0.05)
    fig.savefig ('./plots/morphed_orig_mA{0}_mH{1}.pdf'.format(mA, mH), format='pdf', facecolor='0.98')

    #plt.show()
