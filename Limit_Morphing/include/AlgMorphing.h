#ifndef ALGMORPHBASE_H
#define ALGMORPHBASE_H

#include <memory>
#include <string>
#include <vector>
#include <unordered_map>

#include "RooAbsPdf.h"
#include "RooArgSet.h"
#include "RooArgList.h"
#include "RooBinning.h"
#include "RooRealVar.h"

class TH1;
class RooDataHist;
class RooHistPdf;
class RooMomentMorph1D;
class RooMomentMorphND;

// ====================================================================================================

class AlgMorphBase
{
  public:
    // Default constructor
    AlgMorphBase ();
    // Default destructor
    virtual ~AlgMorphBase ();

    enum Method: int {Linear=0, SineLinear, NonLinear, NonLinearPosFractions, NonLinearLinFractions};

    bool AddBinning (unsigned int, const RooBinning&);
    virtual bool AddRefPointOnGrid (TH1* href, const std::vector<int>& point) = 0;
    virtual bool BuildMorphFcn () = 0;
    virtual bool DefineDim (unsigned nDim, std::string name, float min, float max);
    bool DoHorizontalMorphing () const { return mHorizontalMorphing; }
    const RooBinning& GetBinning (unsigned int, bool);
    unsigned GetDimension () const { return mObservables.size(); }
    const RooRealVar& GetObs (unsigned nDim) const;
    Method MorphingMethod () { return mSetting; }
    std::shared_ptr<TH1> RetrieveMorphedHist (const std::vector<int>& morphPts) const;
    virtual void Reset ();
    virtual std::shared_ptr<TH1> RunMorphing (const std::vector<int>& morphPts) = 0;
    void SetHorizontalMorphing (bool doIt = true) { mHorizontalMorphing = doIt; }
    void SetMorphingMethod (Method method) { mSetting = method; }
    static unsigned long long TotalID () { return mID; }

  protected:
    void AddToPdfList (const RooAbsPdf& pdf) { mPdfList.add(pdf); }
    const RooArgList& PdfList () const { return mPdfList; }
    virtual RooHistPdf* BuildHistPdf (TH1* hist);
    bool CheckDim (unsigned nDim) const;
    unsigned long long CurrentID () const { return mCurrentID; }
    std::string EncodePtsToStr (const std::vector<int>& morphPts) const;
    bool MorphedHistExists (const std::vector<int>& morphPts);
    bool RegisterMorphedHist (std::shared_ptr<TH1> pHist, const std::vector<int>& morphPts);
    const RooArgSet& ObsSet () const { return mObsSet; }

  private:
    bool CheckObsForDim (unsigned nDim) const;
    bool IsEmptyBinning (const RooBinning& binning) const;

    static unsigned long long mID;
    unsigned long long mCurrentID;
    std::vector<RooBinning> mBinnings;
    bool mHorizontalMorphing;
    std::unordered_map<std::string, std::shared_ptr<TH1> > mMorphHists;
    RooArgSet mObsSet;
    std::vector<std::unique_ptr<RooRealVar> > mObservables;
    std::vector<std::unique_ptr<RooDataHist> > mpDataHists;
    std::vector<std::unique_ptr<RooHistPdf>  > mpHistPdfs;
    RooArgList mPdfList;
    Method mSetting;
};

// ====================================================================================================

class AlgMorphOneDim : public AlgMorphBase
{
  public:
    // Default constructor
    AlgMorphOneDim ();
    // Default destructor
    ~AlgMorphOneDim ();

    bool AddRefPointOnGrid (TH1* href, const std::vector<int>& point) override;
    bool BuildMorphFcn () override;
    bool BuildMorphParam (std::string name, float min, float max);
    void Reset () override;
    std::shared_ptr<TH1> RunMorphing (const std::vector<int>& morphPts) override;

  private:
    std::vector<int> mRefPoints;
    std::unique_ptr<RooRealVar> mpMorphParam;
    std::unique_ptr<RooMomentMorph1D> mMorphFcn;
};

// ====================================================================================================

class AlgMorphTwoDim : public AlgMorphBase
{
  public:
    // Default constructor
    AlgMorphTwoDim ();
    // Default destructor
    ~AlgMorphTwoDim ();

    bool AddRefPointOnGrid (TH1* href, const std::vector<int>& point) override;
    bool BuildMorphFcn () override;
    bool BuildMorphParam (unsigned nDim, std::string name, float min, float max);
    void Reset () override;
    std::shared_ptr<TH1> RunMorphing (const std::vector<int>& morphPts) override;

  private:
    std::vector<int> mRefPointsI;
    std::vector<int> mRefPointsII;
    std::unique_ptr<RooRealVar> mpMorphParamI;
    std::unique_ptr<RooRealVar> mpMorphParamII;
    std::unique_ptr<RooMomentMorphND> mMorphFcn;
};

#endif
