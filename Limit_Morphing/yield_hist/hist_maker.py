#!/usr/bin/python -f

import glob, os
from ROOT import *
import VVlvjjSelections

files = glob.glob("${INPUT_TREE_DIRECTORY}*.root")

regions = ["SRWWResolved", "SRWZResolved",
           "SRWWResolvedVBF", "SRWZResolvedVBF",
           "SidebandResolved", "SidebandResolvedVBF",
           "BtagResolved", "BtagResolvedVBF",
           "SRWWHP", "SRWWLP",
           "SRWWHPVBF", "SRWWLPVBF",
           "SRWZHP", "SRWZLP",
           "SRWZHPVBF", "SRWZLPVBF",
           "SidebandHP", "SidebandHPVBF",
           "BtagHP", "BtagHPVBF", 
           "SidebandLP","SidebandLPVBF", 
           "BtagLP",  "BtagLPVBF"]

c = TCanvas()

for region in regions:
    outFile = TFile.Open( (region+".root"), "RECREATE")
    outFile.mkdir("Systematics")
    nom_dir = outFile
    sys_dir = outFile.Get("Systematics")

    this_selection = ""
    if region == "SRWWResolved":
        this_selection=VVlvjjSelections.SRWWResolved.Selection
    elif region == "SRWZResolved":
        this_selection=VVlvjjSelections.SRWZResolved.Selection
    elif region == "SRWWResolvedVBF":
        this_selection=VVlvjjSelections.SRWWResolvedVBF.Selection
    elif region == "SRWZResolvedVBF":
        this_selection=VVlvjjSelections.SRWZResolvedVBF.Selection
    elif region == "SidebandResolved":
        this_selection=VVlvjjSelections.SidebandResolved.Selection
    elif region == "SidebandResolvedVBF":
        this_selection=VVlvjjSelections.SidebandResolvedVBF.Selection
    elif region == "BtagResolved":
        this_selection=VVlvjjSelections.BtagResolved.Selection
    elif region == "BtagResolvedVBF":
        this_selection=VVlvjjSelections.BtagResolvedVBF.Selection
    elif region == "SRWWHP":
        this_selection=VVlvjjSelections.SRWWHP.Selection
    elif region == "SRWWLP":
        this_selection=VVlvjjSelections.SRWWLP.Selection
    elif region == "SRWWHPVBF":
        this_selection=VVlvjjSelections.SRWWHPVBF.Selection
    elif region == "SRWWLPVBF":
        this_selection=VVlvjjSelections.SRWWLPVBF.Selection
    elif region == "SRWZHP":
        this_selection=VVlvjjSelections.SRWZHP.Selection
    elif region == "SRWZLP":
        this_selection=VVlvjjSelections.SRWZLP.Selection
    elif region == "SRWZHPVBF":
        this_selection=VVlvjjSelections.SRWZHPVBF.Selection
    elif region == "SRWZLPVBF":
        this_selection=VVlvjjSelections.SRWZLPVBF.Selection
    elif region == "SidebandHP":
        this_selection=VVlvjjSelections.SidebandHP.Selection
    elif region == "SidebandHPVBF":
        this_selection=VVlvjjSelections.SidebandHPVBF.Selection
    elif region == "SidebandLP":
        this_selection=VVlvjjSelections.SidebandLP.Selection
    elif region == "SidebandLPVBF":
        this_selection=VVlvjjSelections.SidebandLPVBF.Selection
    elif region == "BtagHP":
        this_selection=VVlvjjSelections.BtagHP.Selection
    elif region == "BtagHPVBF":
        this_selection=VVlvjjSelections.BtagHPVBF.Selection
    elif region == "BtagLP":
        this_selection=VVlvjjSelections.BtagLP.Selection
    elif region == "BtagLPVBF":
        this_selection=VVlvjjSelections.BtagLPVBF.Selection
    if this_selection == "":
        print region
        
    ifile=0
    for file_name in files:
        ifile+=1
        print str(ifile) + "/" + str(len(files))
        this_file = TFile.Open(file_name, "READ")
        sig_name = file_name.split("/")[-1].replace(".root", "")

        weight_name = "weightBoosted"
        if "Resolved" in region:
            weight_name = "weightResolved"

        systs = [key.GetName() for key in this_file.GetListOfKeys()]
        for syst_name in systs:
            this_tree = this_file.Get(syst_name)

            hist_name = ""
            if syst_name == "nominal":
                hist_name = sig_name
            else:
                hist_name = sig_name + "_Sys" + syst_name
            this_hist = TH1F(hist_name, hist_name, 600, 0, 6000)
            if this_tree.GetEntries( "("+this_selection+")*"+weight_name) != 0:
                if "Resolved" in region and "SRWW" in region:
                    this_tree.Draw("lvjjmass_constW*0.001>>htemp(600, 0.0, 6000.0)", ("("+this_selection+")*"+weight_name))
                elif "Resolved" in region and "SRWZ" in region:
                    this_tree.Draw("lvjjmass_constZ*0.001>>htemp(600, 0.0, 6000.0)", ("("+this_selection+")*"+weight_name))
                elif "Resolved" in region:
                    this_tree.Draw("lvjjmass*0.001>>htemp(600, 0.0, 6000.0)", ("("+this_selection+")*"+weight_name))
                else:
                    this_tree.Draw("lvJmass*0.001>>htemp(600, 0.0, 6000.0)", ("("+this_selection+")*"+weight_name))
                this_hist = gPad.GetPrimitive("htemp")

            this_hist.SetName(hist_name)
            this_hist.SetTitle(hist_name)
            if syst_name == "nominal":
                this_hist.SetDirectory(nom_dir)
            else:
                this_hist.SetDirectory(sys_dir)






        this_file.Close()
    outFile.Write()
    outFile.Close()
