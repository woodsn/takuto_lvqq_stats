// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME libdIRooMomentMorph1D_dict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "RooMomentMorph1D.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_RooMomentMorph1D(void *p = 0);
   static void *newArray_RooMomentMorph1D(Long_t size, void *p);
   static void delete_RooMomentMorph1D(void *p);
   static void deleteArray_RooMomentMorph1D(void *p);
   static void destruct_RooMomentMorph1D(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooMomentMorph1D*)
   {
      ::RooMomentMorph1D *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooMomentMorph1D >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooMomentMorph1D", ::RooMomentMorph1D::Class_Version(), "RooMomentMorph1D.h", 26,
                  typeid(::RooMomentMorph1D), DefineBehavior(ptr, ptr),
                  &::RooMomentMorph1D::Dictionary, isa_proxy, 4,
                  sizeof(::RooMomentMorph1D) );
      instance.SetNew(&new_RooMomentMorph1D);
      instance.SetNewArray(&newArray_RooMomentMorph1D);
      instance.SetDelete(&delete_RooMomentMorph1D);
      instance.SetDeleteArray(&deleteArray_RooMomentMorph1D);
      instance.SetDestructor(&destruct_RooMomentMorph1D);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooMomentMorph1D*)
   {
      return GenerateInitInstanceLocal((::RooMomentMorph1D*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooMomentMorph1D*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr RooMomentMorph1D::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooMomentMorph1D::Class_Name()
{
   return "RooMomentMorph1D";
}

//______________________________________________________________________________
const char *RooMomentMorph1D::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooMomentMorph1D*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooMomentMorph1D::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooMomentMorph1D*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooMomentMorph1D::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooMomentMorph1D*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooMomentMorph1D::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooMomentMorph1D*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void RooMomentMorph1D::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooMomentMorph1D.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RooMomentMorph1D::Class(),this);
   } else {
      R__b.WriteClassBuffer(RooMomentMorph1D::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooMomentMorph1D(void *p) {
      return  p ? new(p) ::RooMomentMorph1D : new ::RooMomentMorph1D;
   }
   static void *newArray_RooMomentMorph1D(Long_t nElements, void *p) {
      return p ? new(p) ::RooMomentMorph1D[nElements] : new ::RooMomentMorph1D[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooMomentMorph1D(void *p) {
      delete ((::RooMomentMorph1D*)p);
   }
   static void deleteArray_RooMomentMorph1D(void *p) {
      delete [] ((::RooMomentMorph1D*)p);
   }
   static void destruct_RooMomentMorph1D(void *p) {
      typedef ::RooMomentMorph1D current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooMomentMorph1D

namespace {
  void TriggerDictionaryInitialization_RooMomentMorph1D_dict_Impl() {
    static const char* headers[] = {
"RooMomentMorph1D.h",
0
    };
    static const char* includePaths[] = {
"./include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include",
"/data/data3/zp/tkunigo/StatFramework/Limit_Morphing/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate(R"ATTRDUMP(Your description goes here...)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$RooMomentMorph1D.h")))  RooMomentMorph1D;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "RooMomentMorph1D.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"RooMomentMorph1D", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("RooMomentMorph1D_dict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_RooMomentMorph1D_dict_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_RooMomentMorph1D_dict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_RooMomentMorph1D_dict() {
  TriggerDictionaryInitialization_RooMomentMorph1D_dict_Impl();
}
