#include "AlgMorphing.h"
#include "TH1.h"
#include "RooRealVar.h"
#include "RooDataHist.h"
#include "RooHistPdf.h"
#include "RooMomentMorph1D.h"
#include "RooMomentMorphND.h"

// ====================================================================================================

// ***************************
//  AlgMorphBase Definitions *
// ***************************

// Static members
// --------------
unsigned long long AlgMorphBase::mID = 0;

// Public members
// --------------
AlgMorphBase::AlgMorphBase ():
  mCurrentID (mID++),
  mHorizontalMorphing (true),
  mSetting (NonLinear)
{
}

AlgMorphBase::~AlgMorphBase ()
{
}

bool AlgMorphBase::AddBinning (unsigned nDim, const RooBinning& binning)
{
  if (!CheckObsForDim(nDim))
  {
    return false;
  }
  if (mBinnings.size() >= nDim)
  {
    if ( IsEmptyBinning(mBinnings.at(nDim-1)) )
    {
      mBinnings[nDim-1] = binning;
      return true;
    }
    else
    {
      Warning ("AlgMorphBase::AddBinning()", "Binning already defined for dimension n = %d", nDim);
      return false;
    }
  }

  mBinnings.push_back(binning);
  return true;
}

bool AlgMorphBase::DefineDim (unsigned nDim, std::string name, float min, float max)
{
  if (!CheckDim(nDim))
  {
    return false;
  }
  if (mObservables.size() >= nDim)
  {
    std::string ndim_str = nDim == 1 ? "1st" : "2nd";
    std::string dim_name = mObservables.at(nDim-1)->GetName();
    Error ("AlgMorphingBase::DefineDim()", "The %s dimension alredy exists with name %s",
        ndim_str.c_str(), dim_name.c_str());
    return false;
  }
  else if (nDim == 2 && !CheckObsForDim(1))
  {
    Error ("AlgMorphingBase::DefineDim()", "Define the 1st dimension first before adding the 2nd!");
    return false;
  }
  mObservables.emplace_back(new RooRealVar(name.c_str(), name.c_str(), min, max) );

  return mObsSet.add(*mObservables.back(), false);
}

const RooBinning& AlgMorphBase::GetBinning (unsigned nDim, bool createIfNone)
{
  if ( mBinnings.size() < nDim || IsEmptyBinning(mBinnings.at(nDim-1)) )
  {
    if (createIfNone)
    {
      if (nDim == 2 && mBinnings.empty())
      {
        mBinnings.push_back(RooBinning());
      }
      const RooRealVar& obs = GetObs (nDim-1);
      double max = obs.getMax();
      double min = obs.getMin();
      // Uniform binning with 10 GeV bin width
      int nbins = (max - min) / 10.0;
      mBinnings.push_back(RooBinning(nbins, min, max));
    }
    else
    {
      Error ("AlgMorphBase::GetBinning()", "Binning not defined for"
          " dimension n = %u!", nDim);
      throw "Failure in getting binning!";
    }
  }

  return mBinnings.at(nDim-1);
}

const RooRealVar& AlgMorphBase::GetObs (unsigned nDim) const
{
  if (!CheckObsForDim(nDim))
  {
    Error ("AlgMorphBase::GetObs()", "Failed to retrieve the Observable"
        " for dimention n = %d", nDim);
    throw "Failure in getting observable!";
  }
  return *mObservables.at(nDim-1);
}

std::shared_ptr<TH1> AlgMorphBase::RetrieveMorphedHist (const std::vector<int>& morphPts) const
{
  std::string ref_str = EncodePtsToStr (morphPts);
  if (mMorphHists.find(ref_str) == mMorphHists.end())
  {
    if (morphPts.size() == 0)
    {
      Warning ("AlgMorphBase::RetrieveMorphedHist()", "Requested histogram hasn't beem morphed"
          " for morphing point X = %d", morphPts.at(0) );
    }
    else
    {
      Warning ("AlgMorphBase::RetrieveMorphedHist()", "Requested histogram hasn't beem morphed"
          " for morphing point X = %d, Y = %d", morphPts.at(0), morphPts.at(1) );
    }
    return nullptr;
  }
  return mMorphHists.at(ref_str);
}

void AlgMorphBase::Reset ()
{
  mCurrentID = mID++;
  mMorphHists.clear();
  mpHistPdfs.clear();
  mpDataHists.clear();
  mPdfList.removeAll();
}

// Protected member
// ----------------
RooHistPdf* AlgMorphBase::BuildHistPdf (TH1* hist)
{
  if (static_cast<unsigned>(hist->GetDimension()) != mObservables.size())
  {
    Error ("AlgMorphBase::BuildHistPdf()", "Histgram's dimension (n=%d) does not agree with"
        " the number of observables (N=%lu) defined!", hist->GetDimension(), mObservables.size());
    throw "Inconsistent dimensions!";
  }
  std::string dh_name = std::string("dh_ID") + std::to_string(CurrentID()) + "_" + hist->GetName();
  std::string hpdf_name = std::string("hpdf_ID") + std::to_string(CurrentID()) + "_" + hist->GetName();
  mpDataHists.emplace_back(new RooDataHist(dh_name.c_str(), dh_name.c_str(),
        RooArgList(mObsSet), hist ) );
  mpHistPdfs.emplace_back(new RooHistPdf(hpdf_name.c_str(), hpdf_name.c_str(),
        RooArgList(mObsSet), *(mpDataHists.back()) ) );

  return mpHistPdfs.back().get();
}

bool AlgMorphBase::CheckDim (unsigned nDim) const
{
  if (nDim != 1 && nDim != 2)
  {
    Error ("AlgMorphingBase::CheckDim()", "Only first (nDim==1) and second (nDim==2)"
        " dimensions are supported!");
    return false;
  }
  return true;
}

std::string AlgMorphBase::EncodePtsToStr (const std::vector<int>& morphPts) const
{
  std::string ref_str ("");
  for (auto val: morphPts)
  {
    ref_str += std::to_string(val) + "_";
  }
  return ref_str;
}

bool AlgMorphBase::MorphedHistExists (const std::vector<int>& morphPts)
{
  std::string ref_str = EncodePtsToStr (morphPts);
  return mMorphHists.find(ref_str) != mMorphHists.end();
}

bool AlgMorphBase::RegisterMorphedHist (std::shared_ptr<TH1> pHist, const std::vector<int>& morphPts)
{
  std::string ref_str = EncodePtsToStr (morphPts);
  if (mMorphHists.find(ref_str) != mMorphHists.end())
  {
    if (morphPts.size() == 0)
    {
      Warning ("AlgMorphBase::RegisterMorphedHist()", "Histogram already exists"
          " for morphing point X = %d", morphPts.at(0) );
    }
    else
    {
      Warning ("AlgMorphBase::RegisterMorphedHist()", "Histogram already exists"
          " for morphing point X = %d, Y = %d", morphPts.at(0), morphPts.at(1) );
    }
    return false;
  }
  mMorphHists[ref_str] = pHist;

  return true;
}

// Private member
// --------------
bool AlgMorphBase::CheckObsForDim (unsigned nDim) const
{
  if (mObservables.size() < nDim)
  {
    Error ("AlgMorphBase::CheckObsForDim()", "Dimension n=%d either does not"
        " exist or is not valid", nDim);
    return false;
  }
  return true;
}

bool AlgMorphBase::IsEmptyBinning (const RooBinning& binning) const
{
  return binning.lowBound() == -RooNumber::infinity()
    && binning.highBound() == RooNumber::infinity();
}

// ====================================================================================================

// *****************************
//  AlgMorphOneDim Definitions *
// *****************************

// Public members
// --------------
AlgMorphOneDim::AlgMorphOneDim ():
  AlgMorphBase(),
  mpMorphParam (new RooRealVar("alpha", "alpha", 0, 1000))
{
}

AlgMorphOneDim::~AlgMorphOneDim ()
{
}

bool AlgMorphOneDim::AddRefPointOnGrid (TH1* href, const std::vector<int>& point)
{
  if (point.size() != 1)
  {
    Error ("AlgMorphOneDim::AddRefPointOnGrid()", "ONLY unidimensional morphing allowed!");
    return false;
  }
  if (std::find(mRefPoints.begin(), mRefPoints.end(), point.at(0)) != mRefPoints.end())
  {
    Warning ("AlgMorphOneDim::AddRefPointOnGrid()", "Reference point %d already added!", point.at(0));
    return false;
  }
  mRefPoints.push_back(point.at(0));
  try
  {
    RooHistPdf* hist_pdf = BuildHistPdf (href);
    AddToPdfList ( *hist_pdf );
  }
  catch (const char* msg)
  {
    std::cerr << msg << std::endl;
    exit(1);
  }

  return true;
}

bool AlgMorphOneDim::BuildMorphFcn ()
{
  if (GetDimension() == 0)
  {
    Error ("AlgMorphOneDim::BuildMorphFcn()", "No dimension defined!");
    return false;
  }
  else if (mRefPoints.size() < 2)
  {
    Error ("AlgMorphOneDim::BuildMorphFcn()", "At least two reference points needed!"
        " (%lu provided)", mRefPoints.size());
    return false;
  }
  if (!mpMorphParam)
  {
    Error ("AlgMorphOneDim::BuildMorphFcn()", "Morphing parameter not properly built!");
    return false;
  }

  TVectorD ref_vals_vecD ( mRefPoints.size() );
  for (std::size_t i = 0; i != mRefPoints.size(); ++i)
  {
    ref_vals_vecD[i] = mRefPoints.at(i) / 1000.0;
  }

  RooMomentMorph1D::Setting setting (RooMomentMorph1D::NonLinear);
  switch (MorphingMethod())
  {
    case Linear:
      setting = RooMomentMorph1D::Linear;
      break;
    case SineLinear:
      setting = RooMomentMorph1D::SineLinear;
      break;
    case NonLinear:
      setting = RooMomentMorph1D::NonLinear;
      break;
    case NonLinearPosFractions:
      setting = RooMomentMorph1D::NonLinearPosFractions;
      break;
    case NonLinearLinFractions:
      setting = RooMomentMorph1D::NonLinearLinFractions;
      break;
    default:
      Warning ("AlgMorphOneDim::BuildMorphFcn()", "Unknown morphing setting, "
          "falling back to NonLinear!");
      break;
  }

  mMorphFcn.reset( new RooMomentMorph1D ("morph","morph", *mpMorphParam.get(),
        RooArgList(ObsSet()), PdfList(), ref_vals_vecD, setting) );
  mMorphFcn->useHorizontalMorphing(DoHorizontalMorphing());

  return true;
}

bool AlgMorphOneDim::BuildMorphParam (std::string name, float min, float max)
{
  mpMorphParam.reset(new RooRealVar(name.c_str(), name.c_str(), min, max) );
  return true;
}

void AlgMorphOneDim::Reset ()
{
  AlgMorphBase::Reset();
  mRefPoints.clear();
  mMorphFcn.reset(nullptr);
}

std::shared_ptr<TH1> AlgMorphOneDim::RunMorphing (const std::vector<int>& morphPts)
{
  if (!mMorphFcn)
  {
    Error ("AlgMorphOneDim::RunMorphing()", "Morphing function not properly set up!");
    return nullptr;
  }
  if (morphPts.size() != 1)
  {
    Error ("AlgMorphOneDim::RunMorphing()", "%lu-D morphing vector provided while"
        " only 1-D is supported!", morphPts.size());
    return nullptr;
  }
  if ( MorphedHistExists(morphPts) )
  {
    Info ("AlgMorphOneDim::RunMorphing()", "Morphed histogram already exists for"
        " morphing point %d", morphPts.at(0));
    return RetrieveMorphedHist(morphPts);
  }

  mpMorphParam->setVal(morphPts.at(0)/1000.);
  std::shared_ptr<TH1> p_hist;
  std::string ref_str = EncodePtsToStr (morphPts);
  std::string hist_name = "Morphed_ID" + std::to_string(CurrentID()) + "_" + ref_str;
  if (GetDimension() == 1)
  {
    p_hist.reset( mMorphFcn->createHistogram(hist_name.c_str(), GetObs(1),
          RooFit::Binning(GetBinning(1, true))) );
  }
  else
  {
    p_hist.reset( mMorphFcn->createHistogram(hist_name.c_str(), GetObs(1),
          RooFit::Binning(GetBinning(1, true)), RooFit::YVar(GetObs(2),
            RooFit::Binning(GetBinning(2, true)))) );
  }
  RegisterMorphedHist (p_hist, morphPts);

  return p_hist;
}

// ====================================================================================================

// *****************************
//  AlgMorphTwoDim Definitions *
// *****************************

// Public members
// --------------
AlgMorphTwoDim::AlgMorphTwoDim ():
  AlgMorphBase(),
  mpMorphParamI  (new RooRealVar("alpha", "alpha", 0, 1000)),
  mpMorphParamII (new RooRealVar("beta" , "beta" , 0, 1000))
{
}

AlgMorphTwoDim::~AlgMorphTwoDim ()
{
}

bool AlgMorphTwoDim::AddRefPointOnGrid (TH1* href, const std::vector<int>& point)
{
  if (point.size() != 2)
  {
    Error ("AlgMorphTwoDim::AddRefPointOnGrid()", "ONLY two-dimensional morphing allowed!");
    return false;
  }
  unsigned posI = std::distance(mRefPointsI.begin(), std::find(mRefPointsI.begin(),
        mRefPointsI.end(), point.at(0)) );
  unsigned posII = std::distance(mRefPointsII.begin(), std::find(mRefPointsII.begin(),
        mRefPointsII.end(), point.at(0)) );
  if (posI != mRefPointsI.size() && posI == posII)
  {
    Warning ("AlgMorphTwoDim::AddRefPointOnGrid()", "Reference point %d - %d already added!",
        point.at(0), point.at(1));
    return false;
  }
  mRefPointsI.push_back(point.at(0));
  mRefPointsII.push_back(point.at(1));
  try
  {
    RooHistPdf* hist_pdf = BuildHistPdf (href);
    AddToPdfList ( *hist_pdf );
  }
  catch (const char* msg)
  {
    std::cerr << msg << std::endl;
    exit(1);
  }

  return true;
}

bool AlgMorphTwoDim::BuildMorphFcn ()
{
  if (GetDimension() == 0)
  {
    Error ("AlgMorphTwoDim::BuildMorphFcn()", "No dimension defined!");
    return false;
  }
  else if (mRefPointsI.size() < 2)
  {
    Error ("AlgMorphTwoDim::BuildMorphFcn()", "At least two reference points needed!"
        " (%lu provided)", mRefPointsI.size());
    return false;
  }
  if (!mpMorphParamI || !mpMorphParamII)
  {
    Error ("AlgMorphTwoDim::BuildMorphFcn()", "Morphing parameters not properly built!");
    return false;
  }

  // Add p.d.f.s to a RooMomentMorphND::Grid.
  // ----------------------------------------
  // Make copyies of the reference point vectors.
  // Then remove the duplicate entries.
  std::vector<int> vec_x = mRefPointsI; std::sort (vec_x.begin(), vec_x.end());
  auto last_x = std::unique (vec_x.begin(), vec_x.end()); vec_x.erase (last_x, vec_x.end());
  std::vector<int> vec_y = mRefPointsII; std::sort (vec_y.begin(), vec_y.end());
  auto last_y = std::unique (vec_y.begin(), vec_y.end()); vec_y.erase (last_y, vec_y.end());
  double *arr_x = new double [vec_x.size()];
  for (unsigned i = 0; i < vec_x.size(); ++i) { arr_x[i] = vec_x.at(i)/1000.; }
  double *arr_y = new double [vec_y.size()];
  for (unsigned i = 0; i < vec_y.size(); ++i) { arr_y[i] = vec_y.at(i)/1000.; }
  // Space points on the grid
  RooBinning dim1 (vec_x.size()-1, arr_x, "BinningDim1");
  RooBinning dim2 (vec_y.size()-1, arr_y, "BinningDim2");
  RooMomentMorphND::Grid ref_grid (dim1, dim2);
  // Loop over reference points and add pdfs.
  for (unsigned idx = 0; idx != mRefPointsI.size(); ++idx)
  {
    int val_I = mRefPointsI.at(idx);
    int val_II = mRefPointsII.at(idx);
    int bin1 = std::distance (vec_x.begin(), std::find(vec_x.begin(), vec_x.end(), val_I));
    int bin2 = std::distance (vec_y.begin(), std::find(vec_y.begin(), vec_y.end(), val_II));
    ref_grid.addPdf (*static_cast<RooAbsPdf*>(PdfList().at(idx)), bin1, bin2);
  }

  RooMomentMorphND::Setting setting (RooMomentMorphND::NonLinear);
  switch (MorphingMethod())
  {
    case Linear:
      setting = RooMomentMorphND::Linear;
      break;
    case SineLinear:
      setting = RooMomentMorphND::SineLinear;
      break;
    case NonLinear:
      setting = RooMomentMorphND::NonLinear;
      break;
    case NonLinearPosFractions:
      setting = RooMomentMorphND::NonLinearPosFractions;
      break;
    case NonLinearLinFractions:
      setting = RooMomentMorphND::NonLinearLinFractions;
      break;
    default:
      Warning ("AlgMorphOneDim::BuildMorphFcn()", "Unknown morphing setting, "
          "falling back to NonLinear!");
      break;
  }
  // Build morphing function
  mMorphFcn.reset( new RooMomentMorphND ("morph","morph",
        RooArgList(*mpMorphParamI.get(), *mpMorphParamII.get()),
      RooArgList(ObsSet()), ref_grid, setting) );
  mMorphFcn->useHorizontalMorphing(DoHorizontalMorphing());

  delete [] arr_x;
  delete [] arr_y;

  return true;
}

bool AlgMorphTwoDim::BuildMorphParam (unsigned nDim, std::string name, float min, float max)
{
  if (!CheckDim(nDim))
  {
    return false;
  }
  if (nDim == 1)
  {
    mpMorphParamI.reset(new RooRealVar(name.c_str(), name.c_str(), min, max) );
  }
  else
  {
    mpMorphParamII.reset(new RooRealVar(name.c_str(), name.c_str(), min, max) );
  }
  return true;
}

void AlgMorphTwoDim::Reset ()
{
  AlgMorphBase::Reset();
  mRefPointsI.clear();
  mRefPointsII.clear();
  mMorphFcn.reset(nullptr);
}

std::shared_ptr<TH1> AlgMorphTwoDim::RunMorphing (const std::vector<int>& morphPts)
{
  if (!mMorphFcn)
  {
    Error ("AlgMorphTwoDim::RunMorphing()", "Morphing function not properly set up!");
    return nullptr;
  }
  if (morphPts.size() != 2)
  {
    Error ("AlgMorphTwoDim::RunMorphing()", "%lu-D morphing vector provided while"
        " only 2-D is supported!", morphPts.size());
    return nullptr;
  }
  if ( MorphedHistExists(morphPts) )
  {
    Info ("AlgMorphTwoDim::RunMorphing()", "Morphed histogram already exists for"
        " morphing point %d - %d", morphPts.at(0), morphPts.at(1) );
    return RetrieveMorphedHist(morphPts);
  }

  mpMorphParamI->setVal(morphPts.at(0)/1000.);
  mpMorphParamII->setVal(morphPts.at(1)/1000.);
  std::shared_ptr<TH1> p_hist;
  std::string ref_str = EncodePtsToStr (morphPts);
  std::string hist_name = "Morphed_ID" + std::to_string(CurrentID()) + "_" + ref_str;
  if (GetDimension() == 1)
  {
    p_hist.reset( mMorphFcn->createHistogram(hist_name.c_str(), GetObs(1),
          RooFit::Binning(GetBinning(1, true))) );
  }
  else
  {
    p_hist.reset( mMorphFcn->createHistogram(hist_name.c_str(), GetObs(1),
          RooFit::Binning(GetBinning(1, true)), RooFit::YVar(GetObs(2),
            RooFit::Binning(GetBinning(2, true)))) );
  }
  RegisterMorphedHist (p_hist, morphPts);

  return p_hist;
}
