#include <string>
#include <vector>
#include "AlgMorphing.h"
#include "TH1.h"
#include "TH2.h"
#include "TError.h"
#include "TFile.h"
#include "TString.h"
#include "TSystem.h"
#include "TPython.h"

void InterpolateRegion (TFile* inf, TString sigStrFmt, TString sysName = "")
{
  for (int mass = 300; mass <= 5000; )
  {
    if (mass <= 1000) mass += 10;
    else if (mass <= 2000) mass += 20;
    else if (mass <= 3000) mass += 50;
    else mass += 100;
  }
}

int main(int argc, char* argv[])
{
  // Message level
  RooMsgService::instance().setGlobalKillBelow(RooFit::FATAL);

  AlgMorphOneDim mp_engine;

  TString inf ("/net/s3_datad/zxi/Dilep/ZVllqq/ZVllqq_28-01_prod_10/hist-RS_G_ZZ.root");
  TFile* f = TFile::Open(inf, "read");

  mp_engine.SetHorizontalMorphing(true);
  mp_engine.SetMorphingMethod(AlgMorphBase::Linear);

  mp_engine.DefineDim(1, "x", 0, 6000);
  mp_engine.AddBinning(1, RooBinning(600, 0, 6000));

  // Morphing point
  int mX_morph (2000);

  if(argc > 1)
  {
    mX_morph = std::stoi(argv[1]);
  }

  // Add all the reference points
  for (int mX = 300; mX <= 5000; mX += 100)
  {
    if (mX == mX_morph) continue;
    TString h1_name = Form ("RSGZZllqq%d_0ptag1pfat0pjet_0ptv_MergedHPSROfRSG_llJMassScaled", mX);
    TH1* h1 = dynamic_cast<TH1*>( f->Get(h1_name) );
    if (!h1) continue;
    Info ("textMorphZV()", "Adding reference point with mX = %d", mX);
    mp_engine.AddRefPointOnGrid (h1, {mX});
  }

  // Build and run morphing
  bool built = mp_engine.BuildMorphFcn();
  if (!built) return 1;
  TH1* hh = static_cast<TH1*>( mp_engine.RunMorphing({mX_morph}).get() );

  TString sig_title = Form ("RSGZZllqq%d", mX_morph);
  TH1* hh_orig = dynamic_cast<TH1*>(f->Get (sig_title + "_0ptag1pfat0pjet_0ptv_MergedHPSROfRSG_llJMassScaled"));

  hh->Scale (1.0/hh->Integral()*hh_orig->Integral());

  TString fout_name = Form ("output_morph/morphZV_mX%d.root", mX_morph);
  TFile* fout = new TFile (fout_name, "recreate");
  fout->cd();

  hh->SetName(hh_orig->GetName());
  hh->SetTitle(hh_orig->GetTitle());
  hh->Write();
  TString hname_orig = hh_orig->GetName() + TString("_orig");
  hh_orig->SetName(hname_orig);
  hh_orig->Write();

  fout->Close();

  //TString sys_argv = Form("sys.argv=['dummy', '%d', '%d']", int(mX_morph), int(mH_morph) );
  //TPython::Exec("import sys");
  //TPython::Exec(sys_argv);
  //TPython::LoadMacro("./scripts/compareMorphing.py");
}
