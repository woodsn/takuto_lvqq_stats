#include <string>
#include <vector>
#include <algorithm>
#include "AlgMorphing.h"
#include "TH1.h"
#include "TH2.h"
#include "TGraph.h"
#include "TError.h"
#include "TFile.h"
#include "TString.h"
#include "TSystem.h"
#include "TPython.h"
#include "TSpline.h"
#include "TMath.h"
#include <boost/lexical_cast.hpp>

void NextMassPoint (int& mass)
{
  // The next mass point to be interpolated
  if (mass < 1000) mass += 10;
  else if (mass < 2000) mass += 20;
  else if (mass < 3000) mass += 50;
  else mass += 100;
}

void CheckHistForNaN (TH1* h)
{
  for (int i = 1; i < h->GetNbinsX()+1; ++i)
  {
    if (TMath::IsNaN(h->GetBinContent(i)) || h->GetBinContent(i) < 0)
      h->SetBinContent(i, 0.0);
  }
}

std::vector<int> FindNearestPoints (int mass, const std::vector<int>& pts, int N)
{
  std::vector<int> all_pts = pts;
  all_pts.push_back(mass);
  std::sort(all_pts.begin(), all_pts.end());
  unsigned idx = std::distance(all_pts.begin(), std::find(all_pts.begin(), all_pts.end(), mass));
  unsigned begin_idx (0);
  if (idx >= std::floor(N/2.0))
    begin_idx = idx - std::floor(N/2.0);
  if (begin_idx + N - 1 >= pts.size())
    begin_idx = pts.size() - N;
  std::vector<int> results;
  for (int i = 0; i < N; ++i)
  {
    if (begin_idx + 1 >= 0)
      results.push_back(pts.at(begin_idx+i));
  }
  return results;
}

std::vector<std::shared_ptr<TH1> > InterpolateRegion (TFile* inf, TString sigStrFmt, bool isCR, bool isResolved, TString sysName = "", int nNearest = 4)
{
  TH1::AddDirectory (kFALSE);
  // The morphing engine
  AlgMorphOneDim mp_engine;
  // Morphing method
  mp_engine.SetHorizontalMorphing(true);
  mp_engine.SetMorphingMethod(AlgMorphBase::Linear);
  // Define dimension
  mp_engine.DefineDim(1, "x", 0, 6000);
  mp_engine.AddBinning(1, RooBinning(600, 0, 6000));

  // Empty string in nominal case
  if (sysName.CompareTo("nominal", TString::kIgnoreCase) == 0)
  {
    sysName = "";
  }

  if (sysName.IsNull())
    Info ("InterpolateRegion()", "Working on nominal");
  else
    Info ("InterpolateRegion()", "Working on %s", sysName.Data());

  int mass_thres_resolved (1500);

  // A TGraph of signal yields
  int i_point (0);
  TGraph g_yields;
  // Input boundaries
  int mass_low (5e4), mass_high (0);
  // Vector of available signal points
  std::vector<int> avail_pts;
  // Map of simulated signals
  std::unordered_map<int, TH1*> map_hists;
  // Add all the reference points
  for (int mass = 300; mass <= 5000; mass += 100)
  {
    // Define signal histogram name
    TString h1_name  = sysName.IsNull() ? sigStrFmt : "Systematics/" + sigStrFmt + "_Sys" + sysName;
    h1_name.ReplaceAll("MASS", TString::Itoa(mass, 10));
    TH1* h1 = dynamic_cast<TH1*>( inf->Get(h1_name) );
    if (!h1) continue;
    CheckHistForNaN (h1);
    if (h1->Integral() <= 0) continue;
    //Info ("InterpolateRegion()", "Found reference point with mass = %d", mass);
    map_hists[mass] = h1;
    avail_pts.push_back (mass);
    //mp_engine.AddRefPointOnGrid (map_hists.at(mass), {mass});
    g_yields.SetPoint (i_point, mass, h1->Integral());
    i_point++;
    if (mass < mass_low) mass_low = mass;
    if (mass > mass_high) mass_high = mass;
  }

  if (avail_pts.size() < 2)
  {
    Warning("InterpolateRegion()", "A minimum of two reference points needed for morphing!");
    return {};
  }

  // Interpolation of signal yields
  TSpline3 sp_yields ("sp_"+sysName, &g_yields, "b1e1");

  std::vector<std::shared_ptr<TH1> > vec_morph;
  // Do the interpolations
  for (int mass = 300; mass < 5000; )
  {
    // Safety guard
    if (mass <= mass_low)
    {
      NextMassPoint (mass);
      continue;
    }
    else if (mass >= mass_high)
    {
      break;
    }
    // If simulated signal, skip it.
    if (std::find(avail_pts.begin(), avail_pts.end(), mass) != avail_pts.end())
    {
      NextMassPoint (mass);
      continue;
    }
    std::vector<int> nearest_masses = FindNearestPoints (mass, avail_pts, nNearest);
    double sig_yield (sp_yields.Eval(mass));
    if (sig_yield < 0.0)
    {
      TGraph g_yields_local;
      for (unsigned i = 0; i < nearest_masses.size(); ++i)
      {
        int m = nearest_masses.at(i);
        g_yields_local.SetPoint (i, m, sp_yields.Eval(m));
      }
      TSpline3 sp_yields_local ("sp_"+sysName+"_local", &g_yields_local, "b2e2");
      sig_yield = sp_yields_local.Eval(mass);
      if (sig_yield < 0.0)
      {
        std::vector<int> adj_masses = FindNearestPoints (mass, avail_pts, 2);
        float m0 = adj_masses.at(0);
        float m1 = adj_masses.at(1);
        sig_yield = sp_yields.Eval(m0) + (mass-m0)/(m1-m0)*(sp_yields.Eval(m1)-sp_yields.Eval(m0));
      }
    }
    //Info ("InterpolateRegion()", "Interpolating mass = %d with yield = %.3f", mass, sig_yield);
    // Define signal histogram name
    TString h1_name = sysName.IsNull() ? sigStrFmt : sigStrFmt + "_Sys" + sysName;
    h1_name.ReplaceAll("MASS", TString::Itoa(mass, 10));
    if (isCR || (isResolved && mass > mass_thres_resolved))
    {
      // Create a histogram with only one non-empty bin.
      // The histogram is normalized to the expected yield of the signal in the CR.
      std::shared_ptr<TH1> h_ptr ( new TH1F (h1_name, h1_name, 600, 0, 6000) );
      h_ptr->Fill(3000., sig_yield);
      vec_morph.push_back (h_ptr);
    }
    else
    {
      mp_engine.Reset();
      for (auto m: nearest_masses)
      {
        //Info ("InterpolateRegion()", "Adding reference point with mass = %d", m);
        mp_engine.AddRefPointOnGrid (map_hists.at(m), {m});
      }
      bool built = mp_engine.BuildMorphFcn();
      if (!built)
      {
        Error ("InterpolateRegion()", "Failed to build the morphing function!");
        throw;
      }
      // Morphing and normalization
      std::shared_ptr<TH1> h_ptr = mp_engine.RunMorphing({mass});
      h_ptr->SetName (h1_name);
      h_ptr->SetTitle (h1_name);
      CheckHistForNaN (h_ptr.get());
      h_ptr->Scale(1.0/h_ptr->Integral()*sig_yield);
      vec_morph.push_back( h_ptr );
    }
    NextMassPoint (mass);
  }

  return vec_morph;
}

int main(int argc, char* argv[])
{
  if(argc < 3)
  {
    std::cout << "./bin/morphingZV.exe #Region #Syst" << std::endl;
    return 0;
  }

  unsigned regionID = boost::lexical_cast<unsigned>(argv[1]);
  unsigned jobID = boost::lexical_cast<unsigned>(argv[2]);

  // Message level
  RooMsgService::instance().setGlobalKillBelow(RooFit::FATAL);
  RooMsgService::instance().getStream(1).removeTopic(RooFit::NumIntegration);

  std::vector<TString> regions = {
    "SRWWResolved", "SRWZResolved",
    "SRWWResolvedVBF", "SRWZResolvedVBF",
    "SidebandResolved", "SidebandResolvedVBF",
    "BtagResolved", "BtagResolvedVBF",
    "SRWWHP", "SRWWLP",
    "SRWWHPVBF", "SRWWLPVBF",
    "SRWZHP", "SRWZLP",
    "SRWZHPVBF", "SRWZLPVBF",
    "SidebandHP", "SidebandHPVBF",
    "BtagHP", "BtagHPVBF", 
    "SidebandLP","SidebandLPVBF", 
    "BtagLP",  "BtagLPVBF" };

  TString region = regions.at(regionID);

  std::vector<TString> one_sided_syst = {
    "MET_SoftTrk_ResoPara",
    "MET_SoftTrk_ResoPerp",
    "JET_JER_SINGLE_NP",
    "FATJET_Medium_JET_Comb_Baseline_Kin",
    "FATJET_D2R",
    "FATJET_JER",
    "FATJET_JMR",
    "WjetsModeling_AlphaS",
    "WjetsModeling_MadGraph",
    "WjetsModeling_CKKW15",
    "WjetsModeling_CKKW30",
    "WjetsModeling_PDF",
    "TopModeling_MCatNLO",
    "TopModeling_Rad",
    "TopModeling_Herwig"
  };
  
  std::vector<TString> two_sided_syst = {
    "MUON_EFF_STAT","MUON_EFF_SYS",
    "MUON_EFF_STAT_LOWPT","MUON_EFF_SYS_LOWPT",
    "MUON_ISO_STAT","MUON_ISO_SYS",
    "MUON_ID",
    "MUON_MS",
    "MUON_SCALE",
    "MUON_SAGITTA_RESBIAS","MUON_SAGITTA_RHO",
    "MUON_TTVA_STAT","MUON_TTVA_SYS",
    "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR",
    "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR",
    "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR",
    "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR",
    "EG_RESOLUTION_ALL",
    "EG_SCALE_ALL",
    "MET_SoftTrk_Scale",
    "METTrigStat","METTrigTop",
    "FT_EFF_Eigen_B_0_AntiKt4EMTopoJets","FT_EFF_Eigen_B_1_AntiKt4EMTopoJets","FT_EFF_Eigen_B_2_AntiKt4EMTopoJets",
    "FT_EFF_Eigen_C_0_AntiKt4EMTopoJets","FT_EFF_Eigen_C_1_AntiKt4EMTopoJets","FT_EFF_Eigen_C_2_AntiKt4EMTopoJets","FT_EFF_Eigen_C_3_AntiKt4EMTopoJets",
    "FT_EFF_Eigen_Light_0_AntiKt4EMTopoJets","FT_EFF_Eigen_Light_1_AntiKt4EMTopoJets","FT_EFF_Eigen_Light_2_AntiKt4EMTopoJets","FT_EFF_Eigen_Light_3_AntiKt4EMTopoJets","FT_EFF_Eigen_Light_4_AntiKt4EMTopoJets",
    "FT_EFF_extrapolation_AntiKt4EMTopoJets","FT_EFF_extrapolation_from_charm_AntiKt4EMTopoJets",
    "JET_21NP_JET_BJES_Response",
    "JET_21NP_JET_EffectiveNP_1","JET_21NP_JET_EffectiveNP_2","JET_21NP_JET_EffectiveNP_3","JET_21NP_JET_EffectiveNP_4","JET_21NP_JET_EffectiveNP_5","JET_21NP_JET_EffectiveNP_6","JET_21NP_JET_EffectiveNP_7","JET_21NP_JET_EffectiveNP_8restTerm",
    "JET_21NP_JET_EtaIntercalibration_Modelling","JET_21NP_JET_EtaIntercalibration_NonClosure","JET_21NP_JET_EtaIntercalibration_TotalStat",
    "JET_21NP_JET_Flavor_Composition","JET_21NP_JET_Flavor_Response",
    "JET_21NP_JET_Pileup_OffsetMu","JET_21NP_JET_Pileup_OffsetNPV","JET_21NP_JET_Pileup_PtTerm","JET_21NP_JET_Pileup_RhoTopology",
    "JET_21NP_JET_PunchThrough_MC15",
    "JET_21NP_JET_SingleParticle_HighPt",
    "JET_JvtEfficiency",
    "FATJET_Medium_JET_Comb_Modelling_Kin","FATJET_Medium_JET_Comb_TotalStat_Kin","FATJET_Medium_JET_Comb_Tracking_Kin",
    "FATJET_Weak_JET_Rtrk_Baseline_D2Beta1","FATJET_Weak_JET_Rtrk_Modelling_D2Beta1","FATJET_Weak_JET_Rtrk_TotalStat_D2Beta1","FATJET_Weak_JET_Rtrk_Tracking_D2Beta1",
    "PRW_DATASF",
    "WjetsModeling_Scale"
  };

  std::vector<TString> total_syst;
  for (auto syst_base: one_sided_syst)
  {
    total_syst.push_back (syst_base + "_up");
  }
  for (auto syst_base: two_sided_syst)
  {
    total_syst.push_back (syst_base + "_up");
    total_syst.push_back (syst_base + "_down");
  }
  std::cout << "Total number of systs: " << total_syst.size() << std::endl;

  TString syst ("nominal");
  if (jobID > 0) syst = total_syst.at(jobID-1);

  TString input_dir ("./sig_acceptance");

  std::cout << "\n\n\n======= Doing signal interpolation in region " << region << std::endl;
  TString inf_name = input_dir + "/" + region + ".root";
  TFile* inf = TFile::Open(inf_name, "read");
  gSystem->mkdir ("output/"+syst, kTRUE);
  TString fout_name = TString("output/") + syst + "/" + region + ".root";
  TFile* fout = new TFile (fout_name, "recreate");
  std::vector<TString> sig_strs;
  if (region.Contains("WW") && region.Contains("VBF"))
    {
      sig_strs.push_back("VBFWWNWAMASS");
      sig_strs.push_back("VBF-HVTWWMASS");
    }
  else if (region.Contains("WW") && !region.Contains("VBF"))
    {
      sig_strs.push_back("ggHWWNWAMASS");
      sig_strs.push_back("HVTWWMASS");
      sig_strs.push_back("RSGWWMASS");
      sig_strs.push_back("RSGWW-SmReWMASS");
    }
  else if (region.Contains("WZ") && region.Contains("VBF"))
    {
      sig_strs.push_back("VBF-HVTWZMASS");
    }
  else if (region.Contains("WZ") && !region.Contains("VBF"))
    {
      sig_strs.push_back("HVTWZMASS");
    }
  else if (region.Contains("Sideband") && !region.Contains("VBF"))
    {
      sig_strs.push_back("ggHWWNWAMASS");
      sig_strs.push_back("HVTWWMASS");
      sig_strs.push_back("HVTWZMASS");
      sig_strs.push_back("RSGWWMASS");
      sig_strs.push_back("RSGWW-SmReWMASS");
    }
  else if (region.Contains("Sideband") && region.Contains("VBF"))
    {
      sig_strs.push_back("VBFWWNWAMASS");
      sig_strs.push_back("VBF-HVTWWMASS");
      sig_strs.push_back("VBF-HVTWZMASS");
    }
  else if (region.Contains("Btag") && !region.Contains("VBF"))
    {
      sig_strs.push_back("ggHWWNWAMASS");
      sig_strs.push_back("HVTWWMASS");
      sig_strs.push_back("HVTWZMASS");
      sig_strs.push_back("RSGWWMASS");
      sig_strs.push_back("RSGWW-SmReWMASS");
    }
  else if (region.Contains("Btag") && region.Contains("VBF"))
    {
      sig_strs.push_back("VBFWWNWAMASS");
      sig_strs.push_back("VBF-HVTWWMASS");
      sig_strs.push_back("VBF-HVTWZMASS");
    }

  bool is_CR = !region.Contains("SR");
  is_CR = false;
  bool is_resolved = region.Contains("Resolved");
  for (auto sig_str_fmt: sig_strs)
  {
    std::cout << "======= Working on signal " << sig_str_fmt << std::endl;
    std::vector<std::shared_ptr<TH1> > results = InterpolateRegion (inf, sig_str_fmt, is_CR, is_resolved, syst, 4);
    fout->cd();
    if (syst.CompareTo("nominal", TString::kIgnoreCase) != 0)
    {
      gDirectory->cd("/");
      if( !gDirectory->GetKey("Systematics") ) { gDirectory->mkdir("Systematics"); }
      gDirectory->cd("Systematics");
    }
    std::cout << "NaN " << syst << " histograms in region " << region << std::endl;
    for (auto h_ptr: results)
    {
      if (TMath::IsNaN(h_ptr->Integral()))
      {
        std::cout << h_ptr->GetName() << " is NaN!" << std::endl;
      }
      else
      {
        h_ptr->Write();
      }
    }
  }
  inf->Close();
  fout->Close();

  std::cout << "Total morphing jobs created: " << AlgMorphBase::TotalID() << std::endl;

  return 0;
}
