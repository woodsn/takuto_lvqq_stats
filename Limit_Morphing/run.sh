#!/bin/sh

# for SAMPLE in "HVTWW" \
#     "HVTWZ" \
#     "ggHWWNWA" \
#     "RSGWW" \
#     "RSG-SmReW" \
#     "VBFWWNWA" \
#     "VBF-HVTWW" \
#     "VBF-HVTWZ";
for SAMPLE in "HVTWZ";
do
    for VARIATION in `ls input`;
#    for VARIATION in "nominal";
    do
	MASS=750
	root -b -q 'makeTempTree_Boosted.C("'${SAMPLE}'", "'${MASS}'", "'${VARIATION}'")'
	root -b -q 'makeTempTree_Resolved.C("'${SAMPLE}'", "'${MASS}'", "'${VARIATION}'")'
    done
done
